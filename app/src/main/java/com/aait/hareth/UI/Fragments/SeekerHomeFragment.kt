package com.aait.hareth.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.hareth.Base.BaseFragment
import com.aait.hareth.Client
import com.aait.hareth.Listener.OnItemClickListener
import com.aait.hareth.Models.JobModel
import com.aait.hareth.Models.SeekerHomeModel
import com.aait.hareth.Models.SliderModel
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.UI.Activities.CompanyDetailsActivity
import com.aait.hareth.UI.Adapter.HomeSeekerAdapter
import com.aait.hareth.UI.Adapter.SliderAdapter
import com.aait.hareth.Utils.CommonUtil
import com.github.islamkhsh.CardSliderViewPager
import com.google.gson.Gson
import me.relex.circleindicator.CircleIndicator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SeekerHomeFragment :BaseFragment(),OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(activity,CompanyDetailsActivity::class.java)
        intent.putExtra("company",categoriesModels.get(position))
        startActivity(intent)

    }


    lateinit var viewpager:CardSliderViewPager
    lateinit var indicator:CircleIndicator
    lateinit var rv_recycle: RecyclerView

    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    internal var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var gridLayoutManager: GridLayoutManager
    internal var categoriesModels = java.util.ArrayList<JobModel>()
    internal lateinit var homeSeekerAdapter: HomeSeekerAdapter






//    override val layoutResource: Int
//        get() = R.layout.fragment_seeker_home
    companion object {
        fun newInstance(): SeekerHomeFragment {
            val args = Bundle()
            val fragment = SeekerHomeFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    override val layoutResource: Int
        get() = R.layout.fragment_seeker_home

    override fun initializeComponents(view: View) {
        viewpager = view.findViewById(R.id.viewPager)
        indicator = view.findViewById(R.id.indicator)
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        gridLayoutManager = GridLayoutManager(context, 2)
        homeSeekerAdapter = HomeSeekerAdapter(mContext!!, categoriesModels, R.layout.recycler_seeker_home)
        Log.e("fff","fff")
        homeSeekerAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager = gridLayoutManager
        rv_recycle!!.adapter = homeSeekerAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getData() }
        getData()

    }
//    override fun initializeComponents(view: View) {
//        viewpager = view.findViewById(R.id.viewPager)
//        indicator = view.findViewById(R.id.indicator)
//        rv_recycle = view.findViewById(R.id.rv_recycle)
//        gridLayoutManager = GridLayoutManager(mContext, 2)
//        homeSeekerAdapter = mContext?.let { HomeSeekerAdapter(it, categoriesModels, R.layout.recycler_seeker_home) }!!
//        Log.e("fff","fff")
//
//        rv_recycle!!.layoutManager = gridLayoutManager
//        rv_recycle!!.adapter = homeSeekerAdapter
//        Client.getClient()?.create(Service::class.java)?.getHome(mLanguagePrefManager.appLanguage)?.enqueue(object :
//            Callback<SeekerHomeModel>{
//            override fun onResponse(call: Call<SeekerHomeModel>, response: Response<SeekerHomeModel>) {
//                if (response.isSuccessful) {
//                    try {
//                        if (response.body()?.key.equals("1")) {
//
//                            initSliderAds(response.body()?.slider!!)
//                            homeSeekerAdapter.updateAll(response.body()!!.data!!)
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
//                            }
//
//                        } else {}
//                    } catch (e: Exception) {e.printStackTrace() }
//                } else {  }
//            }
//            override fun onFailure(call: Call<SeekerHomeModel>, t: Throwable) {
//                Log.e("response", Gson().toJson(t))
//            }
//        })
//    }




    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.getHome(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<SeekerHomeModel>{
            override fun onResponse(call: Call<SeekerHomeModel>, response: Response<SeekerHomeModel>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {

                    if (response.body()?.key.equals("1")) {
                        initSliderAds(response.body()?.slider!!)
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                        } else {


                            homeSeekerAdapter.updateAll(response.body()!!.data!!)
                        }

                    } else {}

                } else {  }
            }
            override fun onFailure(call: Call<SeekerHomeModel>, t: Throwable) {
               CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                Log.e("response", Gson().toJson(t))
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
            }
        })

    }

    fun initSliderAds(list:ArrayList<SliderModel>){
        if(list.isEmpty()){
            viewpager.visibility=View.GONE
            indicator.visibility=View.GONE
        }
        else{
            viewpager.visibility=View.VISIBLE
            indicator.visibility=View.VISIBLE
            viewpager.adapter= SliderAdapter(context!!,list)
            indicator.setViewPager(viewpager)
        }
    }

    fun initRecycler(list:ArrayList<JobModel>){
        if(list.isEmpty()){


//            val params = LinearLayoutCompat.LayoutParams(
//                LinearLayoutCompat.LayoutParams.WRAP_CONTENT,
//                LinearLayoutCompat.LayoutParams.WRAP_CONTENT
//            )
//            params.setMargins(0,100, 0, 0)
//            image_no_data.layoutParams = params
        }
        else{
//            var layoutManager = GridLayoutManager(mContext!!,2)
//            rv_recycle.layoutManager = layoutManager
//            var adapter = SeekerHomeAdapter(mContext!!,list)
//            rv_recycle.adapter = adapter
        }
    }
}