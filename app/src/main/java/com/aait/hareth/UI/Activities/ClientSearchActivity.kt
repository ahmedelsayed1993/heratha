package com.aait.hareth.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Listener.OnItemClickListener
import com.aait.hareth.Models.JobModel
import com.aait.hareth.Models.SeekerHomeModel
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.UI.Adapter.HomeSeekerAdapter
import com.aait.hareth.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ClientSearchActivity:Parent_Activity(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this@ClientSearchActivity,CompanyDetailsActivity::class.java)
        intent.putExtra("company",categoriesModels.get(position))
        startActivity(intent)
    }

    lateinit var menu:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var city:EditText
    lateinit var search:Button
    lateinit var rv_recycle: RecyclerView

    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    internal var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var gridLayoutManager: GridLayoutManager
    internal var categoriesModels = java.util.ArrayList<JobModel>()
    internal lateinit var homeSeekerAdapter: HomeSeekerAdapter

    override val layoutResource: Int
        get() = R.layout.activity_client_search

    override fun initializeComponents() {
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        city = findViewById(R.id.city)
        search = findViewById(R.id.search)
        menu.setOnClickListener { onBackPressed() }
        title.text = getString(R.string.search)
        notification.visibility = View.GONE
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        gridLayoutManager = GridLayoutManager(mContext, 2)
        homeSeekerAdapter =  HomeSeekerAdapter(mContext, categoriesModels, R.layout.recycler_seeker_home)
        Log.e("fff","fff")
        homeSeekerAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager = gridLayoutManager
        rv_recycle!!.adapter = homeSeekerAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            city.text.clear()
            getData("") }
        search.setOnClickListener {
            if (CommonUtil.checkEditError(city,getString(R.string.search_by_name))){
                    return@setOnClickListener
            }else{
                getData(city.text.toString())
            }
        }

    }
    fun getData(text:String){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.searchclient(mLanguagePrefManager.appLanguage,text)?.enqueue(object :
            Callback<SeekerHomeModel> {
            override fun onResponse(call: Call<SeekerHomeModel>, response: Response<SeekerHomeModel>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {

                    if (response.body()?.key.equals("1")) {

                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                        } else {


                            homeSeekerAdapter.updateAll(response.body()!!.data!!)
                        }

                    } else {}

                } else {  }
            }
            override fun onFailure(call: Call<SeekerHomeModel>, t: Throwable) {
                mContext?.let { CommonUtil.handleException(it,t) }
                t.printStackTrace()
                Log.e("response", Gson().toJson(t))
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
            }
        })

    }
}