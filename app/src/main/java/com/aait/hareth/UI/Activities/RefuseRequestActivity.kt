package com.aait.hareth.UI.Activities

import android.content.Intent
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Models.RequestDetailsResponse
import com.aait.hareth.Models.RequestModel
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RefuseRequestActivity :Parent_Activity(){
    lateinit var menu: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var requestModel: RequestModel
    lateinit var reason:EditText
    lateinit var send:Button
    override val layoutResource: Int
        get() = R.layout.activity_refuse_request

    override fun initializeComponents() {
        notification = findViewById(R.id.notification)
        notification.setOnClickListener { startActivity(Intent(this@RefuseRequestActivity,NotificationActivity::class.java)) }
        requestModel = intent.getSerializableExtra("user") as RequestModel
        Log.e("user", Gson().toJson(requestModel))
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        menu.setOnClickListener { onBackPressed() }
        title.text = getString(R.string.Job_seeker_details)
        reason = findViewById(R.id.reason)
        send = findViewById(R.id.send)
        send.setOnClickListener {
            if (CommonUtil.checkEditError(reason,getString(R.string.Please_write_down_the_reason_for_the_refusal))){
                return@setOnClickListener
            }else{
                getRequest("rejected")
            }
        }
    }
    fun getRequest(action:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getRequest(mLanguagePrefManager.appLanguage,requestModel.id!!,mSharedPrefManager.userData.id!!,action,reason.text.toString())
            ?.enqueue(object : Callback<RequestDetailsResponse> {
                override fun onFailure(call: Call<RequestDetailsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<RequestDetailsResponse>,
                    response: Response<RequestDetailsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.key.equals("1")){
                           startActivity(Intent(this@RefuseRequestActivity,RefuseSentActivity::class.java))
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
}