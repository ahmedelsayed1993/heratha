package com.aait.hareth.UI.Adapter

import android.content.Context

import android.view.View
import com.aait.hareth.Models.SliderModel
import com.aait.hareth.R
import com.github.islamkhsh.CardSliderAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.card_image_slider.view.*

class SliderAdapter (context:Context,list : ArrayList<SliderModel>) : CardSliderAdapter<SliderModel>(list) {
    var list = list
    var context=context
    override fun bindView(position: Int, itemContentView: View, item: SliderModel?) {

            Picasso.with(context).load(item?.image).resize(300,300).into(itemContentView.image)
            itemContentView.setOnClickListener {

            }

    }

    override fun getItemContentLayout(position: Int) : Int { return R.layout.card_image_slider }
}