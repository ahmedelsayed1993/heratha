package com.aait.hareth.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Listener.OnItemClickListener
import com.aait.hareth.Models.*
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.UI.Adapter.ListAdapter
import com.aait.hareth.UI.Adapter.RequestsAdapter
import com.aait.hareth.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class ProviderSearchActivity:Parent_Activity() ,OnItemClickListener{

    override fun onItemClick(view: View, position: Int) {
        if (selected==0){
            listModel = listModels.get(position)
            city.setText(listModel.name!!)
            cities.visibility=View.GONE
        }else {
            val intent = Intent(this@ProviderSearchActivity, JobsDetailsActivity::class.java)
            intent.putExtra("user", requestModels.get(position))
            Log.e("user",Gson().toJson(requestModels.get(position)))
            startActivity(intent)
        }
    }

    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var requestModels = java.util.ArrayList<RequestModel>()
    internal lateinit var requestsAdapter: RequestsAdapter
    lateinit var menu: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var city: TextView
    lateinit var search: Button
    lateinit var cities:RecyclerView
    lateinit var rv_recycle: RecyclerView
    internal lateinit var linearLayoutManager1: LinearLayoutManager
    internal var listModels = ArrayList<ListModel>()
    internal lateinit var listAdapter: ListAdapter

    internal lateinit var listModel: ListModel

    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    internal var swipeRefresh: SwipeRefreshLayout? = null
    var selected:Int=0
    override val layoutResource: Int
        get() = R.layout.activity_provider_search

    override fun initializeComponents() {
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        city = findViewById(R.id.city)
        search = findViewById(R.id.search)
        menu.setOnClickListener { onBackPressed() }
        title.text = getString(R.string.search)
        notification.visibility = View.GONE
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        cities =findViewById(R.id.cities)
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter = mContext?.let { ListAdapter(it,listModels,R.layout.recycler_list) }!!
        listAdapter.setOnItemClickListener(this)
        cities!!.layoutManager= linearLayoutManager1
        cities!!.adapter = listAdapter
        getCities()
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        requestsAdapter = RequestsAdapter(mContext,requestModels,R.layout.recycler_order_requests)
        requestsAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = requestsAdapter
        city.setOnClickListener {
            selected = 0
            if (cities.visibility == View.GONE){
                cities.visibility= View.VISIBLE
            }else{
                cities.visibility= View.GONE
            }
        }
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {

            getData(1) }
        search.setOnClickListener {
            if (CommonUtil.checkTextError(city,getString(R.string.search_by_city))){
                return@setOnClickListener
            }else{
                getData(listModel.id!!)
            }
        }

    }
    fun getCities(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.cities(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ListResponse> {

            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()!!.key.equals("1")){
                        listAdapter.updateAll(response.body()!!.data!!)
                    }else{

                    }
                }
            }
        } )
    }

    fun getData(text:Int){
        selected = 1
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.providerSearch(mLanguagePrefManager.appLanguage,text)?.enqueue(object :
            Callback<ProviderSearchModel> {
            override fun onResponse(call: Call<ProviderSearchModel>, response: Response<ProviderSearchModel>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {

                    if (response.body()?.key.equals("1")) {

                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                        } else {


                            requestsAdapter.updateAll(response.body()!!.data!!)
                        }

                    } else {}

                } else {  }
            }
            override fun onFailure(call: Call<ProviderSearchModel>, t: Throwable) {
                mContext?.let { CommonUtil.handleException(it,t) }
                t.printStackTrace()
                Log.e("response", Gson().toJson(t))
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
            }
        })

    }
}