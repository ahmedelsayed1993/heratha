package com.aait.hareth.UI.Activities

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Build
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Listener.OnItemClickListener
import com.aait.hareth.Models.*
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.UI.Adapter.DirectionAdapter
import com.aait.hareth.UI.Adapter.ListAdapter
import com.aait.hareth.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class EditJobActivity:Parent_Activity(),OnItemClickListener {
    lateinit var start: TextView
    lateinit var end_date: TextView
    lateinit var name: EditText
    lateinit var special: TextView
    lateinit var specials: RecyclerView
    lateinit var salary: EditText
    lateinit var city: TextView
    lateinit var cities: RecyclerView
    lateinit var direction: TextView
    lateinit var directions: RecyclerView
    lateinit var certificate: EditText
    lateinit var length: EditText
    lateinit var weight: EditText
    lateinit var age: EditText
    lateinit var experiences: EditText
    lateinit var confirm: Button
    lateinit var menu: ImageView
    lateinit var logo: ImageView
    lateinit var title: TextView
    lateinit var search: ImageView
    lateinit var notification: ImageView
    lateinit var name_en: EditText
    lateinit var certificate_en: EditText
    lateinit var experiences_en: EditText
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal lateinit var linearLayoutManager1: LinearLayoutManager
    internal lateinit var linearLayoutManager2: LinearLayoutManager
    internal var listModels = ArrayList<ListModel>()
    internal var listModels1 = ArrayList<ListModel>()
    internal var listModels2 = ArrayList<DirectionsModel>()
    internal lateinit var listAdapter: ListAdapter
    internal lateinit var listAdapter1: ListAdapter
    internal lateinit var listAdapter2: DirectionAdapter
    internal lateinit var listModel: ListModel
    internal lateinit var specialty: ListModel
    internal lateinit var Directions: DirectionsModel
    lateinit var date1:LocalDate
    lateinit var date:LocalDate
    var selected:Int = 0
    var id:Int=0

    override fun onItemClick(view: View, position: Int) {
        if (selected==0){
            listModel = listModels.get(position)
            city.setText(listModel.name!!)
            cities.visibility=View.GONE
            city.setBackgroundResource(R.drawable.off_white_shape)
        }else if (selected == 1){
            specialty = listModels1.get(position)
            special.setText(specialty.name!!)
            specials.visibility=View.GONE
            special.setBackgroundResource(R.drawable.off_white_shape)
        }else if (selected == 2){
            Directions = listModels2.get(position)
            direction.setText(Directions.name!!)
            directions.visibility=View.GONE
            direction.setBackgroundResource(R.drawable.off_white_shape)
        }
    }

    override val layoutResource: Int
        get() = R.layout.activity_add_job

    @RequiresApi(Build.VERSION_CODES.O)
    override fun initializeComponents() {
        search = findViewById(R.id.search)
        search.visibility =View.GONE
        notification = findViewById(R.id.notification)
        notification.setOnClickListener { startActivity(Intent(this@EditJobActivity,NotificationActivity::class.java)) }
        id = intent.getIntExtra("id",0)
        menu = findViewById(R.id.menu)
        logo = findViewById(R.id.logo)
        title = findViewById(R.id.title)
        search = findViewById(R.id.search)
        notification = findViewById(R.id.notification)
        menu.setImageResource(R.mipmap.lefarrow)
        logo.visibility = View.GONE
        title.visibility = View.VISIBLE
        title.setText(getString(R.string.Modify_the_job))
        menu.setOnClickListener { onBackPressed() }
        start = findViewById(R.id.start)
        end_date = findViewById(R.id.end_date)
        name = findViewById(R.id.name)
        name_en = findViewById(R.id.name_en)
        special = findViewById(R.id.special)
        specials = findViewById(R.id.specials)
        salary = findViewById(R.id.salary)
        city = findViewById(R.id.city)
        cities = findViewById(R.id.cities)
        direction = findViewById(R.id.direction)
        directions = findViewById(R.id.directions)
        certificate = findViewById(R.id.certificate)
        certificate_en = findViewById(R.id.certificate_en)
        length = findViewById(R.id.length)
        weight = findViewById(R.id.weight)
        age = findViewById(R.id.age)
        experiences = findViewById(R.id.experiences)
        experiences_en = findViewById(R.id.experiences_en)
        confirm = findViewById(R.id.confirm)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter =  ListAdapter(mContext!!,listModels,R.layout.recycler_list)
        listAdapter.setOnItemClickListener(this)
        cities!!.layoutManager= linearLayoutManager
        cities!!.adapter = listAdapter
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter1 =  ListAdapter(mContext!!,listModels1,R.layout.recycler_list)
        listAdapter1.setOnItemClickListener(this)
        specials!!.layoutManager= linearLayoutManager1
        specials!!.adapter = listAdapter1
        linearLayoutManager2 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter2 = DirectionAdapter(mContext!!,listModels2,R.layout.recycler_list)
        listAdapter2.setOnItemClickListener(this)
        directions!!.layoutManager= linearLayoutManager2
        directions!!.adapter = listAdapter2
        getCities()
        getSpecial()
        city.setOnClickListener {
            selected = 0
            city.setBackgroundResource(R.drawable.red_white_shap)
            if (cities.visibility ==View.GONE){
                cities.visibility=View.VISIBLE
            }else{
                cities.visibility=View.GONE
            }
        }
        special.setOnClickListener {
            selected = 1
            special.setBackgroundResource(R.drawable.red_white_shap)
            if (specials.visibility ==View.GONE){
                specials.visibility=View.VISIBLE
            }else{
                specials.visibility=View.GONE
            }
        }
        direction.setOnClickListener {
            selected = 2
            direction.setBackgroundResource(R.drawable.red_white_shap)
            listModels2.clear()
            listModels2.add(DirectionsModel("North",getString(R.string.north)))
            listModels2.add(DirectionsModel("South",getString(R.string.south)))
            listModels2.add(DirectionsModel("East",getString(R.string.east)))
            listModels2.add(DirectionsModel("West",getString(R.string.west)))
            if (directions.visibility ==View.GONE){
                directions.visibility=View.VISIBLE
            }else{
                directions.visibility=View.GONE
            }
        }

        getJob()
        start.setOnClickListener { val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)


            val dpd = DatePickerDialog(mContext, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val m:Int = monthOfYear+1
                var M:String
                if (m<10){
                    M = "0" + m
                }else{
                    M = m.toString()
                }
                var d:String
                if (dayOfMonth<10){
                    d = "0" + dayOfMonth
                }else{
                    d = dayOfMonth.toString()
                }
                // Display Selected date in textbox
                start.setText("" + year + "-" + M + "-" + d)
            }, year, month, day)
            dpd.datePicker.minDate = System.currentTimeMillis()-1000

            dpd.show()
        }
        end_date.setOnClickListener { val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)


            val dpd = DatePickerDialog(mContext, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val m:Int = monthOfYear+1
                var M:String
                if (m<10){
                    M = "0" + m
                }else{
                    M = m.toString()
                }
                var d:String
                if (dayOfMonth<10){
                    d = "0" + dayOfMonth
                }else{
                    d = dayOfMonth.toString()
                }
                // Display Selected date in textbox

                end_date.setText("" + year + "-" + M + "-" + d)
            }, year, month, day)
            dpd.datePicker.minDate = System.currentTimeMillis()-1000
            dpd.show()
        }

        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(name,getString(R.string.sorry_fill_Empty_form))||
                CommonUtil.checkEditError(name_en,getString(R.string.sorry_fill_Empty_form))||
                CommonUtil.checkTextError(special,getString(R.string.sorry_fill_Empty_form))||
                CommonUtil.checkEditError(salary,getString(R.string.sorry_fill_Empty_form))||
                CommonUtil.checkTextError(city,getString(R.string.sorry_fill_Empty_form))||
                CommonUtil.checkTextError(direction,getString(R.string.sorry_fill_Empty_form))||
                CommonUtil.checkEditError(certificate,getString(R.string.sorry_fill_Empty_form))||
                CommonUtil.checkEditError(certificate_en,getString(R.string.sorry_fill_Empty_form))||
                CommonUtil.checkEditError(length,getString(R.string.sorry_fill_Empty_form))||
                CommonUtil.checkEditError(weight,getString(R.string.sorry_fill_Empty_form))||
                CommonUtil.checkEditError(age,getString(R.string.sorry_fill_Empty_form))||
                CommonUtil.checkEditError(experiences,getString(R.string.sorry_fill_Empty_form))||
                CommonUtil.checkEditError(experiences_en,getString(R.string.sorry_fill_Empty_form))||
                CommonUtil.checkTextError(start,getString(R.string.sorry_fill_Empty_form))||
                CommonUtil.checkTextError(end_date,getString(R.string.sorry_fill_Empty_form))){
                return@setOnClickListener
            }else{
                if (!end_date.text.equals("")) {
                    date1= LocalDate.parse(end_date.text.toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                }
                if (!start.text.equals("")) {
                    date = LocalDate.parse(start.text.toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                }
                if (date1.isBefore(date)){
                    CommonUtil.makeToast(mContext,getString(R.string.date_text))
                }else {
                    addJob()
                }
            }
        }

    }
    fun setData(jobDetailsModel: JobDetailsModel){
        Directions = DirectionsModel(jobDetailsModel.direction_key,jobDetailsModel.direction)
        listModel = ListModel(jobDetailsModel.city_id,jobDetailsModel.city)
        specialty = ListModel(jobDetailsModel.specialty_id,jobDetailsModel.specialty)
        name.setText(jobDetailsModel.title_ar)
        name_en.setText(jobDetailsModel.title_en)
        special.text = jobDetailsModel.specialty
        city.text = jobDetailsModel.city
        salary.setText(jobDetailsModel.salary)
        direction.text = jobDetailsModel.direction
        certificate.setText(jobDetailsModel.certificates_ar)
        certificate_en.setText(jobDetailsModel.certificates_en)
        length.setText(jobDetailsModel.height!!.toString())
        weight.setText(jobDetailsModel.width!!.toString())
        age.setText(jobDetailsModel.age!!.toString())
        experiences.setText(jobDetailsModel.experience_ar)
        experiences_en.setText(jobDetailsModel.experience_en)
        start.text = jobDetailsModel.start_date
        end_date.text = jobDetailsModel.end_date
    }

    fun getJob(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getJob(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id)?.enqueue(object :
            Callback<JobDetailsResponse> {
            override fun onFailure(call: Call<JobDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<JobDetailsResponse>,
                response: Response<JobDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        setData(response.body()?.data!!)

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
    fun getCities(){

        Client.getClient()?.create(Service::class.java)?.cities(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ListResponse> {

            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()

            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {

                if (response.isSuccessful){
                    if (response.body()!!.key.equals("1")){
                        listAdapter.updateAll(response.body()!!.data!!)
                    }else{

                    }
                }
            }
        } )
    }
    fun getSpecial(){

        Client.getClient()?.create(Service::class.java)?.specialties(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ListResponse> {

            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()

            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {

                if (response.isSuccessful){
                    if (response.body()!!.key.equals("1")){
                        listAdapter1.updateAll(response.body()!!.data!!)
                    }else{

                    }
                }
            }
        } )
    }

    fun addJob(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.editJob(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id
            ,name.text.toString(),name_en.text.toString(),specialty.id!!,salary.text.toString(),listModel.id!!,Directions.id!!,certificate.text.toString()
            ,certificate_en.text.toString(),length.text.toString(),weight.text.toString(),age.text.toString(),experiences.text.toString(),experiences_en.text.toString()
            ,start.text.toString(),end_date.text.toString())?.enqueue(object:Callback<JobDetailsResponse>{
            override fun onFailure(call: Call<JobDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<JobDetailsResponse>, response: Response<JobDetailsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.key.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        startActivity(Intent(this@EditJobActivity,ProviderMainActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}