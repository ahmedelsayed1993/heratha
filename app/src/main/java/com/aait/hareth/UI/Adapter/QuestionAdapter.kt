package com.aait.hareth.UI.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.Color.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import com.aait.hareth.Base.ParentRecyclerAdapter
import com.aait.hareth.Base.ParentRecyclerViewHolder
import com.aait.hareth.Models.QuestionModel
import com.aait.hareth.R
import com.aait.hareth.R.color.*
import com.bumptech.glide.Glide
import com.aait.hareth.R.color.colorPrimaryDark as colorColorPrimaryDark

class QuestionAdapter(context: Context, data: MutableList<QuestionModel>, layoutId: Int) :
    ParentRecyclerAdapter<QuestionModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.qustion!!.setText(questionModel.question)
        viewHolder.answer!!.setText(questionModel.answer)
        viewHolder.answer.visibility = View.GONE
        viewHolder.question_lay.setOnClickListener {
            if (viewHolder.answer.visibility==View.GONE){
//                viewHolder.question_lay!!.setBackgroundColor(colorPrimary)
                viewHolder.question_lay.setBackgroundColor(Color.parseColor("#EA3434"))
                viewHolder.qustion.textColor=Color.parseColor("#FFFFFF")
                viewHolder.answer.visibility= View.VISIBLE
                viewHolder.down.setImageResource(R.mipmap.ic_chevronn)
            }else{
                viewHolder.question_lay.setBackgroundColor(Color.parseColor("#F8F8F8"))
                viewHolder.qustion.textColor=Color.parseColor("#8D8D8D")
                viewHolder.answer.visibility= View.GONE
                viewHolder.down.setImageResource(R.mipmap.ic_chevron_left)
            }
        }

    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {




        internal  var question_lay=itemView.findViewById<LinearLayout>(R.id.lay_question)
        internal var qustion=itemView.findViewById<TextView>(R.id.question)
        internal var down=itemView.findViewById<ImageView>(R.id.down)
        internal var answer=itemView.findViewById<TextView>(R.id.answer)


    }
}