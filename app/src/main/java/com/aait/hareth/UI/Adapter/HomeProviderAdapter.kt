package com.aait.hareth.UI.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import com.aait.hareth.Base.ParentRecyclerAdapter
import com.aait.hareth.Base.ParentRecyclerViewHolder
import com.aait.hareth.Models.JobModel
import com.aait.hareth.Models.MyJobsModel
import com.aait.hareth.R
import com.bumptech.glide.Glide

class HomeProviderAdapter (context: Context, data: MutableList<MyJobsModel>, layoutId: Int) :
    ParentRecyclerAdapter<MyJobsModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as HomeProviderAdapter.ViewHolder
        val myJobsModel = data.get(position)
        viewHolder.title!!.setText(myJobsModel.titel)
        viewHolder.special!!.setText(myJobsModel.specialty)
        viewHolder.date!!.setText(myJobsModel.created)
       viewHolder.cancel.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })
        viewHolder.edit.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })
    }

    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {

        internal var title=itemView.findViewById<TextView>(R.id.title)
        internal var special=itemView.findViewById<TextView>(R.id.special)
        internal var edit=itemView.findViewById<LinearLayout>(R.id.edit)
        internal var cancel = itemView.findViewById<LinearLayout>(R.id.cancel)
        internal var date=itemView.findViewById<TextView>(R.id.date)

    }
}