package com.aait.hareth.UI.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aait.hareth.Base.ParentRecyclerAdapter
import com.aait.hareth.Base.ParentRecyclerViewHolder
import com.aait.hareth.Models.ProviderOrderModel
import com.aait.hareth.Models.RequestModel
import com.aait.hareth.R
import com.bumptech.glide.Glide

class RequestsAdapter (context: Context, data: MutableList<RequestModel>, layoutId: Int) :
    ParentRecyclerAdapter<RequestModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as RequestsAdapter.ViewHolder
        val requestModel = data.get(position)
        viewHolder.name!!.setText(requestModel.name)
        viewHolder.date!!.setText(requestModel.created)
        viewHolder.address!!.setText(requestModel.city)
        Glide.with(mcontext).load(requestModel.avatar).into(viewHolder.image)
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view ->
            onItemClickListener.onItemClick(
                view,
                position
            ) })

    }

    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {

        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var date=itemView.findViewById<TextView>(R.id.date)
        internal var image = itemView.findViewById<ImageView>(R.id.image)
        internal var address=itemView.findViewById<TextView>(R.id.address)

    }
}