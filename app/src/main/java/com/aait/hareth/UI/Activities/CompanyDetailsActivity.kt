package com.aait.hareth.UI.Activities

import android.app.Dialog
import android.content.Intent
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Models.JobModel
import com.aait.hareth.R
import com.aait.hareth.UI.Adapter.SubscribeTapAdapter
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayout
import de.hdodenhof.circleimageview.CircleImageView

class CompanyDetailsActivity:Parent_Activity() {
    lateinit var menu:ImageView
    lateinit var title:TextView
    lateinit var notification: ImageView
    lateinit var image:CircleImageView
    lateinit var name:TextView
    lateinit var rating:RatingBar
    lateinit var address:TextView
    lateinit var orders:TabLayout
    lateinit var ordersViewPager:ViewPager
    private var mAdapter: SubscribeTapAdapter? = null
    lateinit var jobModel: JobModel
    override val layoutResource: Int
        get() = R.layout.activity_company_details

    override fun initializeComponents() {
        jobModel = intent.getSerializableExtra("company") as JobModel
        orders = findViewById(R.id.orders)
        ordersViewPager = findViewById(R.id.ordersViewPager)
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        rating = findViewById(R.id.rating)
        address = findViewById(R.id.address)
        title.text = getString(R.string.Company_details)
        menu.setOnClickListener { onBackPressed() }
        notification.setOnClickListener { if (mSharedPrefManager.loginStatus!!){
        startActivity(Intent(this@CompanyDetailsActivity,NotificationActivity::class.java))
        }else{
            val dialog =  Dialog(mContext!!)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)

            dialog ?.setCancelable(false)
            dialog ?.setContentView(R.layout.dialog_login)
            val login = dialog?.findViewById<Button>(R.id.login)
            login.setOnClickListener {  val intent=Intent(this@CompanyDetailsActivity,PreLoginActivity::class.java)

                startActivity(intent)
                finish()
            }
            dialog.show()
        }
        }
        mAdapter = SubscribeTapAdapter(mContext, supportFragmentManager,jobModel.id!!)
        ordersViewPager.setAdapter(mAdapter)
        orders!!.setupWithViewPager(ordersViewPager)
        Glide.with(mContext).load(jobModel.image).into(image)
        name.text = jobModel.name
        address.text = jobModel.city
        rating.rating = jobModel.rate!!


    }
}