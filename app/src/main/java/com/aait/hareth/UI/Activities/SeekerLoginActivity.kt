package com.aait.hareth.UI.Activities

import android.content.Intent
import android.text.InputType
import android.view.View
import android.widget.*
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Models.UserResponse
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.Utils.CommonUtil
import com.google.firebase.iid.FirebaseInstanceId
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SeekerLoginActivity : Parent_Activity() {


    lateinit var back : ImageView
    lateinit var phone:EditText
    lateinit var password:EditText
    lateinit var view: ImageView
    lateinit var login:Button
    lateinit var visitor:Button
    lateinit var forgot:TextView
    lateinit var register:LinearLayout
    lateinit var type:String
    var deviceID=""
    override val layoutResource: Int
        get() = R.layout.activity_seeker_login

    override fun initializeComponents() {
        type = intent.getStringExtra("type")
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        back = findViewById(R.id.back)
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        view = findViewById(R.id.view)
        login = findViewById(R.id.login)
        visitor = findViewById(R.id.visitor)
        forgot = findViewById(R.id.forgot)
        register = findViewById(R.id.register)
        if (type.equals("seeker")){
            visitor.visibility=View.VISIBLE
        }else if (type.equals("provider")){
            visitor.visibility = View.GONE
        }

        back.setOnClickListener(View.OnClickListener { startActivity(Intent(this@SeekerLoginActivity,PreLoginActivity::class.java))
        finish()})
        login.setOnClickListener(View.OnClickListener {
            if (CommonUtil.checkEditError(phone,getString(R.string.please_enter_phone))||
                    CommonUtil.checkEditError(password,getString(R.string.please_enter_password))){
                return@OnClickListener
            }else{
                login()
            }
        })
        view.setOnClickListener {
            if (password.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
            password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

           }
            else{
            password.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
           }
        }
        visitor.setOnClickListener(View.OnClickListener {
            if (type.equals("user")) {
                mSharedPrefManager.loginStatus=false
                val intent=Intent(this@SeekerLoginActivity, MainActivity::class.java)
                startActivity(intent)
            }else if (type.equals("provider")){
            } })
        forgot.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this@SeekerLoginActivity,RestorePasswordActivity::class.java))
        })
        register.setOnClickListener(View.OnClickListener {
            if (type.equals("user")) {
                val intent=Intent(this@SeekerLoginActivity, SeekerRegisterActivity::class.java)
                intent.putExtra("type",type)
                startActivity(intent)
            }else if (type.equals("provider")){
                val intent=Intent(this@SeekerLoginActivity, ProviderRegisterActivity::class.java)
                intent.putExtra("type",type)
                startActivity(intent)

            }})

    }

    fun login(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.signIn(mLanguagePrefManager.appLanguage,phone.text.toString(),password.text.toString()
        ,deviceID,"android")?.enqueue(object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
               hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        mSharedPrefManager.loginStatus=true
                        mSharedPrefManager.userData=response.body()?.data!!
                        if (response.body()?.data!!.user_type.equals("provider")){
                            startActivity(Intent(this@SeekerLoginActivity,ProviderMainActivity::class.java))
                            finish()

                        }else{
                            startActivity(Intent(this@SeekerLoginActivity,MainActivity::class.java))
                            finish()
                        }
                    }else if (response.body()?.key.equals("2")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        intent = Intent(this@SeekerLoginActivity,ActivateAccountActivity::class.java)
                        intent.putExtra("user",response.body()?.data!!)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })

    }




}