package com.aait.hareth.UI.Fragments

import android.Manifest
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.text.InputType
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.hareth.Base.BaseFragment
import com.aait.hareth.Client
import com.aait.hareth.Listener.OnItemClickListener
import com.aait.hareth.Models.*
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.UI.Activities.ActivateAccountActivity
import com.aait.hareth.UI.Activities.PresentedDetailsActivity
import com.aait.hareth.UI.Adapter.ListAdapter
import com.aait.hareth.Utils.CheckForSDCard
import com.aait.hareth.Utils.CommonUtil
import com.aait.hareth.Utils.PermissionUtils
import com.aait.hareth.Utils.ProgressRequestBody
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import com.jaiselrahman.filepicker.config.Configurations
import com.jaiselrahman.filepicker.model.MediaFile
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import pub.devrel.easypermissions.EasyPermissions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedInputStream
import java.io.File
import java.io.FileOutputStream
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

class UserProfileFragment:BaseFragment() , OnItemClickListener, ProgressRequestBody.UploadCallbacks, EasyPermissions.PermissionCallbacks {
    override fun onItemClick(view: View, position: Int) {
        if (selected==0) {
            listModel = listModels.get(position)
            city.setText(listModel.name!!)
            cities.visibility = View.GONE
        }else{
            listModel1 = listModels1.get(position)
            lang.setText(listModel1.name!!)
            langs.visibility = View.GONE
            if (listModel1.id==1){
                lang_lay.visibility = View.GONE
                effecient = null
            }else{
                lang_lay.visibility = View.VISIBLE
            }

        }
    }

    override fun onProgressUpdate(percentage: Int) {

    }

    override fun onError() {

    }

    override fun onFinish() {

    }
    companion object {
        fun newInstance(): UserProfileFragment {
            val args = Bundle()
            val fragment = UserProfileFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    private var url: String? = null
    private val WRITE_REQUEST_CODE = 300
    private val TAG = UserProfileFragment::class.java!!.getSimpleName()
    lateinit var change_pass:Button
    lateinit var name:TextView
    lateinit var user_name:EditText
    lateinit var image: CircleImageView
    lateinit var city: TextView
    lateinit var gender: TextView
    lateinit var date_of_birth: TextView
    lateinit var length:EditText
    lateinit var weight:EditText
    lateinit var CV: TextView
    lateinit var type:String
    lateinit var cities: RecyclerView
    lateinit var lang: TextView
    lateinit var langs: RecyclerView
    lateinit var lang_lay: RadioGroup
    lateinit var good: RadioButton
    lateinit var very_good: RadioButton
    lateinit var excellent: RadioButton
    internal var sex:String?=null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var listModels = ArrayList<ListModel>()
    internal lateinit var listAdapter: ListAdapter
    internal lateinit var linearLayoutManager1: LinearLayoutManager
    internal var listModels1 = ArrayList<ListModel>()
    internal lateinit var listAdapter1: ListAdapter
    private var ImageBasePath: String? = null
    internal lateinit var listModel: ListModel
    internal lateinit var listModel1: ListModel
    internal var selected:Int = 0
    lateinit var Id_num:EditText
    lateinit var education:EditText
    lateinit var phone:EditText
    internal var effecient: String? =null
    private val FILE_REQUEST_CODE = 666
    lateinit var files : ArrayList<MediaFile>
    lateinit var download:ImageView
    lateinit var confirm:Button
    internal var returnValue: ArrayList<String>? = ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")

    override val layoutResource: Int
        get() = R.layout.fragment_usder_profile

    override fun initializeComponents(view: View) {
        confirm = view.findViewById(R.id.confirm)
        change_pass = view.findViewById(R.id.change_pass)
        download = view.findViewById(R.id.download)
        change_pass.setOnClickListener { showDialog() }
        files= ArrayList()
        user_name = view.findViewById(R.id.user_name)
        phone = view.findViewById(R.id.phone)
        name = view.findViewById(R.id.name)
        image = view.findViewById(R.id.image)
        Id_num = view.findViewById(R.id.Id_num)
        education = view.findViewById(R.id.education)
        city = view.findViewById(R.id.city)
        gender = view.findViewById(R.id.gender)
        cities = view.findViewById(R.id.cities)
        date_of_birth = view.findViewById(R.id.date_of_birth)
        length = view.findViewById(R.id.length)
        weight = view.findViewById(R.id.weight)
        CV = view.findViewById(R.id.CV)
        lang = view.findViewById(R.id.lang)
        lang_lay = view.findViewById(R.id.lang_lay)
        langs = view.findViewById(R.id.langs)
        good = view.findViewById(R.id.good)
        very_good = view.findViewById(R.id.very_good)
        excellent = view.findViewById(R.id.excellent)
        lang_lay.visibility = View.GONE
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter = ListAdapter(mContext!!,listModels,R.layout.recycler_list)
        listAdapter.setOnItemClickListener(this)
        cities!!.layoutManager= linearLayoutManager
        cities!!.adapter = listAdapter
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter1 =  ListAdapter(mContext!!,listModels1,R.layout.recycler_list)
        listAdapter1.setOnItemClickListener(this)
        langs!!.layoutManager= linearLayoutManager1
        langs!!.adapter = listAdapter1
        getCities()
        getLangs()
        download.setOnClickListener {
            if (CheckForSDCard.isSDCardPresent) {

                //check if app has permission to write to the external storage.
                if (EasyPermissions.hasPermissions(
                        activity,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                ) {
                    //Get the URL entered

                    DownloadFile().execute(mSharedPrefManager.userData.cv)

                } else {
                    //If permission is not present request for the same.
                    EasyPermissions.requestPermissions(activity,
                        mContext!!.getString(R.string.acess_storage),
                        WRITE_REQUEST_CODE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                }


            } else {
                Toast.makeText(
                    activity?.applicationContext,
                    "SD Card not found", Toast.LENGTH_LONG
                ).show()

            }
        }
        image.setOnClickListener { getPickImageWithPermission() }
        confirm.setOnClickListener {

                if (CommonUtil.checkEditError(user_name,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkEditError(phone,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(Id_num,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkTextError(city,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkTextError(gender,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkTextError(date_of_birth,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkEditError(length,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkEditError(weight,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkEditError(education,getString(R.string.sorry_fill_Empty_form)))
                {
                    return@setOnClickListener
                }else{
                    if (files.isEmpty()){
                        Register(effecient)
                    }else{

                            register(effecient)

                    }
                }

        }
        CV.setOnClickListener {
            var intent = Intent(activity, FilePickerActivity::class.java)
            intent.putExtra(
                FilePickerActivity.CONFIGS,  Configurations.Builder()
                    .setCheckPermission(true)
                    .setShowFiles(true)
                    .setShowAudios(false)
                    .setShowVideos(false)
                    .setShowImages(false)
                    .setSuffixes("pdf")
                    .setSingleChoiceMode(true)
                    .build())
            startActivityForResult(intent, FILE_REQUEST_CODE)
        }
        gender.setOnClickListener {
            val popupMenu = androidx.appcompat.widget.PopupMenu(mContext!!, gender)

            popupMenu.inflate(R.menu.gender)
            popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.male -> {
                        gender.setText(getString(R.string.male))
                        sex = "male"
                        return@OnMenuItemClickListener true
                    }
                    R.id.female -> {
                        gender.setText(getString(R.string.female))
                        sex = "female"
                        return@OnMenuItemClickListener true
                    }
                }
                false
            })

            popupMenu.show()
        }
        good.setOnClickListener { effecient="good"
            lang_lay.visibility = View.GONE}
        very_good.setOnClickListener { effecient="very good"
            lang_lay.visibility = View.GONE}
        excellent.setOnClickListener { effecient="excellent"
            lang_lay.visibility = View.GONE}


        city.setOnClickListener {
            selected = 0
            if (cities.visibility ==View.GONE){
                cities.visibility=View.VISIBLE
            }else{
                cities.visibility=View.GONE
            }
        }
        lang.setOnClickListener {
            selected = 1
            if (langs.visibility ==View.GONE){
                langs.visibility=View.VISIBLE
            }else{
                langs.visibility=View.GONE
            }
        }
        date_of_birth.setOnClickListener { val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)


            val dpd = DatePickerDialog(mContext!!, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val m:Int = monthOfYear+1
                // Display Selected date in textbox
                date_of_birth.setText("" + dayOfMonth + "/" + m + "/" + year)
            }, year, month, day)

            dpd.show() }
      if (mSharedPrefManager.loginStatus!!) {
          getProfile()
      }

    }

fun dialog(){
    val dialog =  Dialog(mContext!!)
    dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog ?.setCancelable(false)
    dialog ?.setContentView(R.layout.dialog_change)
    val back = dialog?.findViewById<Button>(R.id.back)
    back?.setOnClickListener { dialog?.dismiss() }
    dialog?.show()
}
    private fun showDialog() {
        val dialog =  Dialog(mContext!!)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog ?.setCancelable(false)
        dialog ?.setContentView(R.layout.dialog_change_password)
        val password = dialog?.findViewById<EditText>(R.id.password)
        val new_password = dialog?.findViewById<EditText>(R.id.new_password)
        val confirm_password = dialog?.findViewById<EditText>(R.id.confirm_new_password)
        val view = dialog?.findViewById<ImageView>(R.id.view)
        val view1 = dialog?.findViewById<ImageView>(R.id.view1)
        val view2 = dialog?.findViewById<ImageView>(R.id.view2)
        val confirm = dialog?.findViewById<Button>(R.id.confirm)
        view?.setOnClickListener { if (password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
            password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
        }
        }
        view1?.setOnClickListener { if (new_password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
            new_password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            new_password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
        }
        }
        view2?.setOnClickListener { if (confirm_password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
            confirm_password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            confirm_password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
        }
        }
        confirm?.setOnClickListener {
            if (CommonUtil.checkEditError(
                    password!!,
                    getString(R.string.Please_enter_the_old_password)
                ) ||
                CommonUtil.checkEditError(
                    new_password!!,
                    getString(R.string.please_new_password)
                ) ||
                CommonUtil.checkLength(new_password, getString(R.string.password_length), 6) ||
                CommonUtil.checkEditError(
                    confirm_password!!,
                    getString(R.string.confirm_new_password)
                )
            ) {
                return@setOnClickListener
            } else {
                if (!confirm_password.text.toString().equals(new_password.text.toString())) {
                    confirm_password.error = getString(R.string.password_not_match)
                } else {
                    showProgressDialog(getString(R.string.please_wait))
                    Client.getClient()?.create(Service::class.java)?.resetPassword(
                        mLanguagePrefManager.appLanguage,
                        mSharedPrefManager.userData.id!!,
                        password?.text.toString(),
                        new_password?.text.toString()
                    )?.enqueue(object : Callback<ResetPasswordModel> {
                        override fun onFailure(call: Call<ResetPasswordModel>, t: Throwable) {
                             CommonUtil.handleException(mContext!!, t)
                            t.printStackTrace()
                            hideProgressDialog()
                            dialog.dismiss()

                        }

                        override fun onResponse(
                            call: Call<ResetPasswordModel>,
                            response: Response<ResetPasswordModel>
                        ) {
                            hideProgressDialog()
                            if (response.isSuccessful) {
                                if (response.body()?.key.equals("1")) {

                                        CommonUtil.makeToast(
                                            mContext!!,
                                            response.body()?.msg!!
                                        )

                                    dialog.dismiss()
                                } else {

                                        CommonUtil.makeToast(
                                            mContext!!,
                                            response.body()?.msg!!

                                        )

                                    dialog.dismiss()
                                }
                            }
                        }

                    })
                }
            }
        }
        dialog ?.show()


    }

    fun getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        )) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.IMAGE_PERMISSIONS,
                        400
                    )
                }
            } else {
                Pix.start(this, options)
                CommonUtil.PrintLogE("Permission is granted before")
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23")
            Pix.start(this, options)
        }
    }
    fun getCities(){

        Client.getClient()?.create(Service::class.java)?.cities(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ListResponse> {

            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()

            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {

                if (response.isSuccessful){
                    if (response.body()!!.key.equals("1")){
                        listAdapter.updateAll(response.body()!!.data!!)
                    }else{

                    }
                }
            }
        } )
    }
    fun getLangs(){

        Client.getClient()?.create(Service::class.java)?.langs(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ListResponse> {

            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()

            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {

                if (response.isSuccessful){
                    if (response.body()!!.key.equals("1")){
                        listAdapter1.updateAll(response.body()!!.data!!)
                    }else{

                    }
                }
            }
        } )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == FILE_REQUEST_CODE && resultCode == AppCompatActivity.RESULT_OK && data != null) {
            files = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES)
            CV.text=files[0].name

        }else  if (requestCode == 100) {
            returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath = returnValue!![0]
            // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
            image!!.setImageURI(Uri.parse(ImageBasePath))
            if (ImageBasePath != null) {
                    updateImage(ImageBasePath!!)
            }
        }
    }
    fun register(efficient:String?){
        showProgressDialog(getString(R.string.please_wait))
        val file = File(files[0].path)
        val requestBody = RequestBody.create(MediaType.parse("*/*"), file)
        val myPart: MultipartBody.Part = MultipartBody.Part.createFormData("cv", file.name, requestBody)
        Client.getClient()?.create(Service::class.java)?.updateUserWithCv(
            mLanguagePrefManager.appLanguage,
            mSharedPrefManager.userData.id!!
            ,name.text.toString()
            ,phone.text.toString()
            ,listModel.id!!
            ,sex!!
            ,date_of_birth.text.toString()
            ,weight.text.toString()
            ,length.text.toString()
            ,myPart
            ,Id_num.text.toString()
            ,education.text.toString()
            ,listModel1.id!!
            ,efficient
        )?.enqueue(object :Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                Log.e("user", Gson().toJson(response.body()?.data))
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        setData(response.body()?.data!!)
                        dialog()
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun Register(efficient:String?){
        showProgressDialog(getString(R.string.please_wait))

        Client.getClient()?.create(Service::class.java)?.updateUserWithoutCv(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,name.text.toString(),phone.text.toString()
            ,listModel.id!!,sex!!,date_of_birth.text.toString(),weight.text.toString(),length.text.toString()
            ,Id_num.text.toString(),education.text.toString(),listModel1.id!!,efficient)?.enqueue(object :Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                Log.e("user", Gson().toJson(response.body()?.data))
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        setData(response.body()?.data!!)
                        dialog()
                    }else{
                         CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun setData(userModel: UserModel){
         Glide.with(mContext!!).load(userModel.avatar).into(image)
        name.text = userModel.name
        user_name.setText(userModel.name)
        phone.setText(userModel.phone)
        Id_num.setText(userModel.idNumber)
        gender.setText(userModel.gender)
        sex = userModel.gender
        date_of_birth.setText(userModel.birth_date)
        weight.setText(userModel.width)
        length.setText(userModel.height)
        education.setText(userModel.education)
        lang.setText(userModel.language)
        listModel1 = ListModel(userModel.language_id,userModel.language)

        city.text = userModel.city_name

        listModel = ListModel(userModel.city_id,userModel.city_name)


    }
    fun getProfile(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProfile(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()


            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        setData(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        DownloadFile().execute(url)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        Log.d(TAG, "Permission has been denied")
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults,
            this@UserProfileFragment
        )
    }

    private inner class DownloadFile : AsyncTask<String, String, String>() {

        private var progressDialog: ProgressDialog? = null
        private var fileName: String? = null
        private var folder: String? = null
        private val isDownloaded: Boolean = false

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        override fun onPreExecute() {
            super.onPreExecute()
            this.progressDialog = ProgressDialog(activity)
            this.progressDialog!!.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
            this.progressDialog!!.setCancelable(false)
            this.progressDialog!!.show()
        }

        /**
         * Downloading file in background thread
         */
        override fun doInBackground(vararg f_url: String): String {
            var count: Int=0
            try {
                val url = URL(f_url[0])
                val connection = url.openConnection()
                connection.connect()
                // getting file length
                val lengthOfFile = connection.contentLength


                // input stream to read file - with 8k buffer
                val input = BufferedInputStream(url.openStream(), 8192)

                val timestamp = SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(Date())

                //Extract file name from URL
                fileName = f_url[0].substring(f_url[0].lastIndexOf('/') + 1, f_url[0].length)

                //Append timestamp to file name
                fileName = timestamp + "_" + fileName

                //External directory path to save file
                folder =
                    Environment.getExternalStorageDirectory().toString() + File.separator + getString(R.string.app_name)+"/"

                //Create androiddeft folder if it does not exist
                val directory = File(folder!!)

                if (!directory.exists()) {
                    directory.mkdirs()
                }

                // Output stream to write file
                val output = FileOutputStream(folder!! + fileName!!)

                val data = ByteArray(1024)

                var total: Long = 0
//                count =input.read(data)

                while (input.read(data)!= -1) {
                    total += count.toLong()
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (total * 100 / lengthOfFile).toInt())
                    Log.d(TAG, "Progress: " + (total * 100 / lengthOfFile).toInt())

                    // writing data to file
                    output.write(data, 0, count)
                }

                // flushing output
                output.flush()

                // closing streams
                output.close()
                input.close()
                return "Downloaded at: $folder$fileName"

            } catch (e: Exception) {
                Log.e("Error: ", e.message)
            }

            return "Something went wrong"
        }

        /**
         * Updating progress bar
         */
        override fun onProgressUpdate(vararg progress: String) {
            // setting progress percentage
            progressDialog!!.progress = Integer.parseInt(progress[0])
        }


        override fun onPostExecute(message: String) {
            // dismiss the dialog after the file was downloaded
            this.progressDialog!!.dismiss()

            // Display File path after downloading
             CommonUtil.makeToast(mContext!!, message)
            Log.e("ddd", message)
        }
    }
    fun updateImage(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = ProgressRequestBody(ImageFile, this@UserProfileFragment)
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.updateUserImage(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,filePart)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()


            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        setData(response.body()?.data!!)
                        dialog()
                    }else{
                       CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }

}