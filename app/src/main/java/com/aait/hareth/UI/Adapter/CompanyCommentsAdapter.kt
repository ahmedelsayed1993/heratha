package com.aait.hareth.UI.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.aait.hareth.Base.ParentRecyclerAdapter
import com.aait.hareth.Base.ParentRecyclerViewHolder
import com.aait.hareth.Models.ComapnyCommentsModel
import com.aait.hareth.Models.CommentModel
import com.aait.hareth.R

class CompanyCommentsAdapter (context: Context, data: MutableList<ComapnyCommentsModel>, layoutId: Int) :
    ParentRecyclerAdapter<ComapnyCommentsModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val comapnyCommentsModel = data.get(position)
        viewHolder.name!!.setText(comapnyCommentsModel.username)
        viewHolder.date!!.setText(comapnyCommentsModel.created)
        viewHolder.comment!!.setText(comapnyCommentsModel.comment)
        viewHolder.rating!!.rating = comapnyCommentsModel.rate!!.toFloat()
        viewHolder.report.visibility = View.GONE




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {

        internal var report=itemView.findViewById<ImageView>(R.id.report)
        internal var comment=itemView.findViewById<TextView>(R.id.comment)
        internal var rating=itemView.findViewById<RatingBar>(R.id.rating)
        internal var date=itemView.findViewById<TextView>(R.id.date)
        internal var name=itemView.findViewById<TextView>(R.id.name)


    }

}