package com.aait.hareth.UI.Activities

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Listener.OnItemClickListener
import com.aait.hareth.Models.ListModel
import com.aait.hareth.Models.ListResponse
import com.aait.hareth.Models.MyJobsModel
import com.aait.hareth.Models.UserResponse
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.UI.Adapter.HomeProviderAdapter
import com.aait.hareth.UI.Adapter.ListAdapter
import com.aait.hareth.UI.Adapter.ProviderOrdersAdapter
import com.aait.hareth.Utils.CommonUtil
import com.aait.hareth.Utils.PermissionUtils
import com.aait.hareth.Utils.ProgressRequestBody
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import gun0912.tedimagepicker.builder.TedImagePicker
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.ArrayList
import java.util.jar.Manifest

class ProviderRegisterActivity :Parent_Activity(),OnItemClickListener {


    override fun onItemClick(view: View, position: Int) {
        listModel = listModels.get(position)
        city.setText(listModel.name!!)
        cities.visibility=View.GONE
    }

    lateinit var type:String
    lateinit var register: Button
    lateinit var name: EditText
    lateinit var phone:EditText
    lateinit var back: ImageView
    lateinit var image: CircleImageView
    lateinit var city: TextView
    lateinit var password:EditText
    lateinit var view:ImageView
    lateinit var check: CheckBox
    lateinit var terms:TextView
    lateinit var press: LinearLayout
    lateinit var cities:RecyclerView
    lateinit var email:EditText
    lateinit var commercial_num:EditText
    var deviceID=""
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var listModels = ArrayList<ListModel>()
    internal lateinit var listAdapter:ListAdapter
    private var ImageBasePath: String? = null
    internal lateinit var listModel:ListModel
    internal var returnValue: ArrayList<String>? = ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")

    override val layoutResource: Int
        get() = R.layout.activity_provider_register

    override fun initializeComponents() {
        type = intent.getStringExtra("type")
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        email = findViewById(R.id.email)
        commercial_num = findViewById(R.id.commercial_num)
        register = findViewById(R.id.register)
        name = findViewById(R.id.name)
        back = findViewById(R.id.back)
        image = findViewById(R.id.image)
        city = findViewById(R.id.city)
        password = findViewById(R.id.password)
        view = findViewById(R.id.view)
        check = findViewById(R.id.check)
        terms = findViewById(R.id.terms)
        press = findViewById(R.id.press)
        cities = findViewById(R.id.cities)
        phone = findViewById(R.id.phone)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter =  ListAdapter(mContext!!,listModels,R.layout.recycler_list)
        listAdapter.setOnItemClickListener(this)
        cities!!.layoutManager= linearLayoutManager
        cities!!.adapter = listAdapter
        getCities()
        back.setOnClickListener { onBackPressed() }
        image.setOnClickListener {
            getPickImageWithPermission()
        }
        view.setOnClickListener { if (password.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
           password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            password.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
        }
        }
        register.setOnClickListener(View.OnClickListener {
            if (ImageBasePath!=null){
                if (CommonUtil.checkEditError(name,getString(R.string.Please_enter_the_entity_name))||
                        CommonUtil.checkEditError(phone,getString(R.string.please_enter_phone))||
                        CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                        CommonUtil.checkEditError(email,getString(R.string.Please_enter_the_email))||
                        !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                        CommonUtil.checkTextError(city,getString(R.string.enter_city))||
                        CommonUtil.checkEditError(commercial_num,getString(R.string.Please_enter_the_commercial_registration_number))||
                        CommonUtil.checkEditError(password,getString(R.string.please_enter_password))||
                        CommonUtil.checkLength(password,getString(R.string.password_length),6)){
                    return@OnClickListener
                }else{
                    if (check.isChecked) {
                        register(ImageBasePath!!)
                    }else{
                        CommonUtil.makeToast(mContext,getString(R.string.terms))
                    }
                }
            }else{
                CommonUtil.makeToast(mContext,getString(R.string.add_image))
            }
        })
        city.setOnClickListener {
            if (cities.visibility ==View.GONE){
                cities.visibility=View.VISIBLE
            }else{
                cities.visibility=View.GONE
            }
        }
    }
    fun getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.CAMERA)&&PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)&&PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                )) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.IMAGE_PERMISSIONS,
                        400
                    )
                }
            } else {
                Pix.start(this, options)
                CommonUtil.PrintLogE("Permission is granted before")
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23")
            Pix.start(this, options)
        }
    }

     override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if (requestCode == 100) {
                returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]
                // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
                image!!.setImageURI(Uri.parse(ImageBasePath))
                if (ImageBasePath != null) {
//                    updateImage(ImageBasePath!!)
                }
            }
        }
    }
    fun getCities(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.cities(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ListResponse>{

            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
               hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()!!.key.equals("1")){
                        listAdapter.updateAll(response.body()!!.data!!)
                    }else{

                    }
                }
            }
        } )
    }

    fun register(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.signUpProvider(mLanguagePrefManager.appLanguage,"provider",name.text.toString(),phone.text.toString()
        ,commercial_num.text.toString(),listModel.id!!,password.text.toString(),email.text.toString(),deviceID,"android",filePart)?.enqueue(object :Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()

            }


            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                Log.e("user",Gson().toJson(response.body()?.data))
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        intent = Intent(this@ProviderRegisterActivity,ActivateAccountActivity::class.java)
                        intent.putExtra("user",response.body()?.data!!)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }

            }

        })
    }
}