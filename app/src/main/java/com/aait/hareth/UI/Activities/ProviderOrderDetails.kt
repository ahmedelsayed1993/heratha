package com.aait.hareth.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Listener.OnItemClickListener
import com.aait.hareth.Models.*
import com.aait.hareth.Models.ProviderOrderDetails
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.UI.Adapter.HomeProviderAdapter
import com.aait.hareth.UI.Adapter.RequestsAdapter
import com.aait.hareth.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProviderOrderDetails:Parent_Activity(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this@ProviderOrderDetails,PresentedDetailsActivity::class.java)
        intent.putExtra("user",requestModels.get(position))
        startActivity(intent)

    }

    lateinit var menu: ImageView
    lateinit var title:TextView
    lateinit var notification: ImageView
    lateinit var special:TextView
    lateinit var salary:TextView
    lateinit var city:TextView
    lateinit var direction:TextView
    lateinit var certificate:TextView
    lateinit var length:TextView
    lateinit var weight:TextView
    lateinit var age:TextView
    lateinit var experiences:TextView
    lateinit var start:TextView
    lateinit var end_date:TextView
    lateinit var status:TextView
    lateinit var order_status:LinearLayout
    lateinit var users:RecyclerView
    lateinit var New:TextView
    lateinit var accepted:TextView
    lateinit var rejected:TextView
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var requestModels = java.util.ArrayList<RequestModel>()
    internal lateinit var requestsAdapter: RequestsAdapter
    lateinit var id :ProviderOrderModel

    var state = "pending"
    override val layoutResource: Int
        get() = R.layout.activity_order_details_provider

    override fun initializeComponents() {
        notification = findViewById(R.id.notification)
        notification.setOnClickListener { startActivity(Intent(this@ProviderOrderDetails,NotificationActivity::class.java)) }
        id = intent.getSerializableExtra("id") as ProviderOrderModel

        Log.e("job_id",Gson().toJson(id))

        status = findViewById(R.id.status)
        order_status = findViewById(R.id.order_status)
        New = findViewById(R.id.New)
        accepted = findViewById(R.id.accepted)
        rejected = findViewById(R.id.rejected)
        users = findViewById(R.id.users)
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        special = findViewById(R.id.special)
        salary = findViewById(R.id.salary)
        city = findViewById(R.id.city)
        direction = findViewById(R.id.direction)
        certificate = findViewById(R.id.certificate)
        length = findViewById(R.id.length)
        weight = findViewById(R.id.weight)
        age = findViewById(R.id.age)
        experiences = findViewById(R.id.experiences)
        start = findViewById(R.id.start)
        end_date = findViewById(R.id.end_date)
        menu.setOnClickListener { onBackPressed() }
        title.text = getString(R.string.Job_details)
        order_status.visibility = View.GONE
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        requestsAdapter = RequestsAdapter(mContext,requestModels,R.layout.recycler_order_requests)
        requestsAdapter.setOnItemClickListener(this)
        users.layoutManager = linearLayoutManager
        users.adapter = requestsAdapter
        status.setOnClickListener {
            if (order_status.visibility == View.GONE){
                order_status.visibility = View.VISIBLE
                status.setBackgroundResource(R.drawable.red_white_shap)
            }else{
                order_status.visibility= View.GONE
                status.setBackgroundResource(R.drawable.white_shape)
            }
        }
        getOrder("pending")
        New.setOnClickListener { state = "pending"
        status.text = getString(R.string.New)
            order_status.visibility = View.GONE
        getOrder("pending")}
        accepted.setOnClickListener { state = "accepted "
        status.text = getString(R.string.accepted)
            order_status.visibility = View.GONE
        getOrder("accepted")}
        rejected.setOnClickListener { state = "rejected "
        status.text = getString(R.string.rejected)
            order_status.visibility = View.GONE
        getOrder("rejected")}

    }
    fun setData(jobDetailsModel: JobDetailsModel){
        special.text = jobDetailsModel.specialty
        city.text = jobDetailsModel.city
        salary.text = jobDetailsModel.salary
        direction.text = jobDetailsModel.direction
        certificate.text = jobDetailsModel.certificates
        length.text= jobDetailsModel.height!!.toString()
        weight.text=(jobDetailsModel.width!!.toString())
        age.text=(jobDetailsModel.age!!.toString())
        experiences.text = jobDetailsModel.experience
        start.text = jobDetailsModel.start_date
        end_date.text = jobDetailsModel.end_date
    }

    fun getOrder(state:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getOrder(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id.id!!,id.job_id!!,state)?.enqueue(object :
            Callback<ProviderOrderDetails>{
            override fun onFailure(call: Call<ProviderOrderDetails>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ProviderOrderDetails>,
                response: Response<ProviderOrderDetails>
            ) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        setData(response.body()?.data!!)
                        if (response.body()?.requests!!.isEmpty()){
                           users.visibility=View.GONE
                        }else{
                            users.visibility=View.VISIBLE
                            requestsAdapter.updateAll(response.body()?.requests!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}