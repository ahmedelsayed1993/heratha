package com.aait.hareth.UI.Fragments

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.hareth.Base.BaseFragment
import com.aait.hareth.Client
import com.aait.hareth.Listener.OnItemClickListener
import com.aait.hareth.Models.*
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.UI.Adapter.ListAdapter
import com.aait.hareth.Utils.CommonUtil
import com.aait.hareth.Utils.PermissionUtils
import com.aait.hareth.Utils.ProgressRequestBody
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.ArrayList

class ProviderProfileFragment:BaseFragment(),OnItemClickListener, ProgressRequestBody.UploadCallbacks {
    override fun onProgressUpdate(percentage: Int) {

    }

    override fun onError() {

    }

    override fun onFinish() {

    }

    override fun onItemClick(view: View, position: Int) {
        listModel = listModels.get(position)
        city.setText(listModel.name!!)
        cities.visibility=View.GONE
    }

    lateinit var image: CircleImageView
    lateinit var name:TextView
    lateinit var rating:RatingBar
    lateinit var company_name:EditText
    lateinit var phone:EditText
    lateinit var email:EditText
    lateinit var city:TextView
    lateinit var cities:RecyclerView
    lateinit var commercial_num:EditText
    lateinit var confirm:Button
    lateinit var change_pass:Button
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var listModels = ArrayList<ListModel>()
    internal lateinit var listAdapter: ListAdapter
    private var ImageBasePath: String? = null
    internal lateinit var listModel: ListModel
    internal var returnValue: ArrayList<String>? = ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")

    companion object {
        fun newInstance(): ProviderProfileFragment {
            val args = Bundle()
            val fragment = ProviderProfileFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    override val layoutResource: Int
        get() = R.layout.fragment_provider_profile

    override fun initializeComponents(view: View) {
        image = view.findViewById(R.id.image)
        name = view.findViewById(R.id.name)
        rating = view.findViewById(R.id.rating)
        company_name = view.findViewById(R.id.company_name)
        phone = view.findViewById(R.id.phone)
        email = view.findViewById(R.id.email)
        city = view.findViewById(R.id.city)
        cities = view.findViewById(R.id.cities)
        confirm = view.findViewById(R.id.confirm)
        change_pass = view.findViewById(R.id.change_pass)
        commercial_num = view.findViewById(R.id.commercial_num)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter =  ListAdapter(mContext!!,listModels,R.layout.recycler_list)
        listAdapter.setOnItemClickListener(this)
        cities!!.layoutManager= linearLayoutManager
        cities!!.adapter = listAdapter
        getCities()
        getProfile()
        change_pass.setOnClickListener {
            showDialog()
            }
        city.setOnClickListener {
            if (cities.visibility ==View.GONE){
                cities.visibility=View.VISIBLE
            }else{
                cities.visibility=View.GONE
            }
        }
        image.setOnClickListener {
            getPickImageWithPermission()
        }
        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(company_name,getString(R.string.Please_enter_the_entity_name))||
                CommonUtil.checkEditError(phone,getString(R.string.please_enter_phone))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkEditError(email,getString(R.string.Please_enter_the_email))||
                !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                CommonUtil.checkTextError(city,getString(R.string.enter_city))||
                CommonUtil.checkEditError(commercial_num,getString(R.string.Please_enter_the_commercial_registration_number))){
               return@setOnClickListener
            }else{
                updateProvider()
            }
        }

    }
    fun setData(userModel:UserModel){
        Glide.with(mContext!!).load(userModel.avatar).into(image)
        name.text = userModel.name
        rating.rating = userModel.rate!!
        company_name.setText(userModel.name)
        phone.setText(userModel.phone)
        email.setText(userModel.email)
        city.text = userModel.city_name
        commercial_num.setText(userModel.commercial_register)
        listModel = ListModel(userModel.city_id,userModel.city_name)


    }
    fun getProfile(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProvider(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()


            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                           setData(response.body()?.data!!)
                    }else{
                         CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getCities(){
        Client.getClient()?.create(Service::class.java)?.cities(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ListResponse>{

            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()

            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {

                if (response.isSuccessful){
                    if (response.body()!!.key.equals("1")){
                        listAdapter.updateAll(response.body()!!.data!!)
                    }else{

                    }
                }
            }
        } )
    }

    fun getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        )) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.IMAGE_PERMISSIONS,
                        400
                    )
                }
            } else {
                Pix.start(this, options)
                CommonUtil.PrintLogE("Permission is granted before")
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23")
            Pix.start(this, options)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if (requestCode == 100) {
                returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]
                // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
                image!!.setImageURI(Uri.parse(ImageBasePath))
                if (ImageBasePath != null) {
                    updateImage(ImageBasePath!!)
                }
            }
        }
    }
    fun dialog(){
        val dialog =  Dialog(mContext!!)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog ?.setCancelable(false)
        dialog ?.setContentView(R.layout.dialog_change)
        val back = dialog?.findViewById<Button>(R.id.back)
        back?.setOnClickListener { dialog?.dismiss() }
        dialog?.show()
    }

    fun updateImage(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = ProgressRequestBody(ImageFile, this@ProviderProfileFragment)
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.updateProviderImage(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,filePart)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()


            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        setData(response.body()?.data!!)
                        dialog()
                    }else{
                         CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun updateProvider(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.updateProvider(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,company_name.text.toString(),phone.text.toString(),commercial_num.text.toString(),listModel.id!!,email.text.toString())
            ?.enqueue(object :Callback<UserResponse>{
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                     CommonUtil.handleException(mContext!!,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
                ) {
                   hideProgressDialog()
                    if(response.isSuccessful){
                        if (response.body()?.key.equals("1")){
                            setData(response.body()?.data!!)
                            dialog()
                        }else{
                             CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
    private fun showDialog() {
        val dialog =  Dialog(mContext!!)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog ?.setCancelable(false)
        dialog ?.setContentView(R.layout.dialog_change_password)
        val password = dialog?.findViewById<EditText>(R.id.password)
        val new_password = dialog?.findViewById<EditText>(R.id.new_password)
        val confirm_password = dialog?.findViewById<EditText>(R.id.confirm_new_password)
        val view = dialog?.findViewById<ImageView>(R.id.view)
        val view1 = dialog?.findViewById<ImageView>(R.id.view1)
        val view2 = dialog?.findViewById<ImageView>(R.id.view2)
        val confirm = dialog?.findViewById<Button>(R.id.confirm)
        view?.setOnClickListener { if (password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
            password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
        }
        }
        view1?.setOnClickListener { if (new_password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
            new_password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            new_password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
        }
        }
        view2?.setOnClickListener { if (confirm_password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
            confirm_password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            confirm_password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
        }
        }
        confirm?.setOnClickListener {
            if (CommonUtil.checkEditError(
                    password!!,
                    getString(R.string.Please_enter_the_old_password)
                ) ||
                CommonUtil.checkEditError(
                    new_password!!,
                    getString(R.string.please_new_password)
                ) ||
                CommonUtil.checkLength(new_password, getString(R.string.password_length), 6) ||
                CommonUtil.checkEditError(
                    confirm_password!!,
                    getString(R.string.confirm_new_password)
                )
            ) {
                return@setOnClickListener
            } else {
                if (!confirm_password.text.toString().equals(new_password.text.toString())) {
                    confirm_password.error = getString(R.string.password_not_match)
                } else {
                    showProgressDialog(getString(R.string.please_wait))
                    Client.getClient()?.create(Service::class.java)?.resetPassword(
                        mLanguagePrefManager.appLanguage,
                        mSharedPrefManager.userData.id!!,
                        password?.text.toString(),
                        new_password?.text.toString()
                    )?.enqueue(object : Callback<ResetPasswordModel> {
                        override fun onFailure(call: Call<ResetPasswordModel>, t: Throwable) {
                             CommonUtil.handleException(mContext!!, t)
                            t.printStackTrace()
                            hideProgressDialog()
                            dialog.dismiss()

                        }

                        override fun onResponse(
                            call: Call<ResetPasswordModel>,
                            response: Response<ResetPasswordModel>
                        ) {
                            hideProgressDialog()
                            if (response.isSuccessful) {
                                if (response.body()?.key.equals("1")) {

                                        CommonUtil.makeToast(
                                            mContext!!,
                                            response.body()?.msg!!
                                        )

                                    dialog.dismiss()
                                } else {

                                        CommonUtil.makeToast(
                                            mContext!!,
                                            response.body()?.msg!!

                                        )

                                    dialog.dismiss()
                                }
                            }
                        }

                    })
                }
            }
        }
        dialog ?.show()

    }
    fun resetPassword(old:String,new:String){

    }
}