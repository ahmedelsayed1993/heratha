package com.aait.hareth.UI.Adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aait.hareth.R
import com.aait.hareth.UI.Fragments.*

class OrderTapAdapter (
    private val context: Context,
    fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            UnderReviewFragment()
        } else if (position == 1){
            AcceptedOrdersFragment()
        }else{
            RejectedOrdersFragment()
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.Under_review)
        } else if (position == 1){
            context.getString(R.string.Accepted)
        }else{
            context.getString(R.string.Rejected)
        }
    }
}
