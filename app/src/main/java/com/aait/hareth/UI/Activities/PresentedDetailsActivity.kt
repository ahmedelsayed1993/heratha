package com.aait.hareth.UI.Activities

import android.Manifest
import android.app.ProgressDialog
import android.content.Intent
import android.os.AsyncTask
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.NonNull
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Models.RequestDetailsModel
import com.aait.hareth.Models.RequestDetailsResponse
import com.aait.hareth.Models.RequestModel
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.Utils.CheckForSDCard
import com.aait.hareth.Utils.CommonUtil
import com.bumptech.glide.Glide
import com.google.gson.Gson

import pub.devrel.easypermissions.EasyPermissions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedInputStream
import java.io.File
import java.io.FileOutputStream
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

class PresentedDetailsActivity:Parent_Activity(), EasyPermissions.PermissionCallbacks {
    lateinit var menu: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var requestModel:RequestModel
    lateinit var image:ImageView
    lateinit var name:TextView
    lateinit var date:TextView
    lateinit var address:TextView
    lateinit var user_name:TextView
    lateinit var city:TextView
    lateinit var sex:TextView
    lateinit var date_of_birth:TextView
    lateinit var length:TextView
    lateinit var weight:TextView
    lateinit var CV:TextView
    lateinit var lay:LinearLayout
    lateinit var accept:Button
    lateinit var refuse:Button
    lateinit var refuse_lay:LinearLayout
    lateinit var reason:TextView
    lateinit var Id_num:TextView
    lateinit var education:TextView
    lateinit var level:TextView
    lateinit var second_lang:TextView
    private var url: String? = null
    private val WRITE_REQUEST_CODE = 300
    private val TAG = PresentedDetailsActivity::class.java!!.getSimpleName()

    override val layoutResource: Int
        get() = R.layout.activity_presented_details

    override fun initializeComponents() {
        notification = findViewById(R.id.notification)
        notification.setOnClickListener { startActivity(Intent(this@PresentedDetailsActivity,NotificationActivity::class.java)) }
        requestModel = intent.getSerializableExtra("user") as RequestModel
        Log.e("user",Gson().toJson(requestModel))
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        menu.setOnClickListener { onBackPressed() }
        title.text = getString(R.string.Job_seeker_details)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        date = findViewById(R.id.date)
        address = findViewById(R.id.address)
        user_name = findViewById(R.id.user_name)
        city = findViewById(R.id.city)
        sex = findViewById(R.id.sex)
        date_of_birth = findViewById(R.id.date_of_birth)
        length = findViewById(R.id.length)
        weight = findViewById(R.id.weight)
        CV = findViewById(R.id.CV)
        lay = findViewById(R.id.lay)
        Id_num = findViewById(R.id.Id_num)
        education = findViewById(R.id.education)
        level = findViewById(R.id.level)
        second_lang = findViewById(R.id.second_lang)
        accept = findViewById(R.id.accept)
        refuse = findViewById(R.id.refuse)
        refuse_lay = findViewById(R.id.refuse_lay)
        reason = findViewById(R.id.reason)
        getRequest(null)
        accept.setOnClickListener { getRequest() }
        refuse.setOnClickListener { val intent = Intent(this@PresentedDetailsActivity,RefuseRequestActivity::class.java)
        intent.putExtra("user",requestModel)
        startActivity(intent)}
        CV.setOnClickListener {
            if (CheckForSDCard.isSDCardPresent) {

                //check if app has permission to write to the external storage.
                if (EasyPermissions.hasPermissions(
                        this@PresentedDetailsActivity,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                ) {
                    //Get the URL entered

                    DownloadFile().execute(url)

                } else {
                    //If permission is not present request for the same.
                    EasyPermissions.requestPermissions(
                        this@PresentedDetailsActivity,
                        mContext.getString(R.string.acess_storage),
                        WRITE_REQUEST_CODE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                }


            } else {
                Toast.makeText(
                    applicationContext,
                    "SD Card not found", Toast.LENGTH_LONG
                ).show()

            }
        }

    }
    fun setData(requestDetailsModel: RequestDetailsModel){
        Glide.with(mContext).load(requestDetailsModel.avatar).into(image)
        name.text=requestDetailsModel.name
        date.text = requestModel.created
        address.text = requestDetailsModel.city_name
        user_name.text=requestDetailsModel.name
        city.text = requestDetailsModel.city_name
        sex.text = requestDetailsModel.gender
        date_of_birth.text = requestDetailsModel.birth_date
        weight.text = requestDetailsModel.width
        length.text = requestDetailsModel.height
        reason.text = requestDetailsModel.reason
        second_lang.text = requestDetailsModel.language
        education.text = requestDetailsModel.education
        level.text = requestDetailsModel.efficiency
        Id_num.text = requestDetailsModel.idNumber
        url = requestDetailsModel.cv
        if (requestDetailsModel.status.equals("accepted")){
            lay.visibility = View.GONE
            refuse_lay.visibility = View.GONE
        }else if (requestDetailsModel.status.equals("rejected")){
            lay.visibility = View.GONE
            refuse_lay.visibility = View.VISIBLE
        }else{
            lay.visibility = View.VISIBLE
            refuse_lay.visibility = View.GONE
        }

    }

    fun getRequest(action:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getRequest(mLanguagePrefManager.appLanguage,requestModel.id!!,mSharedPrefManager.userData.id!!,action,null)
            ?.enqueue(object : Callback<RequestDetailsResponse>{
                override fun onFailure(call: Call<RequestDetailsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<RequestDetailsResponse>,
                    response: Response<RequestDetailsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.key.equals("1")){

                            setData(response.body()?.data!!)

                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
    fun getRequest(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getRequest(mLanguagePrefManager.appLanguage,requestModel.id!!,mSharedPrefManager.userData.id!!,"accepted",null)
            ?.enqueue(object : Callback<RequestDetailsResponse>{
                override fun onFailure(call: Call<RequestDetailsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<RequestDetailsResponse>,
                    response: Response<RequestDetailsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.key.equals("1")){

                            onBackPressed()

                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        DownloadFile().execute(url)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        Log.d(TAG, "Permission has been denied")
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults,
            this@PresentedDetailsActivity
        )
    }

    private inner class DownloadFile : AsyncTask<String, String, String>() {

        private var progressDialog: ProgressDialog? = null
        private var fileName: String? = null
        private var folder: String? = null
        private val isDownloaded: Boolean = false

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        override fun onPreExecute() {
            super.onPreExecute()
            this.progressDialog = ProgressDialog(this@PresentedDetailsActivity)
            this.progressDialog!!.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
            this.progressDialog!!.setCancelable(false)
            this.progressDialog!!.show()
        }

        /**
         * Downloading file in background thread
         */
        override fun doInBackground(vararg f_url: String): String {
            var count: Int=0
            try {
                val url = URL(f_url[0])
                val connection = url.openConnection()
                connection.connect()
                // getting file length
                val lengthOfFile = connection.contentLength


                // input stream to read file - with 8k buffer
                val input = BufferedInputStream(url.openStream(), 8192)

                val timestamp = SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(Date())

                //Extract file name from URL
                fileName = f_url[0].substring(f_url[0].lastIndexOf('/') + 1, f_url[0].length)

                //Append timestamp to file name
                fileName = timestamp + "_" + fileName

                //External directory path to save file
                folder =
                    Environment.getExternalStorageDirectory().toString() + File.separator + getString(R.string.app_name)+"/"

                //Create androiddeft folder if it does not exist
                val directory = File(folder!!)

                if (!directory.exists()) {
                    directory.mkdirs()
                }

                // Output stream to write file
                val output = FileOutputStream(folder!! + fileName!!)

                val data = ByteArray(1024)

                var total: Long = 0
//                count =input.read(data)

                while (input.read(data)!= -1) {
                    total += count.toLong()
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (total * 100 / lengthOfFile).toInt())
                    Log.d(TAG, "Progress: " + (total * 100 / lengthOfFile).toInt())

                    // writing data to file
                    output.write(data, 0, count)
                }

                // flushing output
                output.flush()

                // closing streams
                output.close()
                input.close()
                return "Downloaded at: $folder$fileName"

            } catch (e: Exception) {
                Log.e("Error: ", e.message)
            }

            return "Something went wrong"
        }

        /**
         * Updating progress bar
         */
        override fun onProgressUpdate(vararg progress: String) {
            // setting progress percentage
            progressDialog!!.progress = Integer.parseInt(progress[0])
        }


        override fun onPostExecute(message: String) {
            // dismiss the dialog after the file was downloaded
            this.progressDialog!!.dismiss()

            // Display File path after downloading
            CommonUtil.makeToast(mContext, message)
            Log.e("ddd", message)
        }
    }
}