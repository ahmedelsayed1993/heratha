package com.aait.hareth.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Models.SettingsModel
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContactUsActivity : Parent_Activity(){
    lateinit var menu: ImageView
    lateinit var title: TextView
    lateinit var logo: ImageView
    lateinit var user_name:EditText
    lateinit var phone_number:EditText
    lateinit var message:EditText
    lateinit var send:Button
    lateinit var notification:ImageView
    lateinit var search:ImageView
    override val layoutResource: Int
        get() = R.layout.activity_contact_us

    override fun initializeComponents() {
        search = findViewById(R.id.search)
        search.visibility =View.GONE
        notification = findViewById(R.id.notification)
        notification.setOnClickListener { startActivity(Intent(this@ContactUsActivity,NotificationActivity::class.java)) }
        menu = findViewById(R.id.menu)
        logo = findViewById(R.id.logo)
        title = findViewById(R.id.title)
        logo.visibility = View.GONE
        title.visibility = View.VISIBLE
        title.setText(R.string.contact_us)
        user_name = findViewById(R.id.user_name)
        phone_number = findViewById(R.id.phone_number)
        message = findViewById(R.id.message)
        send = findViewById(R.id.send)

        menu.setOnClickListener { onBackPressed() }
        send.setOnClickListener { if (CommonUtil.checkEditError(user_name,getString(R.string.user_name))||
                CommonUtil.checkEditError(phone_number,getString(R.string.phone_number))||
                CommonUtil.checkEditError(message,getString(R.string.write_message))){
        return@setOnClickListener}else{
            contact()
        }
        }
    }

    fun contact(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.contact(mLanguagePrefManager.appLanguage,user_name.text.toString(),phone_number.text.toString(),message.text.toString())?.enqueue(object :
            Callback<SettingsModel>{
            override fun onFailure(call: Call<SettingsModel>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<SettingsModel>, response: Response<SettingsModel>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                    }else{

                    }
                }
            }
        })
    }





}