package com.aait.hareth.UI.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.hareth.Base.ParentRecyclerAdapter
import com.aait.hareth.Base.ParentRecyclerViewHolder
import com.aait.hareth.Models.MyJobsModel
import com.aait.hareth.Models.ProviderOrderModel
import com.aait.hareth.R

class ProviderOrdersAdapter (context: Context, data: MutableList<ProviderOrderModel>, layoutId: Int) :
    ParentRecyclerAdapter<ProviderOrderModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ProviderOrdersAdapter.ViewHolder
        val providerOrderModel = data.get(position)
        viewHolder.title!!.setText(providerOrderModel.title)
        viewHolder.special!!.setText(providerOrderModel.specialty)
        viewHolder.date!!.setText(providerOrderModel.created)
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view ->
                        onItemClickListener.onItemClick(
                view,
                position
            ) })

    }

    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {

        internal var title=itemView.findViewById<TextView>(R.id.title)
        internal var special=itemView.findViewById<TextView>(R.id.special)

        internal var date=itemView.findViewById<TextView>(R.id.date)

    }
}