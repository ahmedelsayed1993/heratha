package com.aait.hareth.UI.Activities

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Models.BanksModel
import com.aait.hareth.Models.BanksResponse
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.UI.Adapter.BanksAdapter
import com.aait.hareth.Utils.CommonUtil
import com.aait.hareth.Utils.PermissionUtils
import com.aait.hareth.Utils.ProgressRequestBody
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class AccountsActivity:Parent_Activity() , ProgressRequestBody.UploadCallbacks {
    override fun onProgressUpdate(percentage: Int) {

    }

    override fun onError() {

    }

    override fun onFinish() {

    }
    private var ImageBasePath: String? = null
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")

    lateinit var accounts:RecyclerView
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var banksAdapter: BanksAdapter
    internal var banksModels = ArrayList<BanksModel>()
    lateinit var name:EditText
    lateinit var bank_name:EditText
    lateinit var amount:EditText
    lateinit var menu: ImageView
    lateinit var title: TextView
    lateinit var logo: ImageView
    lateinit var search: ImageView
    lateinit var notification: ImageView
    lateinit var image:ImageView
    lateinit var send:Button
    override val layoutResource: Int
        get() = R.layout.activity_bank_accounts

    override fun initializeComponents() {
        notification = findViewById(R.id.notification)
        notification.setOnClickListener { startActivity(Intent(this@AccountsActivity,NotificationActivity::class.java)) }
        menu = findViewById(R.id.menu)
        logo = findViewById(R.id.logo)
        title = findViewById(R.id.title)
        logo.visibility = View.GONE
        title.visibility = View.VISIBLE
        search = findViewById(R.id.search)
        search.visibility = View.GONE
        notification = findViewById(R.id.notification)
        title.text = getString(R.string.Bank_accounts)
        menu.setImageResource(R.mipmap.lefarrow)
        bank_name = findViewById(R.id.bank_name)
        amount = findViewById(R.id.amount)
        name = findViewById(R.id.name)
        image = findViewById(R.id.image)
        send = findViewById(R.id.send)
        accounts = findViewById(R.id.accounts)
        menu.setOnClickListener { onBackPressed() }
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        banksAdapter = BanksAdapter(mContext,banksModels,R.layout.recycler_banks)
        accounts.layoutManager = linearLayoutManager
        accounts.adapter = banksAdapter
        getBanks()
        image.setOnClickListener { getPickImageWithPermission() }
        send.setOnClickListener {
            if (CommonUtil.checkEditError(name,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkEditError(bank_name,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkEditError(amount,getString(R.string.sorry_fill_Empty_form))){
                return@setOnClickListener
            }else{
                if (ImageBasePath!=null){
                    transfer(ImageBasePath!!)

                }else{
                    CommonUtil.makeToast(mContext,getString(R.string.Add_photos))
                }
            }
        }

    }
    fun transfer(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = ProgressRequestBody(ImageFile, this@AccountsActivity)
        filePart = MultipartBody.Part.createFormData("image", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.transfer(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,name.text.toString(),
            bank_name.text.toString(),amount.text.toString(),filePart)?.enqueue(object :Callback<BanksResponse>{
            override fun onFailure(call: Call<BanksResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<BanksResponse>, response: Response<BanksResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        onBackPressed()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getBanks(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.banks(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<BanksResponse>{
            override fun onFailure(call: Call<BanksResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<BanksResponse>, response: Response<BanksResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        banksAdapter.updateAll(response.body()?.data!!)
                    }else{

                    }
                }
            }

        })
    }
    fun getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        )) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.IMAGE_PERMISSIONS,
                        400
                    )
                }
            } else {
                Pix.start(this, options)
                CommonUtil.PrintLogE("Permission is granted before")
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23")
            Pix.start(this, options)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if (requestCode == 100) {
                returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]
                // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
                image!!.setImageURI(Uri.parse(ImageBasePath))
                if (ImageBasePath != null) {
//                    updateImage(ImageBasePath!!)
                }
            }
        }
    }
}