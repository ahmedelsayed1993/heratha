package com.aait.hareth.UI.Activities


import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Models.ResendCodeModel
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FinancialActivity:Parent_Activity() {
    lateinit var menu: ImageView
    lateinit var title: TextView
    lateinit var logo: ImageView
    lateinit var search:ImageView
    lateinit var notification: ImageView
    lateinit var amount:TextView
    lateinit var pay:Button
    override val layoutResource: Int
        get() = R.layout.activity_financial

    override fun initializeComponents() {
        search = findViewById(R.id.search)
        search.visibility =View.GONE
        notification = findViewById(R.id.notification)
        notification.setOnClickListener { startActivity(Intent(this@FinancialActivity,NotificationActivity::class.java)) }
        menu = findViewById(R.id.menu)
        logo = findViewById(R.id.logo)
        title = findViewById(R.id.title)
        logo.visibility = View.GONE
        title.visibility = View.VISIBLE
        search = findViewById(R.id.search)
        notification = findViewById(R.id.notification)
        amount = findViewById(R.id.amount)
        pay = findViewById(R.id.pay)
        title.setText(R.string.Financial_accounts)
        menu.setOnClickListener { onBackPressed() }
        pay.setOnClickListener { startActivity(Intent(this@FinancialActivity,AccountsActivity::class.java)) }
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.financial(mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<ResendCodeModel>{
            override fun onFailure(call: Call<ResendCodeModel>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ResendCodeModel>,
                response: Response<ResendCodeModel>
            ) {
                hideProgressDialog()
                if (response.isSuccessful)
                {
                    if (response.body()?.key.equals("1")){
                        amount.text = response.body()?.data!!+"  "+getString(R.string.rs)
                        if (response.body()?.data.equals("")){
                            pay.visibility=View.GONE
                        }else{
                            pay.visibility = View.VISIBLE
                        }
                    }else{
                        amount.text = ""
                    }
                }
            }

        })

    }
}