package com.aait.hareth.UI.Fragments

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.hareth.Base.BaseFragment
import com.aait.hareth.Client
import com.aait.hareth.Listener.OnItemClickListener
import com.aait.hareth.Models.UserOrdersModels
import com.aait.hareth.Models.UserOrdersResponse
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.UI.Activities.UserOrderDetailsActivity
import com.aait.hareth.UI.Adapter.UserOrdersAdapter
import com.aait.hareth.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RejectedOrdersFragment: BaseFragment(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(activity, UserOrderDetailsActivity::class.java)
        intent.putExtra("order",userOrderModels.get(position))
        startActivity(intent)

    }

    lateinit var rv_recycle: RecyclerView

    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    internal var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var userOrderModels = java.util.ArrayList<UserOrdersModels>()
    internal lateinit var userOrdersAdapter: UserOrdersAdapter
    override val layoutResource: Int
        get() = R.layout.fragment_client_orders

    override fun initializeComponents(view: View) {
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        userOrdersAdapter =  UserOrdersAdapter(mContext!!,userOrderModels,R.layout.recycler_user_order)
        userOrdersAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = userOrdersAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome() }
        getHome()

    }

    fun getHome(){

        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.getUserOrders(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,"rejected")?.enqueue(object :
            Callback<UserOrdersResponse> {
            override fun onResponse(call: Call<UserOrdersResponse>, response: Response<UserOrdersResponse>) {
                swipeRefresh!!.isRefreshing = false

                if (response.isSuccessful) {
                    try {
                        if (response.body()?.key.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                userOrdersAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {}

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<UserOrdersResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false

                Log.e("response", Gson().toJson(t))
            }
        })
    }
}