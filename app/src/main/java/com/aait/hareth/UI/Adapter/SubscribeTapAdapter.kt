package com.aait.hareth.UI.Adapter

import android.content.Context

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

import com.aait.hareth.R
import com.aait.hareth.UI.Fragments.CommentsFragment
import com.aait.hareth.UI.Fragments.JobsFragment


class SubscribeTapAdapter(
    private val context: Context,
    fm: FragmentManager,
    internal var id: Int
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            JobsFragment.newInstance(id)
        } else {
            CommentsFragment.newInstance(id)
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.Available_jobs)
        } else {
            context.getString(R.string.Watch_the_comments)
        }
    }
}
