package com.aait.hareth.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Models.UserResponse
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RestorePasswordActivity : Parent_Activity() {
    lateinit var confirm:Button
    lateinit var back:ImageView
    lateinit var phone:EditText

    override val layoutResource: Int
        get() =  R.layout.activity_restore_password

    override fun initializeComponents() {
        confirm = findViewById(R.id.confirm)
        phone = findViewById(R.id.phone)

        confirm.setOnClickListener(View.OnClickListener {
            if (CommonUtil.checkEditError(phone,getString(R.string.please_enter_phone))){
                return@OnClickListener
            }else{
                forgot()
            }
        })
    }
    fun forgot(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.forgotPass(phone.text.toString(),mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        intent = Intent(this@RestorePasswordActivity,ValidationCodeActivity::class.java)
                        intent.putExtra("user",response.body()?.data!!)
                        startActivity(intent)
                    }
                }
            }

        })
    }




}