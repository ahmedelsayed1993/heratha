package com.aait.hareth.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.Button
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.R

class PreLoginActivity : Parent_Activity() {
    lateinit var seeker :Button
    lateinit var companies : Button
    override val layoutResource: Int
        get() = R.layout.activity_pre_login

    override fun initializeComponents() {
        seeker = findViewById(R.id.seek)
        companies = findViewById(R.id.companies)

        seeker.setOnClickListener(View.OnClickListener {
           val intent=Intent(this@PreLoginActivity,SeekerLoginActivity::class.java)
            intent.putExtra("type","user")
            startActivity(intent)
            finish()

        })
        companies.setOnClickListener(View.OnClickListener {
            val intent=Intent(this@PreLoginActivity,SeekerLoginActivity::class.java)
            intent.putExtra("type","provider")
            startActivity(intent)
            finish()
        })
    }





}