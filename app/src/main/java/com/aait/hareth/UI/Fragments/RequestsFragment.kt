package com.aait.hareth.UI.Fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager
import com.aait.hareth.Base.BaseFragment

import com.aait.hareth.R
import com.aait.hareth.UI.Adapter.OrderTapAdapter
import com.aait.hareth.UI.Adapter.SubscribeTapAdapter
import com.google.android.material.tabs.TabLayout

class RequestsFragment : BaseFragment(){
    lateinit var orders:TabLayout
    lateinit var ordersViewPager:ViewPager
    private var mAdapter: OrderTapAdapter? = null
    companion object {
                fun newInstance(): RequestsFragment {
            val args = Bundle()
            val fragment = RequestsFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    override fun initializeComponents(view: View) {
        orders = view.findViewById(R.id.orders)
        ordersViewPager = view.findViewById(R.id.ordersViewPager)
        mAdapter = OrderTapAdapter(mContext!!, childFragmentManager)
        ordersViewPager.setAdapter(mAdapter)
        orders!!.setupWithViewPager(ordersViewPager)
        Log.e("fff","fff")
    }

    override val layoutResource: Int
        get() = R.layout.fragment_user_requests





}