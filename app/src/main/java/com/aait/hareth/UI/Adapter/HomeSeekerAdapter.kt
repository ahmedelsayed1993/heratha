package com.aait.hareth.UI.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.aait.hareth.Base.ParentRecyclerAdapter
import com.aait.hareth.Base.ParentRecyclerViewHolder
import com.aait.hareth.Models.JobModel
import com.aait.hareth.R
import com.bumptech.glide.Glide

class HomeSeekerAdapter(context: Context, data: MutableList<JobModel>, layoutId: Int) :
    ParentRecyclerAdapter<JobModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as HomeSeekerAdapter.ViewHolder
        val jobModel = data.get(position)
        viewHolder.city!!.setText(jobModel.city)
        viewHolder.name!!.setText(jobModel.name)
        viewHolder.rate!!.text=""+jobModel.rate
        Glide.with(mcontext).load(jobModel.image).into(viewHolder.image)
        viewHolder.rating!!.rating= jobModel.rate!!
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })
    }

    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {




      internal  var image=itemView.findViewById<ImageView>(R.id.image)
        internal var name=itemView.findViewById<TextView>(R.id.name)
       internal var city=itemView.findViewById<TextView>(R.id.city)
        internal var rating=itemView.findViewById<RatingBar>(R.id.rating)
       internal var rate=itemView.findViewById<TextView>(R.id.rate)

    }
}