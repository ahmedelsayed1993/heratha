package com.aait.hareth.UI.Activities

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.*
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Models.ResendCodeModel
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.R.*
import com.aait.hareth.SplashActivity
import com.aait.hareth.UI.Fragments.RequestsFragment
import com.aait.hareth.UI.Fragments.SeekerHomeFragment
import com.aait.hareth.UI.Fragments.SettingsFragment
import com.aait.hareth.UI.Fragments.UserProfileFragment
import com.aait.hareth.Utils.CommonUtil
import com.bumptech.glide.Glide
import com.google.firebase.iid.FirebaseInstanceId
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class MainActivity : Parent_Activity() {


    lateinit var home:LinearLayout
    lateinit var request:LinearLayout
    lateinit var profile:LinearLayout
    lateinit var setting:LinearLayout
    lateinit var home_text :TextView
    lateinit var home_image:ImageView
    lateinit var profile_text :TextView
    lateinit var profile_image:ImageView
    lateinit var request_text :TextView
    lateinit var request_image:ImageView
    lateinit var setting_text :TextView
    lateinit var setting_image:ImageView
    lateinit var menu:ImageView
    lateinit var menu_back:ImageView
    lateinit var title:TextView
    lateinit var logo:ImageView
    lateinit var about_app:TextView
    lateinit var terms:TextView
    lateinit var repeated:TextView
    lateinit var contact_us:TextView
    lateinit var complains:TextView
    lateinit var share_app:TextView
    lateinit var main:TextView
    lateinit var notification:ImageView
    lateinit var image: CircleImageView
    lateinit var name:TextView
    lateinit var logout:TextView
    lateinit var search:ImageView
    protected var selectedTab = 0
    private val tabsStack = Stack<Int>()

    private var fragmentManager: FragmentManager? = null

    private var transaction: FragmentTransaction? = null
    internal lateinit var seekerHomeFragment: SeekerHomeFragment
    internal lateinit var requestsFragment: RequestsFragment
    internal lateinit var settingsFragment: SettingsFragment
    internal lateinit var userProfileFragment: UserProfileFragment
    var deviceID=""
    var selected = 1
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    override val layoutResource: Int
        get() = R.layout.activity_main

    override fun initializeComponents() {
        search = findViewById(R.id.search)
        search.setOnClickListener { startActivity(Intent(this@MainActivity,ClientSearchActivity::class.java)) }
        notification = findViewById(id.notification)
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        notification.setOnClickListener {
            if (mSharedPrefManager.loginStatus!!) {
                startActivity(Intent(this@MainActivity, NotificationActivity::class.java))
            }else{
                visitor()
            }}
        seekerHomeFragment = SeekerHomeFragment.newInstance()
        requestsFragment = RequestsFragment.newInstance()
        userProfileFragment = UserProfileFragment.newInstance()
        settingsFragment = SettingsFragment.newInstance()
        fragmentManager = getSupportFragmentManager()
        transaction = fragmentManager!!.beginTransaction()
        transaction!!.add(id.home_fragment_container, seekerHomeFragment)
        transaction!!.add(id.home_fragment_container, requestsFragment)
        transaction!!.add(id.home_fragment_container,userProfileFragment)
        transaction!!.add(id.home_fragment_container,settingsFragment)
        transaction!!.commit()
        home = findViewById(id.home)
        request = findViewById(id.requests)
        profile = findViewById(id.profile)
        setting = findViewById(id.setting)
        home_image = findViewById(id.home_image)
        home_text = findViewById(id.home_text)
        request_image = findViewById(id.request_image)
        request_text = findViewById(id.request_text)
        profile_image = findViewById(id.profile_image)
        profile_text = findViewById(id.profile_text)
        setting_image = findViewById(id.setting_image)
        setting_text = findViewById(id.setting_text)
        title = findViewById(id.title)
        logo = findViewById(id.logo)
        menu = findViewById(id.menu)
        menu_back = findViewById(id.menu_back)
        showhome()
        sideMenu()
        home.setOnClickListener(View.OnClickListener {
            showhome()
        })
        request.setOnClickListener(View.OnClickListener {
            if (mSharedPrefManager.loginStatus!!) {
                Requests()
            }else{
                visitor()
            }
        })
        profile.setOnClickListener(View.OnClickListener {
            if (mSharedPrefManager.loginStatus!!) {
                profile()
            }else{
                visitor()
            }
        })
        setting.setOnClickListener(View.OnClickListener {
            if (mSharedPrefManager.loginStatus!!) {
                settings()
            }else{
                visitor()
            }
        })

    }


    fun sideMenu(){

            drawer_layout.useCustomBehavior(Gravity.START)
            drawer_layout.useCustomBehavior(Gravity.END)
            drawer_layout.setRadius(Gravity.START, 25f)
            drawer_layout.setRadius(Gravity.END, 25f)
            drawer_layout.setViewScale(
                Gravity.START,
                0.9f
            ) //set height scale for main view (0f to 1f)
            drawer_layout.setViewScale(
                Gravity.END,
                0.9f
            ) //set height scale for main view (0f to 1f)
            drawer_layout.setViewElevation(
                Gravity.START,
                20f
            ) //set main view elevation when drawer open (dimension)
            drawer_layout.setViewElevation(
                Gravity.END,
                20f
            ) //set main view elevation when drawer open (dimension)
            drawer_layout.setViewScrimColor(
                Gravity.START,
                Color.TRANSPARENT
            ) //set drawer overlay coloe (color)
            drawer_layout.setViewScrimColor(
                Gravity.END,
                Color.TRANSPARENT
            ) //set drawer overlay coloe (color)
            drawer_layout.setDrawerElevation(Gravity.START, 20f) //set drawer elevation (dimension)
            drawer_layout.setDrawerElevation(Gravity.END, 20f) //set drawer elevation (dimension)
            drawer_layout.setContrastThreshold(3f) //set maximum of contrast ratio between white text and background color.
            drawer_layout.setRadius(
                Gravity.START,
                40f
            ) //set end container's corner radius (dimension)
            drawer_layout.setRadius(Gravity.END, 40f)

            menu.setOnClickListener { drawer_layout.openDrawer(GravityCompat.START) }
            menu_back.setOnClickListener { drawer_layout.closeDrawer(GravityCompat.START) }
            logout = drawer_layout.findViewById(id.logout)
            image = drawer_layout.findViewById(id.image)
            name = drawer_layout.findViewById(id.name)
            main = drawer_layout.findViewById(id.main)
            about_app = drawer_layout.findViewById(id.about_app)
            terms = drawer_layout.findViewById(id.terms)
            repeated = drawer_layout.findViewById(id.repeated)
            contact_us = drawer_layout.findViewById(id.contact_us)
            complains = drawer_layout.findViewById(id.complains)
            share_app = drawer_layout.findViewById(id.share)
        if (mSharedPrefManager.loginStatus!!) {
            Glide.with(mContext).load(mSharedPrefManager.userData.avatar).into(image)
            name.text = mSharedPrefManager.userData.name
        }else{
            name.visibility = View.GONE
            image.visibility = View.GONE
        }
            main.setOnClickListener { drawer_layout.closeDrawer(GravityCompat.START) }
            about_app.setOnClickListener {
                if (mSharedPrefManager.loginStatus!!) {
                    startActivity(
                        Intent(
                            this@MainActivity,
                            AboutAppActivity::class.java
                        )
                    )
                }else{
                    visitor()
                }
            }
            terms.setOnClickListener {
                if (mSharedPrefManager.loginStatus!!) {
                    startActivity(
                        Intent(
                            this@MainActivity,
                            TermsActivity::class.java
                        )
                    )
                }else{
                    visitor()
                }
            }
            repeated.setOnClickListener {
                if (mSharedPrefManager.loginStatus!!) {
                    startActivity(
                        Intent(
                            this@MainActivity,
                            RepeatedQuestionsActivity::class.java
                        )
                    )
                }else{
                    visitor()
                }
            }
            contact_us.setOnClickListener {
                if (mSharedPrefManager.loginStatus!!) {
                    startActivity(
                        Intent(
                            this@MainActivity,
                            ContactUsActivity::class.java
                        )
                    )
                }else{
                    visitor()
                }
            }
            complains.setOnClickListener {
                if (mSharedPrefManager.loginStatus!!) {
                    startActivity(
                        Intent(
                            this@MainActivity,
                            ComplainsActivity::class.java
                        )
                    )
                }else{
                    visitor()
                }
            }
            share_app.setOnClickListener { CommonUtil.ShareApp(applicationContext) }
            logout.setOnClickListener {
                if (mSharedPrefManager.loginStatus!!) {
                    logout()
                }else{
                    visitor()
                }}


    }
    fun showhome(){
        selected = 1
        home_image.setImageResource(mipmap.noun_homeacti)
        home_text.textColor = Color.parseColor("#EA3434")
        request_text.textColor = Color.parseColor("#989898")
        profile_text.textColor = Color.parseColor("#989898")
        setting_text.textColor = Color.parseColor("#989898")
        profile_image.setImageResource(mipmap.user)
        request_image.setImageResource(mipmap.choices)
        setting_image.setImageResource(mipmap.noun_etting)
        logo.visibility = View.VISIBLE
        title.visibility = View.GONE
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, seekerHomeFragment)
        transaction!!.commit()
    }
    fun Requests(){
        selected = 2
        home_image.setImageResource(mipmap.noun_home)
        request_text.textColor = Color.parseColor("#EA3434")
        profile_text.textColor = Color.parseColor("#989898")
        setting_text.textColor = Color.parseColor("#989898")
        home_text.textColor = Color.parseColor("#989898")
        profile_image.setImageResource(mipmap.user)
        request_image.setImageResource(mipmap.choicesacti)
        setting_image.setImageResource(mipmap.noun_etting)
        logo.visibility = View.GONE
        title.visibility = View.VISIBLE
        title.setText(string.Requests)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, requestsFragment)
        transaction!!.commit()
    }
    fun profile(){
        selected = 2
        home_image.setImageResource(mipmap.noun_home)
        profile_text.textColor = Color.parseColor("#EA3434")
        request_text.textColor = Color.parseColor("#989898")
        setting_text.textColor = Color.parseColor("#989898")
        home_text.textColor = Color.parseColor("#989898")
        profile_image.setImageResource(mipmap.useracti)
        request_image.setImageResource(mipmap.choices)
        setting_image.setImageResource(mipmap.noun_etting)
        logo.visibility = View.GONE
        title.visibility = View.VISIBLE
        title.setText(string.My_profile)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, userProfileFragment)
        transaction!!.commit()
    }
    fun settings(){
        selected = 2
        home_image.setImageResource(mipmap.noun_home)
        setting_text.textColor = Color.parseColor("#EA3434")
        profile_text.textColor = Color.parseColor("#989898")
        request_text.textColor = Color.parseColor("#989898")
        home_text.textColor = Color.parseColor("#989898")
        profile_image.setImageResource(mipmap.user)
        request_image.setImageResource(mipmap.choices)
        setting_image.setImageResource(mipmap.noun_settings)
        logo.visibility = View.GONE
        title.visibility = View.VISIBLE
        title.setText(string.setting)
       openFragment(settingsFragment)
    }
    private fun openFragment( fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        // transaction.setCustomAnimations(R.anim.fade_in, R.anim.item_fade_out_slow)
        transaction.replace(id.home_fragment_container, fragment)
        transaction.commit()
    }
    fun logout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.logOut(mSharedPrefManager.userData.id!!,deviceID,"android",mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ResendCodeModel> {
            override fun onFailure(call: Call<ResendCodeModel>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ResendCodeModel>,
                response: Response<ResendCodeModel>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        mSharedPrefManager.loginStatus=false
                        mSharedPrefManager.Logout()
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        startActivity(Intent(this@MainActivity, SplashActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun visitor(){
        val dialog =  Dialog(mContext!!)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog ?.setCancelable(false)
        dialog ?.setContentView(R.layout.dialog_login)
        val login = dialog?.findViewById<Button>(R.id.login)
        login.setOnClickListener {  val intent=Intent(this@MainActivity,PreLoginActivity::class.java)

            startActivity(intent)
            finish()
        }
        dialog.show()
    }
}


