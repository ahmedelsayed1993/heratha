package com.aait.hareth.UI.Activities

import android.content.Intent
import android.graphics.Color
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Models.ResendCodeModel
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.SplashActivity
import com.aait.hareth.UI.Fragments.*
import com.aait.hareth.Utils.CommonUtil
import com.bumptech.glide.Glide
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.drawer_layout
import kotlinx.android.synthetic.main.activity_provider_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ProviderMainActivity:Parent_Activity() {
    lateinit var home: LinearLayout
    lateinit var request: LinearLayout
    lateinit var profile: LinearLayout
    lateinit var setting: LinearLayout
    lateinit var home_text : TextView
    lateinit var home_image: ImageView
    lateinit var profile_text : TextView
    lateinit var profile_image: ImageView
    lateinit var request_text : TextView
    lateinit var request_image: ImageView
    lateinit var setting_text : TextView
    lateinit var setting_image: ImageView
    lateinit var menu: ImageView
    lateinit var menu_back: ImageView
    lateinit var title: TextView
    lateinit var logo: ImageView
    lateinit var about_app: TextView
    lateinit var terms: TextView
    lateinit var repeated: TextView
    lateinit var contact_us: TextView
    lateinit var complains: TextView
    lateinit var share_app: TextView
    lateinit var comments:TextView
    lateinit var Financial_accounts:TextView
    lateinit var main: TextView
    lateinit var add:ImageView
    lateinit var image: CircleImageView
    lateinit var name:TextView
    lateinit var logout:TextView
    protected var selectedTab = 0
    private val tabsStack = Stack<Int>()
    lateinit var search:ImageView

    private var fragmentManager: FragmentManager? = null

    private var transaction: FragmentTransaction? = null
    internal lateinit var providerHomeFragment: ProviderHomeFragment
    internal lateinit var providerRequestsFragment: ProviderRequestsFragment
    internal lateinit var settingsFragment: SettingsFragment
    internal lateinit var profileFragment: ProviderProfileFragment
    lateinit var notification:ImageView
    var deviceID=""
    var selected = 1
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    override val layoutResource: Int
        get() = R.layout.activity_provider_main

    override fun initializeComponents() {
        search = findViewById(R.id.search)
        search.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,ProviderSearchActivity::class.java)) }
        notification = findViewById(R.id.notification)
        notification.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,NotificationActivity::class.java)) }
        Log.e("user",Gson().toJson(mSharedPrefManager.userData!!))
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        providerHomeFragment = ProviderHomeFragment.newInstance()
        providerRequestsFragment = ProviderRequestsFragment.newInstance()
        profileFragment = ProviderProfileFragment.newInstance()
        settingsFragment = SettingsFragment.newInstance()
        fragmentManager = getSupportFragmentManager()
        transaction = fragmentManager!!.beginTransaction()
        transaction!!.add(R.id.home_fragment_container, providerHomeFragment)
        transaction!!.add(R.id.home_fragment_container, providerRequestsFragment)
        transaction!!.add(R.id.home_fragment_container,profileFragment)
        transaction!!.add(R.id.home_fragment_container,settingsFragment)
        transaction!!.commit()
        home = findViewById(R.id.home)
        add = findViewById(R.id.add)
        request = findViewById(R.id.requests)
        profile = findViewById(R.id.profile)
        setting = findViewById(R.id.setting)
        home_image = findViewById(R.id.home_image)
        home_text = findViewById(R.id.home_text)
        request_image = findViewById(R.id.request_image)
        request_text = findViewById(R.id.request_text)
        profile_image = findViewById(R.id.profile_image)
        profile_text = findViewById(R.id.profile_text)
        setting_image = findViewById(R.id.setting_image)
        setting_text = findViewById(R.id.setting_text)
        title = findViewById(R.id.title)
        logo = findViewById(R.id.logo)
        menu = findViewById(R.id.menu)

        menu_back = findViewById(R.id.menu_back)
        showhome()
        sideMenu()
        home.setOnClickListener(View.OnClickListener {
            showhome()
        })
        request.setOnClickListener(View.OnClickListener {
            Requests()
        })
        profile.setOnClickListener(View.OnClickListener {
            profile()
        })
        setting.setOnClickListener(View.OnClickListener {
            settings()
        })
        add.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,AddJobActivity::class.java)) }
    }


    fun sideMenu(){
        drawer_layout.useCustomBehavior(Gravity.START)
        drawer_layout.useCustomBehavior(Gravity.END)
        drawer_layout.setRadius(Gravity.START, 25f)
        drawer_layout.setRadius(Gravity.END, 25f)
        drawer_layout.setViewScale(Gravity.START, 0.9f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewScale(Gravity.END, 0.9f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewElevation(Gravity.START, 20f) //set main view elevation when drawer open (dimension)
        drawer_layout.setViewElevation(Gravity.END, 20f) //set main view elevation when drawer open (dimension)
        drawer_layout.setViewScrimColor(Gravity.START, Color.TRANSPARENT) //set drawer overlay coloe (color)
        drawer_layout.setViewScrimColor(Gravity.END, Color.TRANSPARENT) //set drawer overlay coloe (color)
        drawer_layout.setDrawerElevation(Gravity.START, 20f) //set drawer elevation (dimension)
        drawer_layout.setDrawerElevation(Gravity.END, 20f) //set drawer elevation (dimension)
        drawer_layout.setContrastThreshold(3f) //set maximum of contrast ratio between white text and background color.
        drawer_layout.setRadius(Gravity.START, 40f) //set end container's corner radius (dimension)

        drawer_layout.setRadius(Gravity.END, 40f)

        menu.setOnClickListener { drawer_layout.openDrawer(GravityCompat.START) }
        menu_back.setOnClickListener { drawer_layout.closeDrawer(GravityCompat.START) }
        logout = drawer_layout.findViewById(R.id.logout)
        image = drawer_layout.findViewById(R.id.image)
        name = drawer_layout.findViewById(R.id.name)
        main = drawer_layout.findViewById(R.id.main)
        about_app = drawer_layout.findViewById(R.id.about_app)
        Financial_accounts = drawer_layout.findViewById(R.id.Financial_accounts)
        terms = drawer_layout.findViewById(R.id.terms)
        repeated = drawer_layout.findViewById(R.id.repeated)
        contact_us = drawer_layout.findViewById(R.id.contact_us)
        complains = drawer_layout.findViewById(R.id.complains)
        share_app = drawer_layout.findViewById(R.id.share)
        comments = drawer_layout.findViewById(R.id.comments)
        Glide.with(mContext).load(mSharedPrefManager.userData.avatar).into(image)
        name.text = mSharedPrefManager.userData.name
        comments.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,CommentsActivity::class.java)) }
        main.setOnClickListener { drawer_layout.closeDrawer(GravityCompat.START) }
        about_app.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,AboutAppActivity::class.java)) }
        terms.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,TermsActivity::class.java))  }
        repeated.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,RepeatedQuestionsActivity::class.java)) }
        contact_us.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,ContactUsActivity::class.java)) }
        complains.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,ComplainsActivity::class.java)) }
        share_app.setOnClickListener { CommonUtil.ShareApp(applicationContext) }
        Financial_accounts.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,FinancialActivity::class.java)) }

        logout.setOnClickListener { logout() }
    }
    fun showhome(){
        selected = 1
        home_image.setImageResource(R.mipmap.noun_homeacti)
        home_text.textColor = Color.parseColor("#EA3434")
        request_text.textColor = Color.parseColor("#989898")
        profile_text.textColor = Color.parseColor("#989898")
        setting_text.textColor = Color.parseColor("#989898")
        profile_image.setImageResource(R.mipmap.user)
        request_image.setImageResource(R.mipmap.choices)
        setting_image.setImageResource(R.mipmap.noun_etting)
        logo.visibility = View.VISIBLE
        title.visibility = View.GONE
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, providerHomeFragment)
        transaction!!.commit()
    }
    fun Requests(){
        selected = 2
        home_image.setImageResource(R.mipmap.noun_home)
        request_text.textColor = Color.parseColor("#EA3434")
        profile_text.textColor = Color.parseColor("#989898")
        setting_text.textColor = Color.parseColor("#989898")
        home_text.textColor = Color.parseColor("#989898")
        profile_image.setImageResource(R.mipmap.user)
        request_image.setImageResource(R.mipmap.choicesacti)
        setting_image.setImageResource(R.mipmap.noun_etting)
        logo.visibility = View.GONE
        title.visibility = View.VISIBLE
        title.setText(R.string.Requests)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, providerRequestsFragment)
        transaction!!.commit()
    }
    fun profile(){
        selected = 2
        home_image.setImageResource(R.mipmap.noun_home)
        profile_text.textColor = Color.parseColor("#EA3434")
        request_text.textColor = Color.parseColor("#989898")
        setting_text.textColor = Color.parseColor("#989898")
        home_text.textColor = Color.parseColor("#989898")
        profile_image.setImageResource(R.mipmap.useracti)
        request_image.setImageResource(R.mipmap.choices)
        setting_image.setImageResource(R.mipmap.noun_etting)
        logo.visibility = View.GONE
        title.visibility = View.VISIBLE
        title.setText(R.string.My_profile)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, profileFragment)
        transaction!!.commit()
    }
    fun settings(){
        selected = 2
        home_image.setImageResource(R.mipmap.noun_home)
        setting_text.textColor = Color.parseColor("#EA3434")
        profile_text.textColor = Color.parseColor("#989898")
        request_text.textColor = Color.parseColor("#989898")
        home_text.textColor = Color.parseColor("#989898")
        profile_image.setImageResource(R.mipmap.user)
        request_image.setImageResource(R.mipmap.choices)
        setting_image.setImageResource(R.mipmap.noun_settings)
        logo.visibility = View.GONE
        title.visibility = View.VISIBLE
        title.setText(R.string.setting)
        openFragment(settingsFragment)
    }
    private fun openFragment( fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        // transaction.setCustomAnimations(R.anim.fade_in, R.anim.item_fade_out_slow)
        transaction.replace(R.id.home_fragment_container, fragment)
        transaction.commit()
    }
    fun logout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.logOut(mSharedPrefManager.userData.id!!,deviceID,"android",mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ResendCodeModel> {
            override fun onFailure(call: Call<ResendCodeModel>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ResendCodeModel>,
                response: Response<ResendCodeModel>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        mSharedPrefManager.loginStatus=false
                        mSharedPrefManager.Logout()
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        startActivity(Intent(this@ProviderMainActivity,SplashActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
}