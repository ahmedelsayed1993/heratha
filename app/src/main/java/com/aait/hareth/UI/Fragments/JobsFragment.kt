package com.aait.hareth.UI.Fragments

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.hareth.Base.BaseFragment
import com.aait.hareth.Client
import com.aait.hareth.Listener.OnItemClickListener
import com.aait.hareth.Models.*
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.UI.Activities.PreLoginActivity
import com.aait.hareth.UI.Activities.PresentJobActivity
import com.aait.hareth.UI.Activities.SeekerLoginActivity
import com.aait.hareth.UI.Adapter.CompanyJobsAdpter
import com.aait.hareth.UI.Adapter.HomeProviderAdapter
import com.aait.hareth.UI.Adapter.ListAdapter
import com.aait.hareth.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class JobsFragment:BaseFragment(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        if (view.id==R.id.lay){
            if (mSharedPrefManager.loginStatus!!) {
                val intent = Intent(activity, PresentJobActivity::class.java)
                intent.putExtra("job", myJobsModels.get(position))
                startActivity(intent)
            }else{
                val dialog =  Dialog(mContext!!)
                dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog ?.setCancelable(false)
                dialog ?.setContentView(R.layout.dialog_login)
                val login = dialog?.findViewById<Button>(R.id.login)
                login?.setOnClickListener {  val intent=Intent(activity, PreLoginActivity::class.java)

                    startActivity(intent)
                    activity?.finish()
                }
                dialog?.show()
            }

        }else {

            listModel = listModels.get(position)
            city.setText(listModel.name!!)
            cities_lay.visibility = View.GONE
            getJobs(listModel.id, null)
        }

    }

    lateinit var direction:TextView
    lateinit var city :TextView
    lateinit var jobs:RecyclerView
    lateinit var directions:LinearLayout
    lateinit var north:TextView
    lateinit var south:TextView
    lateinit var east:TextView
    lateinit var west:TextView
    lateinit var cities_lay:LinearLayout
    lateinit var cities:RecyclerView
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var listModels = ArrayList<ListModel>()
    internal lateinit var listAdapter: ListAdapter
    internal lateinit var listModel:ListModel
    internal lateinit var linearLayoutManager1: LinearLayoutManager
    internal var myJobsModels = java.util.ArrayList<CompanyJobs>()
    internal lateinit var homeProviderAdapter: CompanyJobsAdpter
    companion object {
        fun newInstance(id:Int): JobsFragment {
            val args = Bundle()
            val fragment = JobsFragment()
            args.putInt("id",id)
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var bundle:Bundle
    internal var id: Int = 0
    var direct:String? = null

    override val layoutResource: Int
        get() = R.layout.fragment_jobs

    override fun initializeComponents(view: View) {
        bundle= this.arguments!!
        id = bundle.getInt("id")
        direction = view.findViewById(R.id.direction)
        city  = view.findViewById(R.id.city)
        jobs = view.findViewById(R.id.jobs)
        directions = view.findViewById(R.id.directions)
        north = view.findViewById(R.id.north)
        south = view.findViewById(R.id.south)
        east = view.findViewById(R.id.east)
        west = view.findViewById(R.id.west)
        cities_lay = view.findViewById(R.id.cities_lay)
        cities = view.findViewById(R.id.cities)
        cities_lay.visibility = View.GONE
        directions.visibility = View.GONE
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        homeProviderAdapter = CompanyJobsAdpter(mContext!!,myJobsModels,R.layout.recycler_company_jobs)
        homeProviderAdapter.setOnItemClickListener(this)
        jobs!!.layoutManager= linearLayoutManager1
        jobs!!.adapter = homeProviderAdapter
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter =  ListAdapter(mContext!!,listModels,R.layout.recycler_list)
        listAdapter.setOnItemClickListener(this)
        cities!!.layoutManager= linearLayoutManager
        cities!!.adapter = listAdapter
        getCities()
        getJobs(null,null)
        city.setOnClickListener {
            if(cities_lay.visibility==View.GONE){
                cities_lay.visibility = View.VISIBLE
            }else{
                cities_lay.visibility = View.GONE
            }
        }
        direction.setOnClickListener{
            if (directions.visibility == View.GONE){
                directions.visibility = View.VISIBLE
            }else{
                directions.visibility = View.GONE
            }
        }
        north.setOnClickListener {
            direct = "north"
            directions.visibility = View.GONE
            direction.text = getString(R.string.north)
            getJobs(null,direct)
        }
        south.setOnClickListener {
            direct = "south"
            directions.visibility = View.GONE
            direction.text = getString(R.string.south)
            getJobs(null,direct)
        }
        east.setOnClickListener {
            direct = "east"
            directions.visibility = View.GONE
            direction.text = getString(R.string.east)
            getJobs(null,direct)
        }
        west.setOnClickListener {
            direct = "west"
            directions.visibility = View.GONE
            direction.text = getString(R.string.west)
            getJobs(null,direct)
        }


    }

    fun getCities(){

        Client.getClient()?.create(Service::class.java)?.cities(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ListResponse> {

            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()

            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {

                if (response.isSuccessful){
                    if (response.body()!!.key.equals("1")){
                        listAdapter.updateAll(response.body()!!.data!!)
                    }else{

                    }
                }
            }
        } )
    }

    fun getJobs(city:Int?,direction:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getCompany(mLanguagePrefManager.appLanguage,id,city,direction)?.enqueue(object :Callback<AvaalbleJobsResponse>{
            override fun onFailure(call: Call<AvaalbleJobsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AvaalbleJobsResponse>,
                response: Response<AvaalbleJobsResponse>
            ) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        if (response.body()?.jobs!!.isEmpty()){
                           jobs.visibility = View.GONE
                        }else{
                            jobs.visibility = View.VISIBLE
                            homeProviderAdapter.updateAll(response.body()?.jobs!!)
                        }
                    }
                }
            }

        })
    }
}