package com.aait.hareth.UI.Activities

import android.app.Dialog
import android.content.Intent
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Models.AddJobModel
import com.aait.hareth.Models.JobDetailsModel
import com.aait.hareth.Models.JobDetailsResponse
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class JobDetailsActivity :Parent_Activity(){
    var id:Int=0
    lateinit var menu:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var special:TextView
    lateinit var salary:TextView
    lateinit var city:TextView
    lateinit var direction:TextView
    lateinit var certificate:TextView
    lateinit var length:TextView
    lateinit var weight:TextView
    lateinit var age:TextView
    lateinit var experiences:TextView
    lateinit var start:TextView
    lateinit var end_date:TextView
    lateinit var edit:Button
    lateinit var delete:Button
    override val layoutResource: Int
        get() = R.layout.activity_job_details

    override fun initializeComponents() {
        notification = findViewById(R.id.notification)
        notification.setOnClickListener { startActivity(Intent(this@JobDetailsActivity,NotificationActivity::class.java)) }
        id = intent.getIntExtra("id",0)
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        special = findViewById(R.id.special)
        salary = findViewById(R.id.salary)
        city = findViewById(R.id.city)
        direction = findViewById(R.id.direction)
        certificate = findViewById(R.id.certificate)
        length = findViewById(R.id.length)
        weight = findViewById(R.id.weight)
        age = findViewById(R.id.age)
        experiences = findViewById(R.id.experiences)
        start = findViewById(R.id.start)
        end_date = findViewById(R.id.end_date)
        edit = findViewById(R.id.edit)
        delete = findViewById(R.id.delete)
        menu.setOnClickListener { onBackPressed() }
        title.text = getString(R.string.Job_details)
        getJob()
        delete.setOnClickListener { val dialog = Dialog(mContext!!)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog ?.setCancelable(false)
            dialog ?.setContentView(R.layout.dialog_delete_job)
            val yes = dialog?.findViewById<Button>(R.id.yes)
            val no = dialog?.findViewById<Button>(R.id.no)
            val message = dialog?.findViewById<TextView>(R.id.message)
            yes?.setOnClickListener {
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.deleteJob(mLanguagePrefManager.appLanguage,id!!,mSharedPrefManager.userData.id!!)?.enqueue(
                    object :Callback<AddJobModel>{
                        override fun onFailure(call: Call<AddJobModel>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                        }

                        override fun onResponse(
                            call: Call<AddJobModel>,
                            response: Response<AddJobModel>
                        ) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if(response.body()?.key.equals("1")){
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    onBackPressed()
                                    dialog?.dismiss()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    dialog?.dismiss()
                                }
                            }
                        }

                    }
                )
            }
            no?.setOnClickListener { dialog?.dismiss() }

            dialog?.show() }
        edit.setOnClickListener { val intent = Intent(this@JobDetailsActivity,EditJobActivity::class.java)
        intent.putExtra("id",id)
        startActivity(intent)}

    }
    fun setData(jobDetailsModel: JobDetailsModel){
        special.text = jobDetailsModel.specialty
        city.text = jobDetailsModel.city
        salary.text = jobDetailsModel.salary
        direction.text = jobDetailsModel.direction
        certificate.text = jobDetailsModel.certificates
        length.text= jobDetailsModel.height!!.toString()
        weight.text=(jobDetailsModel.width!!.toString())
        age.text=(jobDetailsModel.age!!.toString())
        experiences.text = jobDetailsModel.experience
        start.text = jobDetailsModel.start_date
        end_date.text = jobDetailsModel.end_date
    }

    fun getJob(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getJob(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id)?.enqueue(object :
            Callback<JobDetailsResponse> {
            override fun onFailure(call: Call<JobDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<JobDetailsResponse>,
                response: Response<JobDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        setData(response.body()?.data!!)

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
}