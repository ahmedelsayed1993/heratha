package com.aait.hareth.UI.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.TextView
import com.aait.hareth.Base.ParentRecyclerAdapter
import com.aait.hareth.Base.ParentRecyclerViewHolder
import com.aait.hareth.Models.CancelModel
import com.aait.hareth.Models.QuestionModel
import com.aait.hareth.R

class ReasonsAdapter (context: Context, data: MutableList<CancelModel>, layoutId: Int) :
    ParentRecyclerAdapter<CancelModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    internal var selectedPosition = 0
    var RadioButton.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val cancelModel = data.get(position)
        viewHolder.reason!!.setText(cancelModel.content)
        viewHolder.reason.setChecked(selectedPosition == position)
        viewHolder.reason.setTag(position)
        viewHolder.reason.setOnClickListener(View.OnClickListener { view ->
            onItemClickListener.onItemClick(view, position)
            selectedPosition = viewHolder.reason.getTag() as Int
            viewHolder.reason.textColor = R.color.colorPrimary
            notifyDataSetChanged()
        })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {




        internal  var reason=itemView.findViewById<RadioButton>(R.id.reason)



    }

}