package com.aait.hareth.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.R
import com.aait.hareth.SplashActivity

class LanguageActivity :Parent_Activity(){
    lateinit var arabic:RadioButton
    lateinit var english:RadioButton
    lateinit var menu:ImageView
    lateinit var title:TextView
    lateinit var logo:ImageView
    lateinit var search:ImageView
    lateinit var notification:ImageView
    override val layoutResource: Int
        get() = R.layout.activity_app_language

    override fun initializeComponents() {
        search = findViewById(R.id.search)
        search.visibility =View.GONE
        notification = findViewById(R.id.notification)
        notification.setOnClickListener { startActivity(Intent(this@LanguageActivity,NotificationActivity::class.java)) }
        arabic = findViewById(R.id.arabic)
        english = findViewById(R.id.english)
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        logo = findViewById(R.id.logo)
        search = findViewById(R.id.search)
        notification = findViewById(R.id.notification)
        title.visibility =View.VISIBLE
        logo.visibility = View.GONE
        title.text = getString(R.string.app_language)
        menu.setOnClickListener { onBackPressed() }
        if (mLanguagePrefManager.appLanguage.equals("ar")){
            arabic.isChecked = true
        }else{
            english.isChecked = true
        }
        arabic.setOnClickListener { mLanguagePrefManager.appLanguage = "ar"
            startActivity(Intent(this@LanguageActivity,SplashActivity::class.java))
        finish()
        }
        english.setOnClickListener { mLanguagePrefManager.appLanguage = "en"
            startActivity(Intent(this@LanguageActivity,SplashActivity::class.java))
            finish()
        }


    }
}