package com.aait.hareth.UI.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import com.aait.hareth.Base.ParentRecyclerAdapter
import com.aait.hareth.Base.ParentRecyclerViewHolder
import com.aait.hareth.Models.JobModel
import com.aait.hareth.Models.NotificationModel
import com.aait.hareth.R
import com.bumptech.glide.Glide
import com.daimajia.swipe.SwipeLayout

class NotificationAdapter (context: Context, data: MutableList<NotificationModel>, layoutId: Int) :
    ParentRecyclerAdapter<NotificationModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as NotificationAdapter.ViewHolder
        val notificationModel = data.get(position)
        viewHolder.text!!.setText(notificationModel.text)
        viewHolder.swipe.setShowMode(SwipeLayout.ShowMode.PullOut)

        //dari kiri
        viewHolder.swipe.addDrag(
            SwipeLayout.DragEdge.Right,
            viewHolder.swipe.findViewById(R.id.bottom_wraper)
        )
        viewHolder.swipe.addSwipeListener(object : SwipeLayout.SwipeListener {
            override fun onStartOpen(layout: SwipeLayout) {

            }

            override fun onOpen(layout: SwipeLayout) {

            }

            override fun onStartClose(layout: SwipeLayout) {

            }

            override fun onClose(layout: SwipeLayout) {

            }

            override fun onUpdate(layout: SwipeLayout, leftOffset: Int, topOffset: Int) {

            }

            override fun onHandRelease(layout: SwipeLayout, xvel: Float, yvel: Float) {

            }
        })
        viewHolder.delete.setOnClickListener(View.OnClickListener { view ->
            onItemClickListener.onItemClick(
                view,
                position
            ) })

    }

    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {




        internal  var delete=itemView.findViewById<TextView>(R.id.delete)
        internal var text=itemView.findViewById<TextView>(R.id.text)
        internal var swipe=itemView.findViewById<SwipeLayout>(R.id.swipe)
        internal var lay=itemView.findViewById<LinearLayout>(R.id.lay)


    }
}