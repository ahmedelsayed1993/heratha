package com.aait.hareth.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Models.SettingsModel
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AboutAppActivity : Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_about_app
    lateinit var menu:ImageView
    lateinit var title: TextView
    lateinit var logo:ImageView
    lateinit var about:TextView
    lateinit var notification:ImageView
    lateinit var search:ImageView



    override fun initializeComponents() {
        search = findViewById(R.id.search)
        search.visibility =View.GONE
        notification = findViewById(R.id.notification)
        notification.setOnClickListener { startActivity(Intent(this@AboutAppActivity,NotificationActivity::class.java)) }
       menu = findViewById(R.id.menu)
        logo = findViewById(R.id.logo)
        title = findViewById(R.id.title)
        logo.visibility = View.GONE
        title.visibility = View.VISIBLE
        title.setText(R.string.about_app)
        about = findViewById(R.id.about)

        menu.setOnClickListener { onBackPressed() }
        getAbout()
    }





    fun getAbout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getAbout(mLanguagePrefManager.appLanguage)?.enqueue(object :Callback<SettingsModel>{
            override fun onFailure(call: Call<SettingsModel>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(applicationContext,t)
                t.printStackTrace()
                about.text=""


            }

            override fun onResponse(call: Call<SettingsModel>, response: Response<SettingsModel>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        about.text= response.body()?.data!!
                    }
                }
            }

        })
    }
}