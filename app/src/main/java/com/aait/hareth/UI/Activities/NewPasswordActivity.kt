package com.aait.hareth.UI.Activities

import android.content.Intent
import android.text.InputType
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Models.UserModel
import com.aait.hareth.Models.UserResponse
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewPasswordActivity : Parent_Activity() {
    lateinit var send :Button
    lateinit var password:EditText
    lateinit var view:ImageView
    lateinit var confirm:EditText
    lateinit var view1:ImageView
    lateinit var back:ImageView
    override val layoutResource: Int
        get() = R.layout.activity_new_password
    lateinit var type:UserModel

    override fun initializeComponents() {
        type = intent.getSerializableExtra("type") as UserModel
        password = findViewById(R.id.password)
        view = findViewById(R.id.view)
        confirm = findViewById(R.id.confirm)
        view1 = findViewById(R.id.view1)
        back = findViewById(R.id.back)
        send = findViewById(R.id.send)
        back.setOnClickListener { onBackPressed() }
        view.setOnClickListener {  if (password.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
            password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }
        else{
            password.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
        } }
        view1.setOnClickListener {
            if (confirm.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
                confirm.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            }
            else{
                confirm.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
            }
        }
        send.setOnClickListener(View.OnClickListener {
            if (CommonUtil.checkEditError(password,getString(R.string.please_enter_password))||
                    CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                    CommonUtil.checkEditError(confirm,getString(R.string.confirm_new_password))){
                return@OnClickListener
            }else{
                if (!password.text.toString().equals(confirm.text.toString())){
                    confirm.error = getString(R.string.password_not_match)
                }else{
                    update()
                }
            }
        })
    }

    fun update(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.UpdatePass(type.id!!,password.text.toString(),mLanguagePrefManager.appLanguage)?.enqueue(
            object : Callback<UserResponse> {
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.key.equals("1")){
                            val intent=Intent(this@NewPasswordActivity,SeekerLoginActivity::class.java)
                            intent.putExtra("type",response.body()?.data?.user_type)
                            startActivity(intent)
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }
            }
        )
    }






}