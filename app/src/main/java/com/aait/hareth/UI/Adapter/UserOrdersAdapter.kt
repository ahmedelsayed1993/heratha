package com.aait.hareth.UI.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.aait.hareth.Base.ParentRecyclerAdapter
import com.aait.hareth.Base.ParentRecyclerViewHolder
import com.aait.hareth.Models.RequestModel
import com.aait.hareth.Models.UserOrdersModels
import com.aait.hareth.R
import com.bumptech.glide.Glide

class UserOrdersAdapter (context: Context, data: MutableList<UserOrdersModels>, layoutId: Int) :
    ParentRecyclerAdapter<UserOrdersModels>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as UserOrdersAdapter.ViewHolder
        val userOrdersModels = data.get(position)
        viewHolder.name!!.setText(userOrdersModels.name)
        viewHolder.rating!!.rating = userOrdersModels.rate!!.toFloat()
        viewHolder.address!!.setText(userOrdersModels.city)
        Glide.with(mcontext).load(userOrdersModels.avatar).into(viewHolder.image)
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view ->
            onItemClickListener.onItemClick(
                view,
                position
            ) })

    }

    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {

        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var rating = itemView.findViewById<RatingBar>(R.id.rating)
        internal var image = itemView.findViewById<ImageView>(R.id.image)
        internal var address=itemView.findViewById<TextView>(R.id.address)

    }
}