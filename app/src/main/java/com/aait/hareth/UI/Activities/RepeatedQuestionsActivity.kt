package com.aait.hareth.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Models.QuestionModel
import com.aait.hareth.Models.QuestionsModel
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.UI.Adapter.QuestionAdapter
import com.aait.hareth.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RepeatedQuestionsActivity : Parent_Activity(){
    lateinit var menu: ImageView
    lateinit var title: TextView
    lateinit var logo: ImageView
    lateinit var notification:ImageView
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var questionModel = java.util.ArrayList<QuestionModel>()
    internal lateinit var questionAdapter: QuestionAdapter
    lateinit var rv_recycle:RecyclerView
    lateinit var search:ImageView
    override val layoutResource: Int
        get() = R.layout.activity_repeated_questions

    override fun initializeComponents() {
        search = findViewById(R.id.search)
        search.visibility =View.GONE
        notification = findViewById(R.id.notification)
        notification.setOnClickListener { startActivity(Intent(this@RepeatedQuestionsActivity,NotificationActivity::class.java)) }
        menu = findViewById(R.id.menu)
        logo = findViewById(R.id.logo)
        title = findViewById(R.id.title)
        logo.visibility = View.GONE
        title.visibility = View.VISIBLE
        title.setText(R.string.repeated_questions)
        rv_recycle= findViewById(R.id.rv_recycle)

        menu.setOnClickListener { onBackPressed() }

        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        questionAdapter = QuestionAdapter(mContext,questionModel,R.layout.recycler_question)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = questionAdapter
        getQuestions()
    }
    fun getQuestions(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getQuestions(mLanguagePrefManager.appLanguage)?.enqueue(object :Callback<QuestionsModel>{
            override fun onFailure(call: Call<QuestionsModel>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(applicationContext,t)
                t.printStackTrace()
                Log.e("error", Gson().toJson(t))


            }

            override fun onResponse(call: Call<QuestionsModel>, response: Response<QuestionsModel>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        Log.e("error", Gson().toJson(response.body()?.data!!))
                        questionAdapter.updateAll(response.body()?.data!!)
                    }
                }
            }

        })
    }





}