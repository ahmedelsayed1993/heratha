package com.aait.hareth.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Models.JobDetailsModel
import com.aait.hareth.Models.JobDetailsResponse
import com.aait.hareth.Models.UserOrderDetailsResponse
import com.aait.hareth.Models.UserOrdersModels
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.Utils.CommonUtil
import kotlinx.android.synthetic.main.provider_content_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserOrderDetailsActivity :Parent_Activity(){
    lateinit var menu: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var special: TextView
    lateinit var salary: TextView
    lateinit var city: TextView
    lateinit var direction: TextView
    lateinit var certificate: TextView
    lateinit var length: TextView
    lateinit var weight: TextView
    lateinit var age: TextView
    lateinit var experiences: TextView
    lateinit var start: TextView
    lateinit var end_date: TextView
    lateinit var company_name:TextView
    lateinit var phone_lay:LinearLayout
    lateinit var phone:TextView
    lateinit var refuse:TextView
    lateinit var reason:TextView
    lateinit var address:TextView
    lateinit var userOrdersModels:UserOrdersModels
    override val layoutResource: Int
        get() = R.layout.activity_user_order_details

    override fun initializeComponents() {
        userOrdersModels = intent.getSerializableExtra("order") as UserOrdersModels
        notification = findViewById(R.id.notification)
        notification.setOnClickListener { startActivity(Intent(this@UserOrderDetailsActivity,NotificationActivity::class.java)) }
        company_name = findViewById(R.id.company_name)
        phone_lay = findViewById(R.id.phone_lay)
        address = findViewById(R.id.address)
        phone = findViewById(R.id.phone)
        refuse = findViewById(R.id.refuse)
        reason = findViewById(R.id.reason)
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        special = findViewById(R.id.special)
        salary = findViewById(R.id.salary)
        city = findViewById(R.id.city)
        direction = findViewById(R.id.direction)
        certificate = findViewById(R.id.certificate)
        length = findViewById(R.id.length)
        weight = findViewById(R.id.weight)
        age = findViewById(R.id.age)
        experiences = findViewById(R.id.experiences)
        start = findViewById(R.id.start)
        end_date = findViewById(R.id.end_date)
        menu.setOnClickListener { onBackPressed() }
        title.text = getString(R.string.Job_details)
        if (userOrdersModels.status!!.equals("pending")){
            reason.visibility = View.GONE
            refuse.visibility = View.GONE
            phone_lay.visibility = View.GONE
        }else if (userOrdersModels.status!!.equals("accepted")){
            reason.visibility = View.GONE
            refuse.visibility = View.GONE
            phone_lay.visibility = View.VISIBLE
        }else if (userOrdersModels.status!!.equals("rejected")){
            reason.visibility = View.VISIBLE
            refuse.visibility = View.VISIBLE
            phone_lay.visibility = View.GONE
        }

        getJob()
    }



    fun setData(jobDetailsModel: UserOrderDetailsResponse){
        company_name.text = jobDetailsModel.companyInfo!!.name
        phone.text = jobDetailsModel.companyInfo!!.phone
        address.text = jobDetailsModel.companyInfo!!.city
        special.text = jobDetailsModel.data!!.specialty
        city.text = jobDetailsModel.data!!.city
        salary.text = jobDetailsModel.data!!.salary
        direction.text = jobDetailsModel.data!!.direction
        certificate.text = jobDetailsModel.data!!.certificates
        length.text= jobDetailsModel.data!!.height!!.toString()
        weight.text=(jobDetailsModel.data!!.width!!.toString())
        age.text=(jobDetailsModel.data!!.age!!.toString())
        experiences.text = jobDetailsModel.data!!.experience
        start.text = jobDetailsModel.data!!.start_date
        end_date.text = jobDetailsModel.data!!.end_date
        reason.text = jobDetailsModel.data!!.reason_rejected
    }

    fun getJob(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getOrderDetails(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,userOrdersModels.id!!.toInt())?.enqueue(object :
            Callback<UserOrderDetailsResponse> {
            override fun onFailure(call: Call<UserOrderDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<UserOrderDetailsResponse>,
                response: Response<UserOrderDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        setData(response.body()!!)

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
}