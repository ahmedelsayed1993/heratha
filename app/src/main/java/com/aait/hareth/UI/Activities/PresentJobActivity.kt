package com.aait.hareth.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Models.CompanyJobs
import com.aait.hareth.Models.JobDetailsModel
import com.aait.hareth.Models.JobDetailsResponse
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PresentJobActivity:Parent_Activity() {
    lateinit var menu: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var special: TextView
    lateinit var salary: TextView
    lateinit var city: TextView
    lateinit var direction: TextView
    lateinit var certificate: TextView
    lateinit var length: TextView
    lateinit var weight: TextView
    lateinit var age: TextView
    lateinit var experiences: TextView
    lateinit var start: TextView
    lateinit var end_date: TextView
    lateinit var job_name:TextView
    lateinit var companyJobs: CompanyJobs
    lateinit var apply:Button
    override val layoutResource: Int
        get() = R.layout.activity_presnt_job

    override fun initializeComponents() {
        companyJobs = intent.getSerializableExtra("job") as CompanyJobs
        notification = findViewById(R.id.notification)
        notification.setOnClickListener { startActivity(Intent(this@PresentJobActivity,NotificationActivity::class.java)) }
        job_name = findViewById(R.id.job_name)
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        special = findViewById(R.id.special)
        salary = findViewById(R.id.salary)
        city = findViewById(R.id.city)
        direction = findViewById(R.id.direction)
        certificate = findViewById(R.id.certificate)
        length = findViewById(R.id.length)
        weight = findViewById(R.id.weight)
        age = findViewById(R.id.age)
        experiences = findViewById(R.id.experiences)
        start = findViewById(R.id.start)
        end_date = findViewById(R.id.end_date)
        apply = findViewById(R.id.apply)
        apply.setOnClickListener { getJob() }

        menu.setOnClickListener { onBackPressed() }
        title.text = getString(R.string.Job_details)
        getJob(null)

    }

    fun setData(jobDetailsModel: JobDetailsModel){
        job_name.text = jobDetailsModel.title
        special.text = jobDetailsModel.specialty
        city.text = jobDetailsModel.city
        salary.text = jobDetailsModel.salary
        direction.text = jobDetailsModel.direction
        certificate.text = jobDetailsModel.certificates
        length.text= jobDetailsModel.height!!.toString()
        weight.text=(jobDetailsModel.width!!.toString())
        age.text=(jobDetailsModel.age!!.toString())
        experiences.text = jobDetailsModel.experience
        start.text = jobDetailsModel.start_date
        end_date.text = jobDetailsModel.end_date
    }

    fun getJob(present:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.present(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,companyJobs.provider_id!!.toInt(),companyJobs.id!!,present)?.enqueue(object :
            Callback<JobDetailsResponse> {
            override fun onFailure(call: Call<JobDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<JobDetailsResponse>,
                response: Response<JobDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        setData(response.body()?.data!!)

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
    fun getJob(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Present(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,companyJobs.provider_id!!.toInt(),companyJobs.id!!,"1")?.enqueue(object :
            Callback<JobDetailsResponse> {
            override fun onFailure(call: Call<JobDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<JobDetailsResponse>,
                response: Response<JobDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        startActivity(Intent(this@PresentJobActivity,RequestSentActivity::class.java))
                        finish()

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
}