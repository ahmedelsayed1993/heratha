package com.aait.hareth.UI.Activities

import android.content.Intent
import android.widget.Button
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.R

class RefuseSentActivity :Parent_Activity(){
    lateinit var back:Button
    override val layoutResource: Int
        get() = R.layout.activity_refuse_sent

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        back.setOnClickListener { startActivity(Intent(this@RefuseSentActivity,ProviderMainActivity::class.java))
        finish()}
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this@RefuseSentActivity,ProviderMainActivity::class.java))
        finish()
    }
}