package com.aait.hareth.UI.Fragments

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.hareth.Base.BaseFragment
import com.aait.hareth.Client
import com.aait.hareth.Listener.OnItemClickListener
import com.aait.hareth.Models.*
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.UI.Activities.EditJobActivity
import com.aait.hareth.UI.Activities.JobDetailsActivity
import com.aait.hareth.UI.Adapter.HomeProviderAdapter
import com.aait.hareth.UI.Adapter.HomeSeekerAdapter
import com.aait.hareth.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProviderHomeFragment:BaseFragment(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.cancel){
            val dialog = Dialog(mContext!!)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog ?.setCancelable(false)
            dialog ?.setContentView(R.layout.dialog_delete_job)
            val yes = dialog?.findViewById<Button>(R.id.yes)
            val no = dialog?.findViewById<Button>(R.id.no)
            val message = dialog?.findViewById<TextView>(R.id.message)
            yes?.setOnClickListener {
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.deleteJob(mLanguagePrefManager.appLanguage,myJobsModels.get(position).id!!,mSharedPrefManager.userData.id!!)?.enqueue(
                    object :Callback<AddJobModel>{
                        override fun onFailure(call: Call<AddJobModel>, t: Throwable) {
                             CommonUtil.handleException(mContext!!,t)
                            t.printStackTrace()
                            hideProgressDialog()
                        }

                        override fun onResponse(
                            call: Call<AddJobModel>,
                            response: Response<AddJobModel>
                        ) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if(response.body()?.key.equals("1")){
                                     CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                                    homeProviderAdapter.notifyDataSetChanged()
                                    dialog?.dismiss()
                                }else{
                                     CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                                    dialog?.dismiss()
                                }
                            }
                        }

                    }
                )
            }
            no?.setOnClickListener { dialog?.dismiss() }

            dialog?.show()
        }else if (view.id==R.id.edit){
            val intent = Intent(activity,EditJobActivity::class.java)
            intent.putExtra("id",myJobsModels.get(position).id!!)
            startActivity(intent)

        }else{
            val intent = Intent(activity,JobDetailsActivity::class.java)
            intent.putExtra("id",myJobsModels.get(position).id!!)
            startActivity(intent)
        }
    }

    lateinit var rv_recycle: RecyclerView

    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    internal var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var myJobsModels = java.util.ArrayList<MyJobsModel>()
    internal lateinit var homeProviderAdapter: HomeProviderAdapter

    companion object {
        fun newInstance(): ProviderHomeFragment {
            val args = Bundle()
            val fragment = ProviderHomeFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    override val layoutResource: Int
        get() = R.layout.app_recycle

    override fun initializeComponents(view: View) {
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        homeProviderAdapter =  HomeProviderAdapter(mContext!!,myJobsModels,R.layout.recycler_provider_home)
        homeProviderAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = homeProviderAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome() }
        getHome()

    }

    fun getHome(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.myJobs(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<MyJobsResponse> {
            override fun onResponse(call: Call<MyJobsResponse>, response: Response<MyJobsResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.key.equals("1")) {
                            Log.e("myJobs",Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                homeProviderAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {}

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<MyJobsResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })
    }
}