package com.aait.hareth.UI.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.aait.hareth.Base.ParentRecyclerAdapter
import com.aait.hareth.Base.ParentRecyclerViewHolder
import com.aait.hareth.Models.CommentModel
import com.aait.hareth.Models.DirectionsModel
import com.aait.hareth.R

class CommentsAdapter (context: Context, data: MutableList<CommentModel>, layoutId: Int) :
    ParentRecyclerAdapter<CommentModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val commentModel = data.get(position)
        viewHolder.name!!.setText(commentModel.name)
        viewHolder.date!!.setText(commentModel.created)
        viewHolder.comment!!.setText(commentModel.comment)
        viewHolder.rating!!.rating = commentModel.rate!!.toFloat()
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {

        internal var report=itemView.findViewById<ImageView>(R.id.report)
        internal var comment=itemView.findViewById<TextView>(R.id.comment)
        internal var rating=itemView.findViewById<RatingBar>(R.id.rating)
        internal var date=itemView.findViewById<TextView>(R.id.date)
        internal var name=itemView.findViewById<TextView>(R.id.name)


    }

}