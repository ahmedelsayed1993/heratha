package com.aait.hareth.UI.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.hareth.Base.ParentRecyclerAdapter
import com.aait.hareth.Base.ParentRecyclerViewHolder
import com.aait.hareth.Models.BanksModel
import com.aait.hareth.Models.MyJobsModel
import com.aait.hareth.R

class BanksAdapter (context: Context, data: MutableList<BanksModel>, layoutId: Int) :
    ParentRecyclerAdapter<BanksModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as BanksAdapter.ViewHolder
        val banksModel = data.get(position)
        viewHolder.bank_name!!.setText(banksModel.bank_name)
        viewHolder.account_name!!.setText(banksModel.account_name)
        viewHolder.account_number!!.setText(banksModel.account_number)
        viewHolder.iban!!.setText(banksModel.iban_number)
    }

    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {

        internal var account_number=itemView.findViewById<TextView>(R.id.account_number)
        internal var account_name=itemView.findViewById<TextView>(R.id.account_name)
        internal var bank_name = itemView.findViewById<TextView>(R.id.bank_name)
        internal var iban=itemView.findViewById<TextView>(R.id.iban)

    }
}