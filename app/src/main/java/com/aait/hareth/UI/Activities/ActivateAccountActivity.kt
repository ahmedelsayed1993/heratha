package com.aait.hareth.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Models.ResendCodeModel
import com.aait.hareth.Models.UserModel
import com.aait.hareth.Models.UserResponse
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivateAccountActivity  : Parent_Activity() {
    lateinit var confirm:Button
    lateinit var type:UserModel
    lateinit var back:ImageView
    lateinit var phone:EditText
    lateinit var resend:TextView

    override val layoutResource: Int
        get() = R.layout.activity_activate_account

    override fun initializeComponents() {
        type = intent.getSerializableExtra("user") as UserModel
        confirm = findViewById(R.id.confirm)
        phone = findViewById(R.id.phone)
        resend = findViewById(R.id.resend)
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed() }
        confirm.setOnClickListener(View.OnClickListener {
            if (CommonUtil.checkEditError(phone,getString(R.string.Please_enter_the_activation_code))){
                return@OnClickListener
            }else{
                checkCode()
            }
        })
        resend.setOnClickListener { resend() }
    }
    fun resend(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.resend(type.id!!,mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ResendCodeModel> {
            override fun onFailure(call: Call<ResendCodeModel>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ResendCodeModel>,
                response: Response<ResendCodeModel>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun checkCode(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.checkCode(mLanguagePrefManager.appLanguage,type.id!!,phone.text.toString())?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        if (type.user_type?.equals("user")!!) {
                            mSharedPrefManager.loginStatus = true
                            mSharedPrefManager.userData = response.body()?.data!!
                            val intent=Intent(this@ActivateAccountActivity, MainActivity::class.java)
                            intent.putExtra("type",type)
                            startActivity(intent)
                        }else if (type.user_type?.equals("provider")!!){
                            val intent=Intent(this@ActivateAccountActivity, Provider_acceptActivity::class.java)
                            intent.putExtra("type",type.user_type!!)
                            startActivity(intent)

                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }




}