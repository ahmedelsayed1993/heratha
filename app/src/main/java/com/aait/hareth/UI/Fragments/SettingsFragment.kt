package com.aait.hareth.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.ToggleButton
import com.aait.hareth.Base.BaseFragment
import com.aait.hareth.Client
import com.aait.hareth.Models.SwitchNotification
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.UI.Activities.LanguageActivity
import com.aait.hareth.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SettingsFragment : BaseFragment() {
    lateinit var lang:TextView
    lateinit var lang_lay:LinearLayout
    lateinit var online:ToggleButton
    var i=1
    override val layoutResource: Int
        get() = R.layout.fragment_settings

    override fun initializeComponents(view: View) {
        online = view.findViewById(R.id.online)
        lang = view.findViewById(R.id.lang)
        lang_lay = view.findViewById(R.id.lang_lay)
        if (mLanguagePrefManager.appLanguage.equals("ar")){
            lang.text = getString(R.string.arabic)
        }else{
            lang.text = getString(R.string.english)
        }
        lang_lay.setOnClickListener { startActivity(Intent(activity,LanguageActivity::class.java))
        activity?.finish()}
        online.setOnClickListener {
            if (online.isChecked) {
                i = 1
                switch(1)

            } else {
                i = 0
                switch(0)
            }
        }

    }
    companion object {
        fun newInstance(): SettingsFragment {
            val args = Bundle()
            val fragment = SettingsFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    fun switch(i:Int){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.switchNotification(mSharedPrefManager.userData.id!!,i)?.enqueue(object :
            Callback<SwitchNotification> {
            override fun onFailure(call: Call<SwitchNotification>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()

            }

            override fun onResponse(
                call: Call<SwitchNotification>,
                response: Response<SwitchNotification>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        if (response.body()?.data==0){
                            online.isActivated = false
                        }else if (response.body()?.data==1){
                            online.isActivated = true
                        }
                    }
                }
            }
        })
    }

}