package com.aait.hareth.UI.Activities

import android.app.DatePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Listener.OnItemClickListener
import com.aait.hareth.Models.ListModel
import com.aait.hareth.Models.ListResponse
import com.aait.hareth.Models.UserResponse
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.UI.Adapter.ListAdapter
import com.aait.hareth.Utils.CommonUtil
import com.aait.hareth.Utils.PermissionUtils
import com.aait.hareth.Utils.ProgressRequestBody
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import com.jaiselrahman.filepicker.config.Configurations
import com.jaiselrahman.filepicker.model.MediaFile
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.*

class SeekerRegisterActivity : Parent_Activity(),OnItemClickListener {


    override fun onItemClick(view: View, position: Int) {
        if (selected==0) {
            listModel = listModels.get(position)
            city.setText(listModel.name!!)
            cities.visibility = View.GONE
        }else{
            listModel1 = listModels1.get(position)
            lang.setText(listModel1.name!!)
            langs.visibility = View.GONE
            if (listModel1.id==1){
                lang_lay.visibility = View.GONE
                effecient = null
            }else{
                lang_lay.visibility = View.VISIBLE
            }

        }
    }

    lateinit var register:Button
    lateinit var name:EditText
    lateinit var back:ImageView
    lateinit var image: CircleImageView
    lateinit var city:TextView
    lateinit var gender:TextView
    lateinit var date_of_birth:TextView
    lateinit var length:EditText
    lateinit var weight:EditText
    lateinit var CV:TextView
    lateinit var password:EditText
    lateinit var view:ImageView
    lateinit var check:CheckBox
    lateinit var terms:TextView
    lateinit var press:LinearLayout
    lateinit var type:String
    lateinit var cities: RecyclerView
    lateinit var lang:TextView
    lateinit var langs:RecyclerView
    lateinit var lang_lay:RadioGroup
    lateinit var good:RadioButton
    lateinit var very_good:RadioButton
    lateinit var excellent:RadioButton
    internal var path6: String? = null
    internal var sex:String?=null
    var deviceID=""
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var listModels = ArrayList<ListModel>()
    internal lateinit var listAdapter: ListAdapter
    internal lateinit var linearLayoutManager1: LinearLayoutManager
    internal var listModels1 = ArrayList<ListModel>()
    internal lateinit var listAdapter1: ListAdapter
    private var ImageBasePath: String? = null
    internal lateinit var listModel: ListModel
    internal lateinit var listModel1: ListModel
    internal var selected:Int = 0
    lateinit var Id_num:EditText
    lateinit var education:EditText
    lateinit var phone:EditText
    internal var effecient: String? =null
    private val FILE_REQUEST_CODE = 666
    lateinit var files : ArrayList<MediaFile>
    internal var returnValue: ArrayList<String>? = ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")

    override val layoutResource: Int
        get() = R.layout.activity_seeker_register

    override fun initializeComponents() {
        type = intent.getStringExtra("type")
        files= ArrayList()
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        register = findViewById(R.id.register)
        phone = findViewById(R.id.phone)
        name = findViewById(R.id.name)
        back = findViewById(R.id.back)
        image = findViewById(R.id.image)
        Id_num = findViewById(R.id.Id_num)
        education = findViewById(R.id.education)
        city = findViewById(R.id.city)
        gender = findViewById(R.id.gender)
        cities = findViewById(R.id.cities)
        date_of_birth = findViewById(R.id.date_of_birth)
        length = findViewById(R.id.length)
        weight = findViewById(R.id.weight)
        CV = findViewById(R.id.CV)
        password = findViewById(R.id.password)
        view = findViewById(R.id.view)
        check = findViewById(R.id.check)
        terms = findViewById(R.id.terms)
        press = findViewById(R.id.press)
        lang = findViewById(R.id.lang)
        lang_lay = findViewById(R.id.lang_lay)
        langs = findViewById(R.id.langs)
        good = findViewById(R.id.good)
        very_good = findViewById(R.id.very_good)
        excellent = findViewById(R.id.excellent)
        lang_lay.visibility = View.GONE
        back.setOnClickListener { onBackPressed() }
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter =  ListAdapter(mContext!!,listModels,R.layout.recycler_list)
        listAdapter.setOnItemClickListener(this)
        cities!!.layoutManager= linearLayoutManager
        cities!!.adapter = listAdapter
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter1 =  ListAdapter(mContext!!,listModels1,R.layout.recycler_list)
        listAdapter1.setOnItemClickListener(this)
        langs!!.layoutManager= linearLayoutManager1
        langs!!.adapter = listAdapter1
        getCities()
        getLangs()
        image.setOnClickListener { getPickImageWithPermission() }
        register.setOnClickListener {
            if (ImageBasePath==null){
                if (CommonUtil.checkEditError(name,getString(R.string.sorry_fill_Empty_form))||
                        CommonUtil.checkEditError(phone,getString(R.string.sorry_fill_Empty_form))||
                        CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                        CommonUtil.checkEditError(Id_num,getString(R.string.sorry_fill_Empty_form))||
                        CommonUtil.checkTextError(city,getString(R.string.sorry_fill_Empty_form))||
                        CommonUtil.checkTextError(gender,getString(R.string.sorry_fill_Empty_form))||
                        CommonUtil.checkTextError(date_of_birth,getString(R.string.sorry_fill_Empty_form))||
                        CommonUtil.checkEditError(length,getString(R.string.sorry_fill_Empty_form))||
                        CommonUtil.checkEditError(weight,getString(R.string.sorry_fill_Empty_form))||
                        CommonUtil.checkEditError(education,getString(R.string.sorry_fill_Empty_form))||
                            CommonUtil.checkEditError(password,getString(R.string.sorry_fill_Empty_form))||
                            CommonUtil.checkLength(password,getString(R.string.password_length),6))
                        {
                    return@setOnClickListener
                }else{
                    if (files.isEmpty()){
                        CommonUtil.makeToast(mContext,getString(R.string.Curriculum_Vitae))
                    }else{
                        if (check.isChecked) {
                            register(effecient)
                        }else{
                            CommonUtil.makeToast(mContext,getString(R.string.terms))
                        }
                    }
                }
            }else{
                if (CommonUtil.checkEditError(name,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkEditError(phone,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(Id_num,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkTextError(city,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkTextError(gender,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkTextError(date_of_birth,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkEditError(length,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkEditError(weight,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkEditError(education,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkEditError(password,getString(R.string.sorry_fill_Empty_form))||
                    CommonUtil.checkLength(password,getString(R.string.password_length),6))
                {
                    return@setOnClickListener
                }else{
                    if (files.isEmpty()){
                        CommonUtil.makeToast(mContext,getString(R.string.Curriculum_Vitae))
                    }else{
                        if (check.isChecked) {
                            register(effecient, ImageBasePath!!)
                        }else{
                            CommonUtil.makeToast(mContext,getString(R.string.terms))
                        }
                    }
                }

            }
        }
        view.setOnClickListener { if (password.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
            password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            password.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
        }
        }
        CV.setOnClickListener {
            var intent = Intent(this, FilePickerActivity::class.java)
            intent.putExtra(
                FilePickerActivity.CONFIGS,  Configurations.Builder()
                    .setCheckPermission(true)
                    .setShowFiles(true)
                    .setShowAudios(false)
                    .setShowVideos(false)
                    .setShowImages(false)
                    .setSuffixes("pdf")
                    .setSingleChoiceMode(true)
                    .build())
            startActivityForResult(intent, FILE_REQUEST_CODE)
        }
        gender.setOnClickListener {
            val popupMenu = PopupMenu(mContext, gender)

            popupMenu.inflate(R.menu.gender)
            popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.male -> {
                        gender.setText(getString(R.string.male))
                        sex = "male"
                        return@OnMenuItemClickListener true
                    }
                    R.id.female -> {
                        gender.setText(getString(R.string.female))
                        sex = "female"
                        return@OnMenuItemClickListener true
                    }
                }
                false
            })

            popupMenu.show()
        }
        good.setOnClickListener { effecient="good"
        lang_lay.visibility = View.GONE}
        very_good.setOnClickListener { effecient="very good"
            lang_lay.visibility = View.GONE}
        excellent.setOnClickListener { effecient="excellent"
            lang_lay.visibility = View.GONE}

        city.setOnClickListener {
            selected = 0
            if (cities.visibility ==View.GONE){
                cities.visibility=View.VISIBLE
            }else{
                cities.visibility=View.GONE
            }
        }
        lang.setOnClickListener {
            selected = 1
            if (langs.visibility ==View.GONE){
                langs.visibility=View.VISIBLE
            }else{
                langs.visibility=View.GONE
            }
        }
        date_of_birth.setOnClickListener { val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)


            val dpd = DatePickerDialog(mContext, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val m:Int = monthOfYear+1
                // Display Selected date in textbox
                date_of_birth.setText("" + dayOfMonth + "/" + m + "/" + year)
            }, year, month, day)

            dpd.show() }
    }

    fun getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        )) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.IMAGE_PERMISSIONS,
                        400
                    )
                }
            } else {
                Pix.start(this, options)
                CommonUtil.PrintLogE("Permission is granted before")
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23")
            Pix.start(this, options)
        }
    }
    fun getCities(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.cities(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ListResponse> {

            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()!!.key.equals("1")){
                        listAdapter.updateAll(response.body()!!.data!!)
                    }else{

                    }
                }
            }
        } )
    }
    fun getLangs(){

        Client.getClient()?.create(Service::class.java)?.langs(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ListResponse> {

            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()

            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {

                if (response.isSuccessful){
                    if (response.body()!!.key.equals("1")){
                        listAdapter1.updateAll(response.body()!!.data!!)
                    }else{

                    }
                }
            }
        } )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == FILE_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            files = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES)
            CV.text=files[0].name

        }else  if (requestCode == 100) {
            returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath = returnValue!![0]
            // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
            image!!.setImageURI(Uri.parse(ImageBasePath))
            if (ImageBasePath != null) {
//                    updateImage(ImageBasePath!!)
            }
        }
    }
    fun register(efficient:String?){
        showProgressDialog(getString(R.string.please_wait))

        val file = File(files[0].path)
        Log.e("file",file.toString())
        val requestBody = RequestBody.create(MediaType.parse("*/*"), file)
        val myPart: MultipartBody.Part = MultipartBody.Part.createFormData("cv", file.name, requestBody)

        Client.getClient()?.create(Service::class.java)?.singUpWithoutImage("user",name.text.toString(),phone.text.toString()
        ,listModel.id!!,sex!!,date_of_birth.text.toString(),weight.text.toString(),length.text.toString(),myPart,password.text.toString()
        ,deviceID,"android",Id_num.text.toString(),education.text.toString(),listModel1.id!!,efficient)?.enqueue(object :Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                Log.e("user", Gson().toJson(response.body()?.data))
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        intent = Intent(this@SeekerRegisterActivity,ActivateAccountActivity::class.java)
                        intent.putExtra("user",response.body()?.data!!)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun register(efficient:String?,path:String){
        showProgressDialog(getString(R.string.please_wait))
        val file = File(files[0].path)
        val requestBody = RequestBody.create(MediaType.parse("*/*"), file)
        val myPart: MultipartBody.Part = MultipartBody.Part.createFormData("cv", file.name, requestBody)
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.singUpWithImage("user",name.text.toString(),phone.text.toString()
            ,listModel.id!!,sex!!,date_of_birth.text.toString(),weight.text.toString(),length.text.toString(),myPart,filePart,password.text.toString()
            ,deviceID,"android",Id_num.text.toString(),education.text.toString(),listModel1.id!!,efficient)?.enqueue(object :Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                Log.e("user", Gson().toJson(response.body()?.data))
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        intent = Intent(this@SeekerRegisterActivity,ActivateAccountActivity::class.java)
                        intent.putExtra("user",response.body()?.data!!)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }


    }