package com.aait.hareth

import android.content.Intent
import android.os.Handler
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.UI.Activities.PreLoginActivity
import com.aait.hareth.UI.Activities.MainActivity
import com.aait.hareth.UI.Activities.ProviderMainActivity
import maes.tech.intentanim.CustomIntent

class SplashActivity : Parent_Activity() {
    var isDataLoaded = false
    var isSplashFinishid = false

    lateinit var logo : ImageView
    override val layoutResource: Int
        get() = R.layout.activity_splash

    override fun initializeComponents() {
        CustomIntent.customType(this, "fadein-to-fadeout")
        logo = findViewById(R.id.logo)

        val logoAnimation = AnimationUtils.loadAnimation(this, R.anim.anim_splash)
        val logoAnimation2 = AnimationUtils.loadAnimation(this, R.anim.anim_shine)

        Handler().postDelayed({
            logo.startAnimation(logoAnimation2)
            Handler().postDelayed({
                isSplashFinishid=true
                if (mSharedPrefManager.loginStatus==true){
                    if (mSharedPrefManager.userData.user_type.equals("provider")){
                        startActivity(Intent(this@SplashActivity,ProviderMainActivity::class.java))
                        this@SplashActivity.finish()
                    }else{
                        startActivity(Intent(this@SplashActivity,MainActivity::class.java))
                        this@SplashActivity.finish()
                    }
                }else {

                    var intent = Intent(this@SplashActivity, PreLoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }

            }, 2100)
        }, 1800)
    }





//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_splash)
//    }
}
