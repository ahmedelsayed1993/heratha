package com.aait.hareth.UI.Activities

import android.content.Intent
import android.widget.*
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Models.ResendCodeModel
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddCommentActivity:Parent_Activity() {
    lateinit var menu: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var rating:RatingBar
    lateinit var comment:EditText
    lateinit var add:Button
     var id:Int=0
    override val layoutResource: Int
        get() = R.layout.activity_add_comment

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        rating = findViewById(R.id.rating)
        comment = findViewById(R.id.comment)
        add = findViewById(R.id.add)
        menu.setOnClickListener { onBackPressed() }
        notification.setOnClickListener { startActivity(Intent(this@AddCommentActivity,NotificationActivity::class.java)) }
        title.text = getString(R.string.Add_a_comment)
        add.setOnClickListener { if (CommonUtil.checkEditError(comment,getString(R.string.Please_write_your_comment))){
        return@setOnClickListener}else{
            add()
        }
        }

    }
    fun add(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddComment(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id,comment.text.toString(),rating.rating.toInt())?.enqueue(object :
            Callback<ResendCodeModel> {
            override fun onFailure(call: Call<ResendCodeModel>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ResendCodeModel>,
                response: Response<ResendCodeModel>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        onBackPressed()
                    }else{

                    }
                }
            }
        })
    }
}