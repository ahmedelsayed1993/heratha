package com.aait.hareth.UI.Activities

import android.app.Dialog
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.Client
import com.aait.hareth.Listener.OnItemClickListener
import com.aait.hareth.Models.*
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.UI.Adapter.CommentsAdapter
import com.aait.hareth.UI.Adapter.HomeProviderAdapter
import com.aait.hareth.UI.Adapter.ReasonsAdapter
import com.aait.hareth.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CommentsActivity :Parent_Activity(),OnItemClickListener{

    lateinit var cancelModel: CancelModel
    var reason_id = 0
    val cancelModels = ArrayList<CancelModel>()
    override fun onItemClick(view: View, position: Int) {
        val dialog = Dialog(mContext!!)
        if (view.id == R.id.reason){
            cancelModel = cancelModels.get(position)
            reason_id=cancelModel.id!!

        }else {

            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.setCancelable(false)
            dialog?.setContentView(R.layout.dialog_cancel)
            val delete = dialog?.findViewById<ImageView>(R.id.delete)
            val send = dialog?.findViewById<Button>(R.id.send)
            val resons = dialog?.findViewById<RecyclerView>(R.id.reasons)
            val linearLayoutManager1: LinearLayoutManager
            val reasonsAdapter: ReasonsAdapter

            delete?.setOnClickListener { dialog?.dismiss() }
            linearLayoutManager1 =
                LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
            reasonsAdapter = ReasonsAdapter(
                mContext,
                cancelModels,
                R.layout.recycler_reasons
            )
            reasonsAdapter.setOnItemClickListener(this)
            resons.layoutManager = linearLayoutManager1
            resons.adapter = reasonsAdapter
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)
                ?.getResons(mLanguagePrefManager.appLanguage)
                ?.enqueue(object : Callback<CancelResponse> {
                    override fun onFailure(call: Call<CancelResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext, t)
                        t.printStackTrace()
                        hideProgressDialog()
                    }

                    override fun onResponse(
                        call: Call<CancelResponse>,
                        response: Response<CancelResponse>
                    ) {
                        hideProgressDialog()
                        if (response.isSuccessful) {
                            if (response.body()?.key.equals("1")) {
                                reasonsAdapter.updateAll(response.body()?.data!!)
                            } else {

                            }
                        }
                    }

                })
            send.setOnClickListener {
                if (reason_id==0){

                }else{
                    showProgressDialog(getString(R.string.please_wait))
                    Client.getClient()?.create(Service::class.java)?.cancelComment(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,
                        reason_id,commentModels.get(position).id!!)?.enqueue(object :Callback<ResendCodeModel>{
                        override fun onFailure(call: Call<ResendCodeModel>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                            dialog?.dismiss()
                        }

                        override fun onResponse(
                            call: Call<ResendCodeModel>,
                            response: Response<ResendCodeModel>
                        ) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.key.equals("1")){
                                    dialog?.dismiss()
                                    val dialog1 =  Dialog(mContext!!)
                                    dialog1?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                    dialog1?.setCancelable(false)
                                    dialog1?.setContentView(R.layout.dialog_done)
                                    val back = dialog1?.findViewById<Button>(R.id.back)
                                    back.setOnClickListener { dialog1?.dismiss()
                                    onBackPressed()}
                                    dialog1?.show()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.data!!)
                                    dialog?.dismiss()
                                }
                            }
                        }

                    })
                }
            }


            dialog?.show()
        }

    }

    lateinit var menu: ImageView
    lateinit var title: TextView
    lateinit var logo: ImageView
    lateinit var rv_recycle: RecyclerView
    lateinit var notification:ImageView

    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null
    lateinit var search:ImageView

    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var commentModels = java.util.ArrayList<CommentModel>()
    internal lateinit var commentsAdapter: CommentsAdapter
    override val layoutResource: Int
        get() = R.layout.activity_comments

    override fun initializeComponents() {
        search = findViewById(R.id.search)
        search.visibility =View.GONE
        notification = findViewById(R.id.notification)
        notification.setOnClickListener { startActivity(Intent(this@CommentsActivity,NotificationActivity::class.java)) }
        menu = findViewById(R.id.menu)
        logo = findViewById(R.id.logo)
        title = findViewById(R.id.title)
        logo.visibility = View.GONE
        title.visibility = View.VISIBLE
        title.setText(R.string.comments)
        menu.setOnClickListener { onBackPressed() }
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        commentsAdapter = CommentsAdapter(mContext,commentModels,R.layout.recycler_comments)
        commentsAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = commentsAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome() }
        getHome()

    }
    fun getHome(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.getComments(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<CommentsResponse> {
            override fun onResponse(call: Call<CommentsResponse>, response: Response<CommentsResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.key.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                commentsAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {}

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<CommentsResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })
    }
}