package com.aait.hareth.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.hareth.Base.BaseFragment
import com.aait.hareth.Client
import com.aait.hareth.Listener.OnItemClickListener
import com.aait.hareth.Models.MyJobsModel
import com.aait.hareth.Models.MyJobsResponse
import com.aait.hareth.Models.ProviderOrderModel
import com.aait.hareth.Models.ProviderOrdersResponse
import com.aait.hareth.Network.Service
import com.aait.hareth.R
import com.aait.hareth.UI.Activities.ProviderOrderDetails
import com.aait.hareth.UI.Adapter.HomeProviderAdapter
import com.aait.hareth.UI.Adapter.ProviderOrdersAdapter
import com.aait.hareth.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProviderRequestsFragment:BaseFragment(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(activity,ProviderOrderDetails::class.java)
        intent.putExtra("id",providerOrderModels.get(position))

        startActivity(intent)
    }

    lateinit var rv_recycle: RecyclerView

    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    internal var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var providerOrderModels = java.util.ArrayList<ProviderOrderModel>()
    internal lateinit var providerOrdersAdapter: ProviderOrdersAdapter

    companion object {
        fun newInstance(): ProviderRequestsFragment {
            val args = Bundle()
            val fragment = ProviderRequestsFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    override val layoutResource: Int
        get() = R.layout.app_recycle

    override fun initializeComponents(view: View) {
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        providerOrdersAdapter =  ProviderOrdersAdapter(mContext!!,providerOrderModels,R.layout.recycler_provider_orders)
        providerOrdersAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = providerOrdersAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome() }
        getHome()
    }
    fun getHome(){

        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.providerOrder(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<ProviderOrdersResponse> {
            override fun onResponse(call: Call<ProviderOrdersResponse>, response: Response<ProviderOrdersResponse>) {
                swipeRefresh!!.isRefreshing = false

                if (response.isSuccessful) {
                    try {
                        if (response.body()?.key.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                providerOrdersAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {}

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<ProviderOrdersResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false

                Log.e("response", Gson().toJson(t))
            }
        })
    }
}