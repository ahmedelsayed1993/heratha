package com.aait.hareth.UI.Activities

import android.content.Intent
import android.widget.Button
import com.aait.hareth.Base.Parent_Activity
import com.aait.hareth.R

class Provider_acceptActivity:Parent_Activity() {
    lateinit var type:String
    lateinit var Back:Button
    override val layoutResource: Int
        get() = R.layout.activity_provider_accept

    override fun initializeComponents() {

        type = intent.getStringExtra("type")
        Back = findViewById(R.id.back)
        Back.setOnClickListener {  val intent= Intent(this@Provider_acceptActivity,SeekerLoginActivity::class.java)
            intent.putExtra("type","provider")
            startActivity(intent)
        this.finish()}

    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent= Intent(this@Provider_acceptActivity,SeekerLoginActivity::class.java)
        intent.putExtra("type","provider")
        startActivity(intent)
        this.finish()
    }
}