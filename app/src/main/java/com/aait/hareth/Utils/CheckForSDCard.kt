package com.aait.hareth.Utils

import android.os.Environment

object CheckForSDCard {
    val isSDCardPresent: Boolean
        get() = if (Environment.getExternalStorageState() ==

            Environment.MEDIA_MOUNTED
        ) {
            true
        } else false
}
