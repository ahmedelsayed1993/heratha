package com.aait.hareth.Utils


import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.ContentUris
import android.content.Context
import android.content.Context.INPUT_METHOD_SERVICE
import android.os.Build
import android.util.Log
import com.google.gson.Gson
import android.content.Intent
import android.database.Cursor
import android.graphics.*
import android.net.Uri
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.aait.hareth.R
import com.aait.hareth.UI.Views.Toaster
import com.aait.hareth.Utils.CommonUtil
import com.squareup.picasso.Picasso
import id.zelory.compressor.Compressor
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class MyUtils{

    companion object {

        fun printLog(name: String, o: Any) {
            Log.e(name, Gson().toJson(o))
        }

        @SuppressLint("NewApi")
        fun isAppIsInBackground(context: Context): Boolean {
            var isInBackground = true
            val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                val runningProcesses = am.runningAppProcesses
                for (processInfo in runningProcesses) {
                    if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        for (activeProcess in processInfo.pkgList) {
                            if (activeProcess == context.packageName) {
                                isInBackground = false
                            }
                        }
                    }
                }
            } else {
                val taskInfo = am.getRunningTasks(1)
                val componentInfo = taskInfo[0].topActivity
                if ((componentInfo?.packageName ?: context.packageName) as Boolean) {
                    isInBackground = false
                }
            }
            return isInBackground
        }

        fun intentUrl(context: Context, url: String) { context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url))) }

        fun bitmapFromUrl(context: Context, url: String): Bitmap? {
            try {
                var bmp: Bitmap = Picasso.with(context).load(url).resize(300, 300).get()
                return bmp
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return null
        }

        private fun circleBitmap(bitmap: Bitmap): Bitmap {
            val output = Bitmap.createBitmap(bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(output)
            val color = Color.RED
            val paint = Paint()
            val rect = Rect(0, 0, bitmap.width, bitmap.height)
            val rectF = RectF(rect)
            paint.isAntiAlias = true
            canvas.drawARGB(0, 0, 0, 0)
            paint.color = color
            canvas.drawOval(rectF, paint)
            paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
            canvas.drawBitmap(bitmap, rect, rect, paint)
            bitmap.recycle()
            return output
        }

        fun shareUrl(context: Context, title: String, body: String, url: String) {
            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.putExtra(Intent.EXTRA_SUBJECT, title) //header or title
            var sAux = "\n $body \n\n" // message body
            sAux += "$url \n\n"
            share.putExtra(Intent.EXTRA_TEXT, sAux)
            context.startActivity(Intent.createChooser(share, title))
        }

        fun rotateImage(img: Bitmap, degree: Float): Bitmap {
            val matrix = Matrix()
            matrix.postRotate(degree)
            return Bitmap.createBitmap(img, 0, 0, img.width, img.height, matrix, true)
        }

        fun flipImage(img: Bitmap, horizental: Boolean, vertical: Boolean): Bitmap {

            val matrix = Matrix()
            matrix.preScale((if (horizental) -1 else 1).toFloat(), (if (vertical) -1 else 1).toFloat())
            return Bitmap.createBitmap(img, 0, 0, img.width, img.height, matrix, true)
        }

        fun requestFocus(view: View, window: Window) {
            if (view.requestFocus()) {
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
            }
        }

        fun HideKeyboard(context: Context) {
            val inputMethodManager = context.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow((context as Activity).currentFocus!!.windowToken, 0)
        }



        fun makePhoneCall(context: Context,number: String) {
            try { context.startActivity(Intent(Intent.ACTION_DIAL, Uri.parse("tel:$number")))
            } catch (e: Exception) { e.printStackTrace() }
        }
        fun Context.sedToWhatsAppNumber(number: String){
            try{
                val uri = Uri.parse("smsto:$number")
                val i = Intent(Intent.ACTION_SENDTO, uri)
                i.setPackage("com.whatsapp")
                startActivity(Intent.createChooser(i, ""))
            }catch (e: Exception) {
                e.printStackTrace()
            }

        }
        fun Context.sedToWhatsApp(message: String){
            try{
                val sendIntent = Intent()
                sendIntent.action = Intent.ACTION_SEND
                sendIntent.putExtra(Intent.EXTRA_TEXT, message)
                sendIntent.type = "text/plain"
                sendIntent.setPackage("com.whatsapp")
                startActivity(sendIntent)
            }catch (e: Exception) { e.printStackTrace() }

        }


        fun reduceImageSize(context:Context,path:Uri,realPROFILEImagePath:String,imageView: ImageView){
            if (realPROFILEImagePath!=null) {
                var img: File =  File(realPROFILEImagePath)
                Log.e("1111111",img.length().toString())
                if (img.length()>1500000){
                    var compressedImgFile =  Compressor(context).compressToFile(img)
                    Log.e("2222222",compressedImgFile.length().toString())
                    var imgBitMap= Compressor(context).compressToBitmap(compressedImgFile)
                    imageView.setImageBitmap(imgBitMap)
                }
                else{
                    Picasso.with(context)
                        .load(path)
                        .fit()
                        .into(imageView)
                }
            }else{

                CommonUtil.makeToast(context,context.resources.getString(R.string.error_in_loading_image_select_another_one))}
        }


        fun getdate(context:Context,textView:TextView,disableLastDates:Boolean){
            var myCalendar: Calendar? = Calendar.getInstance()
            val date = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                myCalendar?.set(Calendar.YEAR, year)
                myCalendar?.set(Calendar.MONTH, monthOfYear)
                myCalendar?.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val myFormat = "dd-MM-yyyy"
                val sdf = SimpleDateFormat(myFormat, Locale.US)
                textView.text = sdf.format(myCalendar?.time)
            }
            textView.setOnClickListener{
                var datePickerDialog=DatePickerDialog(context, date, myCalendar?.get(Calendar.YEAR)!!, myCalendar?.get(Calendar.MONTH)!!, myCalendar?.get(Calendar.DAY_OF_MONTH)!!)
                if(disableLastDates){datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000}
                datePickerDialog.show()
            }
        }

        fun getTime(context:Context,textView:TextView){
            val cal = Calendar.getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)
                textView.text = SimpleDateFormat("hh:mm a",Locale.US).format(cal.time)
            }
            textView.setOnClickListener{ TimePickerDialog(context, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), false).show() }
        }


        fun getNumFromString (string:String):String{
            return string.replace("[^0-9,.]", "")
        }

//        fun dateFormateConvert(inputDate:String,inputFormat:String,outFormat:String):String{
//            var dateFormat= SimpleDateFormat(inputFormat)
//            var myDate:Date?=null
//            try { myDate = dateFormat.parse(inputDate) } catch (e: ParseException) { e.printStackTrace() }
//            var timeFormat = SimpleDateFormat(outFormat, Locale.US)
//            var finalDate = timeFormat.format(myDate)
//            return finalDate
//        }
//        fun timeFormateConvert(){
//
//
//        }


        fun CintentToGoogleMapLocation(context: Context,lat:String,lng:String ,name: String){
             var intent = Intent(Intent.ACTION_VIEW,
                Uri.parse("geo:0,0?q=$lat,$lng ($name)"))
                context.startActivity(intent)
        }
        fun intentToGoogleMapDirections(context: Context,lat:String,lng:String){
            val url = "http://maps.google.com/maps?daddr=$lat,$lng"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            context.startActivity(intent)
        }

        fun intentToGmail(context: Context,mailAddress:String){
            val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", mailAddress, null))
            // emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name))
            context.startActivity(Intent.createChooser(emailIntent, "Send email..."))
        }



        // get real path from uri    for multipart
//        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//        @RequiresApi(Build.VERSION_CODES.KITKAT)
//        fun getPathFromUri(context: Context, uri: Uri): String? {
//
//            val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
//
//            // DocumentProvider
//            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
//                // ExternalStorageProvider
//                if (isExternalStorageDocument(uri)) {
//                    val docId = DocumentsContract.getDocumentId(uri)
//                    val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//                    val type = split[0]
//
//                    if ("primary".equals(type, ignoreCase = true)) {
//                        return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
//                    }
//                } else if (isDownloadsDocument(uri)) {
//
//                    val id = DocumentsContract.getDocumentId(uri)
//                    val contentUri = ContentUris.withAppendedId(
//                            Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id)!!)
//
//                    return getDataColumn(context, contentUri, null, null)
//                } else if (isMediaDocument(uri)) {
//                    val docId = DocumentsContract.getDocumentId(uri)
//                    val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//                    val type = split[0]
//
//                    var contentUri: Uri? = null
//                    if ("image" == type) {
//                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
//                    } else if ("video" == type) {
//                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
//                    } else if ("audio" == type) {
//                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
//                    }
//
//                    val selection = "_id=?"
//                    val selectionArgs = arrayOf(split[1])
//
//                    return getDataColumn(context, contentUri, selection, selectionArgs)
//                }// MediaProvider
//            } else if ("content".equals(uri.scheme, ignoreCase = true)) {
//                return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(context, uri, null, null)
//            } else if ("file".equals(uri.scheme, ignoreCase = true)) {
//                return uri.path
//            }
//            return null
//        }
        fun getDataColumn(context: Context, uri: Uri?, selection: String?, selectionArgs: Array<String>?): String? {
            var cursor: Cursor? = null
            val column = "_data"
            val projection = arrayOf(column)
            try {
                cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
                if (cursor != null && cursor.moveToFirst()) {
                    val index = cursor.getColumnIndexOrThrow(column)
                    return cursor.getString(index)
                }
            } finally {
                if (cursor != null)
                    cursor.close()
            }
            return null
        }
        fun isExternalStorageDocument(uri: Uri): Boolean { return "com.android.externalstorage.documents" == uri.authority }
        fun isDownloadsDocument(uri: Uri): Boolean { return "com.android.providers.downloads.documents" == uri.authority }
        fun isMediaDocument(uri: Uri): Boolean { return "com.android.providers.media.documents" == uri.authority }
        fun isGooglePhotosUri(uri: Uri): Boolean { return "com.google.android.apps.photos.content" == uri.authority }
    }
}