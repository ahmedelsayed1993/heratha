package com.aait.hareth.Utils

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.animation.Animation
import android.widget.ImageView
import com.aait.hareth.R

class MyUtilsAnimitions {
    companion object{
        fun colorChanging(image:ImageView,speed:Int,color1:Float,color2:Float){
            val anim = ValueAnimator.ofFloat(0f, 1f)
            anim.duration = speed.toLong()
            val hsv = FloatArray(3)
            var runColor: Int
            // Transition color
//            hsv[1] = .4f
//            hsv[2] = .4f
            hsv[1] = color1
            hsv[2] = color2
            anim.addUpdateListener { animation ->
                hsv[0] = 360 * animation.animatedFraction
                runColor = Color.HSVToColor(hsv)
                image.setColorFilter(runColor)
            }
            anim.repeatCount = Animation.INFINITE
            anim.start()
        }


        fun backGroundColorChanging(context:Context,view: View){
            val set = AnimatorInflater.loadAnimator(context, R.animator.anim_color_changing) as AnimatorSet
            set.setTarget(view)
            set.start()
        }

    }
}