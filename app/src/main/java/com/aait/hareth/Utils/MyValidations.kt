package com.aait.hareth.Utils

import android.content.Context
import android.widget.CheckBox
import android.widget.EditText
import com.aait.hareth.R

class MyValidations{

    companion object {

//        fun checEmptyValidation(context: Context, editText: EditText): Boolean {
//            if (editText.text.toString().trim { it <= ' ' }.isEmpty()) {
//                editText.error = context.getString(R.string.sorry_fill_Empty_form)
//                editText.requestFocus()
//
//                return false
//            } else { return true }
//        }

//        fun checkName(editText: EditText): Boolean {
//            if (editText.text.toString().matches("^[a-zA-Z].*".toRegex())) { return true }
//            else {
//                editText.error = "user name shouldn't start's with number or special character"
//                return false
//            }
//        }

//        fun checkPhoneSize(context: Context,editText: EditText): Boolean {
//            if (editText.text.toString().length < 9) {
//                editText.error = context.getString(R.string.sorry_phone_nuber_isnt_correct)
//                editText.requestFocus()
//
//                return false
//            } else { return true }
//        }
//
//
//        fun checkEmail(context: Context, email: EditText): Boolean {
//            if (!email.text.toString().matches("^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))?$".toRegex())) {
//                email.error = context.getString(R.string.sorry_this_email_isnt_correct)
//                email.requestFocus()
//                return false
//            } else { return true }
//        }
//
//        fun checkPassSize(context: Context, editText: EditText): Boolean {
//            if (editText.text.toString().length < 6) {
//                editText.error = context.getString(R.string.password_must_be_at_least_6_digit)
//                editText.requestFocus()
//                return false
//            } else { return true }
//        }
//
//        fun checkMatch(context: Context, et_pass: EditText, et_confirm_pass: EditText): Boolean {
//            var isMatch = false
//            if (et_pass.text.toString().matches(et_confirm_pass.text.toString().toRegex())) {
//                isMatch = true
//            } else {
//                et_confirm_pass.error = context.getString(R.string.sorry_password_and_confirm_password_is_not_the_same)
//                et_confirm_pass.requestFocus()
//            }
//            return isMatch
//        }

        fun checkCheckBox(context: Context,checkbox: CheckBox,msg:String): Boolean {
            if (checkbox.isChecked) { return true }
            else {
                CommonUtil.makeToast(context,msg)
                return false }
        }

    }
}