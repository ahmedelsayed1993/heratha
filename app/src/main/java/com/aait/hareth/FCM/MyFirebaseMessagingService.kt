package com.aait.hareth.FCM

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.aait.hareth.Perefernces.LanguagePrefManager
import com.aait.hareth.Perefernces.SharedPrefManager
import com.aait.hareth.R
import com.aait.hareth.UI.Activities.NotificationActivity
import com.aait.hareth.Utils.CommonUtil
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson

/**
 * Created by aait on 11/4/2019.
 */
class MyFirebaseMessagingService : FirebaseMessagingService() {

    private val TAG = "FCM Messaging"

    internal var notificationTitle: String? = null

    internal var notificationMessage: String? = null

    internal var notificationData: String? = null

    internal lateinit var mSharedPrefManager: SharedPrefManager

    internal var notificationType: String? = null

    // OrderModel mOrderModel;

    internal lateinit var mLanguagePrefManager: LanguagePrefManager

    override fun onNewToken(s: String?) {
        super.onNewToken(s)
        Log.e("NEW_TOKEN", s!!)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

       // remoteMessage.notification?.let { CommonUtil.onPrintLog(it) }
        CommonUtil.onPrintLog(remoteMessage.data)
        mSharedPrefManager = SharedPrefManager(applicationContext)
        mLanguagePrefManager = LanguagePrefManager(applicationContext)

        //        if (!mSharedPrefManager.getUserData().getNotification_status().equals("")) {
        //            return;
        //        }


        //        notificationType = remoteMessage.getData().get("type");
        notificationTitle = remoteMessage.data["title"]
        notificationMessage = remoteMessage.data["body"]
        notificationType = remoteMessage.data["type"]
        // if the notification contains data payload
        if (remoteMessage == null) {
            return
        }

        // if the user not logged in never do any thing
        if (!mSharedPrefManager.loginStatus!!) {
            return
        } else {
            if (remoteMessage.data["type"] == "order") {
                val intent = Intent(this, NotificationActivity::class.java)
                showNotification(remoteMessage, intent)
            } else if (remoteMessage.data["type"] == "public_notification") {
                val intent = Intent(this, NotificationActivity::class.java)
                showNotification(remoteMessage, intent)
            }

        }
    }

    private fun showNotification(message: RemoteMessage, intent: Intent) {

        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK


        val pendingIntent = PendingIntent.getActivity(
            this,
            0,
            intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val myNotification = NotificationCompat.Builder(this)
            .setContentTitle(message.data["title"])
            .setContentText(message.data["body"])
            .setTicker("Notification!")
            .setWhen(System.currentTimeMillis())
            .setContentIntent(pendingIntent)
            .setDefaults(Notification.DEFAULT_SOUND)
            .setAutoCancel(true)
            .setPriority(Notification.PRIORITY_MAX)
            .setSmallIcon(R.mipmap.logo)
        val notificationManager =
            this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel =
                NotificationChannel("10001", "NOTIFICATION_CHANNEL_NAME", importance)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.DKGRAY
            notificationChannel.setShowBadge(true)
            notificationChannel.enableVibration(true)
            notificationChannel.vibrationPattern =
                longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
            myNotification.setChannelId("10001")
            notificationManager.createNotificationChannel(notificationChannel)
        }
        notificationManager.notify(0 /* Request Code */, myNotification.build())
    }

}