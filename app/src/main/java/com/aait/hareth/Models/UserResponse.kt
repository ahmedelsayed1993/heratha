package com.aait.hareth.Models

import java.io.Serializable

class UserResponse:Serializable {
    var data:UserModel?=null
    var key:String?=null
    var value:String?=null
    var msg:String?=null
}