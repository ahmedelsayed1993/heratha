package com.aait.hareth.Models

import java.io.Serializable

class ProviderOrderModel:Serializable {
    var id:Int?=null
    var job_id:Int?=null
    var title:String?=null
    var specialty:String?=null
    var created:String?=null
}