package com.aait.hareth.Models

import java.io.Serializable

class ListModel:Serializable {
    constructor(id: Int?, name: String?) {
        this.id = id
        this.name = name
    }

    var id:Int?=null
    var name:String?=null
}