package com.aait.hareth.Models

import java.io.Serializable

class ProviderOrderDetails:Serializable {
    var key:String?=null
    var value:String?=null
    var msg:String?=null
    var data:JobDetailsModel?=null
    var requests:ArrayList<RequestModel>?=null
}