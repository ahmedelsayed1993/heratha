package com.aait.hareth.Models

import java.io.Serializable

class ResetPasswordModel :Serializable{
    var key:String?=null
    var value:String?=null
    val msg:String?=null
    val data:Int?=null
}