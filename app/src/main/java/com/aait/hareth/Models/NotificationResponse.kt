package com.aait.hareth.Models

import java.io.Serializable

class NotificationResponse:Serializable {
    var key:String?=null
    var value:String?=null
    var data:ArrayList<NotificationModel>?=null
}