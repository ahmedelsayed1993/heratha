package com.aait.hareth.Models

import java.io.Serializable

class ComapnyCommentsModel : Serializable {
    var id:Int?=null
    var rate:String?=null
    var comment:String?=null
    var username:String?=null
    var created:String?=null
}