package com.aait.hareth.Models

import java.io.Serializable

class SwitchNotification:Serializable {
    var key:String?= null
    var value:String?=null
    var data:Int?=null
}