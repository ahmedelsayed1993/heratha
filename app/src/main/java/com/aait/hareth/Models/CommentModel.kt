package com.aait.hareth.Models

import java.io.Serializable

class CommentModel:Serializable {
    var id:Int?=null
    var rate:String?=null
    var comment:String?=null
    var name:String?=null
    var created:String?=null
}