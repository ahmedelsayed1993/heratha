package com.aait.hareth.Models

import java.io.Serializable

class QuestionsModel :Serializable {
    var key:String?=null
    var value:String?=null
    var data:ArrayList<QuestionModel>?=null
}