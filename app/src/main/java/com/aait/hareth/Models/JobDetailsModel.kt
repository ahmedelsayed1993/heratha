package com.aait.hareth.Models

import java.io.Serializable

class JobDetailsModel:Serializable {
    var id:Int?=null
    var title:String?=null
    val title_ar:String?=null
    val title_en:String?=null
    val specialty:String?=null
    val specialty_id:Int?=null
    val salary:String?=null
    val city:String?=null
    val city_id:Int?=null
    val direction_key:String?=null
    val direction:String?=null
    val certificates:String?=null
    val certificates_ar:String?=null
    val certificates_en:String?=null
    val height:Int?=null
    val width:Int?=null
    val age:Int?=null
    val experience:String?=null
    val experience_en:String?=null
    val experience_ar:String?=null
    val start_date:String?=null
    val end_date:String?=null
    var reason_rejected:String?=null


}