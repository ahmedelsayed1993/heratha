package com.aait.hareth.Models

import java.io.Serializable

class CompanyInfoModel:Serializable {
    var name:String?=null
    var phone:String?=null
    var city:String?=null
}