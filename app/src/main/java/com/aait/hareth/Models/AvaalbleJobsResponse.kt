package com.aait.hareth.Models

import java.io.Serializable

class AvaalbleJobsResponse:Serializable {
    var key:String?=null
    var value:String?=null
    var companyInfo:JobModel?=null
    var jobs:ArrayList<CompanyJobs>?=null
}