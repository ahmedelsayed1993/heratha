package com.aait.hareth.Models

import java.io.Serializable

class UserModel :Serializable{
    var id:Int?=null
    var name:String?= null
    var phone:String?=null
    var code:String?=null
    var email:String?=null
    var commercial_register:String?=null
    var city_id:Int?=null
    var city_name:String?=null
    var user_type:String?=null
    var device_id:String?=null
    var device_type:String?=null
    var gender:String?=null
    var width:String?=null
    var height:String?=null
    var rate:Float?=null
    var birth_date:String?=null
    var cv:String?=null
    var avatar:String?=null
    var date:String?=null
    var idNumber:String?=null
    var education:String?=null
    var language:String?=null
    var efficiency:String?=null
    var language_id:Int?=null

}