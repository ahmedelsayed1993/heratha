package com.aait.hareth.Models

import java.io.Serializable

class ProviderSearchModel :Serializable{
    var key:String?=null
    var value:String?=null
    var data:ArrayList<RequestModel>?=null
}