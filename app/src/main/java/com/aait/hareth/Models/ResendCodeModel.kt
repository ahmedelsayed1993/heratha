package com.aait.hareth.Models

import java.io.Serializable

class ResendCodeModel:Serializable {
    var data:String?=null
    var key:String?=null
    var value:String?=null
    var msg:String?=null
}