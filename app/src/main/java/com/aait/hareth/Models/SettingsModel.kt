package com.aait.hareth.Models

import java.io.Serializable

class SettingsModel :Serializable {
    var key:String?=null
    var value:String?=null
    var data:String?=null
}