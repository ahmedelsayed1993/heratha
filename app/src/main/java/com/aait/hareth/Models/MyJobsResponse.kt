package com.aait.hareth.Models

import java.io.Serializable

class MyJobsResponse:Serializable {
    var key:String?=null
    var value:String?=null
    var data:ArrayList<MyJobsModel>?=null
}