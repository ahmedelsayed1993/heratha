package com.aait.hareth.Models

import java.io.Serializable

class ListResponse:Serializable {
    var key:String?=null
    var value:String?=null
    var data:ArrayList<ListModel>?=null
}