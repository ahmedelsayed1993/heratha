package com.aait.hareth.Models

import com.google.android.gms.common.api.internal.IStatusCallback
import java.io.Serializable

class DirectionsModel:Serializable {


    constructor(id: String?, name: String?) {
        this.id = id
        this.name = name
    }

    var id:String?=null
    var name:String?=null

}