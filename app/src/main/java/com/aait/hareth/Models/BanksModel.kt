package com.aait.hareth.Models

import java.io.Serializable

class BanksModel :Serializable{
    var id:Int?=null
    var bank_name:String?=null
    var account_name:String?=null
    var account_number:String?=null
    var iban_number:String?=null
}