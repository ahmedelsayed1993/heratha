package com.aait.hareth.Models

import java.io.Serializable

class RequestDetailsResponse:Serializable {
    var key:String?=null
    var value:String?=null
    var msg:String?=null
    var data:RequestDetailsModel?=null
}