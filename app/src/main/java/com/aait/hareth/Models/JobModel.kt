package com.aait.hareth.Models

import java.io.Serializable

class JobModel : Serializable {
    var id:Int?=null
    var name:String?=null
    var city:String?=null
    var image:String?=null
    var rate:Float?=null
}