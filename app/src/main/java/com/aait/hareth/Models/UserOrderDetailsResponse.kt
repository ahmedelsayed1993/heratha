package com.aait.hareth.Models

import java.io.Serializable

class UserOrderDetailsResponse:Serializable {
    var key:String?=null
    var value:String?=null
    var msg:String?=null
    var companyInfo:CompanyInfoModel?=null
    var data:JobDetailsModel?=null
}