package com.aait.hareth.Models

import java.io.Serializable

class SeekerHomeModel :Serializable {
    var key:String?=null
    var value:String?=null
    var slider:ArrayList<SliderModel>?=null
    var data:ArrayList<JobModel>?=null
}