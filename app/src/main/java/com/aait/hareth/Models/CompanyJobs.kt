package com.aait.hareth.Models

import java.io.Serializable

class CompanyJobs : Serializable {
    var id:Int ?=null
    var title:String ?=null
    var specialty:String ?=null
    var date:String ?=null
    var provider_id:String?=null
}