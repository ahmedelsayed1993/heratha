package com.aait.hareth.Models

import java.io.Serializable

class UserOrdersResponse:Serializable {
    var key:String?=null
    var value:String?=null
    var data:ArrayList<UserOrdersModels>?=null
}