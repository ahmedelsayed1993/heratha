package com.aait.hareth.Models

import java.io.Serializable

class UserOrdersModels :Serializable{
    var id:Int?=null
    var job_id:Int?=null
    var avatar:String?=null
    var name:String?=null
    var city:String?=null
    var rate:Double?=null
    var status:String?=null
}