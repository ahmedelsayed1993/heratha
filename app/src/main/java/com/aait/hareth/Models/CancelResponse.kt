package com.aait.hareth.Models

import java.io.Serializable

class CancelResponse:Serializable {
    var key:String?=null
    var value:String?=null
    var data:ArrayList<CancelModel>?=null
}