package com.aait.hareth.Models

import java.io.Serializable

class CommentsResponse:Serializable {
    var key:String?=null
    var value:String?=null
    var msg:String?=null
    var data:ArrayList<CommentModel>?=null
}