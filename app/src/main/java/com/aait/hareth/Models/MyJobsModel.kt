package com.aait.hareth.Models

import java.io.Serializable

class MyJobsModel:Serializable {
    var id:Int ?=null
    var titel:String ?=null
    var specialty:String ?=null
    var created:String ?=null
    var provider_id:String?=null
}