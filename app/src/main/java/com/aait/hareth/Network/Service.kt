package com.aait.hareth.Network

import com.aait.hareth.Models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Query

interface Service {
    @POST("home")
    fun getHome(@Query("lang") lang:String):Call<SeekerHomeModel>

    @POST("about")
    fun getAbout(@Query("lang") lang: String):Call<SettingsModel>

    @POST("terms")
    fun getTerms(@Query("lang") lang: String):Call<SettingsModel>

    @POST("questions")
    fun getQuestions(@Query("lang") lang:String):Call<QuestionsModel>

    @POST("contact-us")
    fun contact(@Query("lang") lang:String,
                @Query("name") name:String,
                @Query("phone") phone:String,
                @Query("message") message:String):Call<SettingsModel>

    @POST("complaints")
    fun complain(@Query("lang") lang:String,
                @Query("name") name:String,
                @Query("phone") phone:String,
                @Query("message") message:String):Call<SettingsModel>

    @POST("my-jobs")
    fun myJobs(@Query("lang") lang:String,
               @Query("provider_id") provider_id:Int):Call<MyJobsResponse>


    @POST("search")
    fun searchclient(@Query("lang") lang: String,
                     @Query("text") text:String):Call<SeekerHomeModel>

    @POST("provider-orders")
    fun providerOrder(@Query("lang") lang:String,
                      @Query("provider_id") provider_id:Int):Call<ProviderOrdersResponse>

    @POST("cities")
    fun cities(@Query("lang") lang:String):Call<ListResponse>

    @Multipart
    @POST("sign-up-provider")
    fun signUpProvider(@Query("lang") lang:String,
                       @Query("user_type") user_type:String,
                       @Query("name") name:String,
                       @Query("phone") phone:String,
                       @Query("commercial_register") commercial_register:String,
                       @Query("city_id") city_id:Int,
                       @Query("password") password:String,
                       @Query("email") email:String,
                       @Query("device_id") device_id:String,
                       @Query("device_type") device_type:String,
                       @Part avatar: MultipartBody.Part):Call<UserResponse>
    @POST("update-password")
    fun UpdatePass(@Query("user_id") user_id: Int,
                   @Query("password") password: String,
                   @Query("lang") lang:String):Call<UserResponse>
    @POST("check-code")
    fun checkCode(@Query("lang") lang:String,
                  @Query("user_id") user_id:Int,
                  @Query("code") code:String):Call<UserResponse>
    @POST("resend-code")
    fun resend(@Query("user_id") user_id: Int,
               @Query("lang") lang:String):Call<ResendCodeModel>

    @POST("forget-password")
    fun forgotPass(@Query("phone") phone:String,
                   @Query("lang") lang:String):Call<UserResponse>
    @POST("sign-in")
    fun signIn(@Query("lang") lang:String,
               @Query("phone") phone:String,
               @Query("password") password:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String):Call<UserResponse>

    @POST("edit-profile-provider")
    fun getProvider(@Query("lang") lang:String,
                    @Query("provider_id") provider_id:Int):Call<UserResponse>

    @Multipart
    @POST("edit-profile-provider")
    fun updateProviderImage(@Query("lang") lang:String,
                            @Query("provider_id") provider_id:Int,
                            @Part avatar:MultipartBody.Part):Call<UserResponse>
    @POST("edit-profile-provider")
    fun updateProvider(@Query("lang") lang:String,
                       @Query("provider_id") provider_id:Int,
                       @Query("name") name:String,
                       @Query("phone") phone:String,
                       @Query("commercial_register") commercial_register:String,
                       @Query("city_id") city_id:Int,
                       @Query("email") email:String):Call<UserResponse>

    @POST("reset-password")
    fun resetPassword(@Query("lang") lang:String,
                      @Query("user_id") user_id:Int,
                      @Query("current_password") current_password:String,
                      @Query("password") password:String):Call<ResetPasswordModel>
    @POST("specialties")
    fun specialties(@Query("lang") lang: String):Call<ListResponse>

    @POST("add-job")
    fun addJob(@Query("lang") lang:String,
               @Query("provider_id") provider_id:Int,
               @Query("title_ar") title_ar:String,
               @Query("title_en") title_en:String,
               @Query("specialty_id") specialty_id:Int,
               @Query("salary") salary:String,
               @Query("city_id") city_id:Int,
               @Query("direction") direction:String,
               @Query("certificates_ar") certificates_ar:String,
               @Query("certificates_en") certificates_en:String,
               @Query("width") width:String,
               @Query("height") height:String,
               @Query("age") age:String,
               @Query("experience_ar") experience_ar:String,
               @Query("experience_en") experience_en:String,
               @Query("start_date") start_date:String,
               @Query("end_date") end_date:String):Call<AddJobModel>

    @POST("edit-job")
    fun editJob(@Query("lang") lang:String,
                @Query("provider_id") provider_id:Int,
                @Query("job_id") job_id:Int,
                @Query("title_ar") title_ar:String,
                @Query("title_en") title_en:String,
                @Query("specialty_id") specialty_id:Int,
                @Query("salary") salary:String,
                @Query("city_id") city_id:Int,
                @Query("direction") direction:String,
                @Query("certificates_ar") certificates_ar:String,
                @Query("certificates_en") certificates_en:String,
                @Query("width") width:String,
                @Query("height") height:String,
                @Query("age") age:String,
                @Query("experience_ar") experience_ar:String,
                @Query("experience_en") experience_en:String,
                @Query("start_date") start_date:String,
                @Query("end_date") end_date:String):Call<JobDetailsResponse>

    @POST("delete-job")
    fun deleteJob(@Query("lang") lang:String,
                  @Query("job_id") job_id:Int,
                  @Query("provider_id") provider_id:Int):Call<AddJobModel>
    @POST("job-details")
    fun getJob(@Query("lang") lang:String,
               @Query("provider_id") provider_id:Int,
               @Query("job_id") job_id:Int):Call<JobDetailsResponse>

    @POST("order-details")
    fun getOrder(@Query("lang") lang:String,
                 @Query("provider_id") provider_id:Int,
                 @Query("order_id") order_id:Int,
                 @Query("job_id") job_id:Int,
                 @Query("status") status:String):Call<ProviderOrderDetails>

    @POST("details-request")
    fun getRequest(@Query("lang") lang:String,
                   @Query("order_id") order_id:Int,
                   @Query("provider_id") provider_id:Int,
                   @Query("action") action:String?,
                   @Query("reason") reason: String?
    ):Call<RequestDetailsResponse>
    @POST("user-details")
    fun getdetails(@Query("lang") lang:String,
                   @Query("job_id") job_id:Int,
                   @Query("provider_id") provider_id:Int,
                   @Query("user_id") user_id: Int):Call<RequestDetailsResponse>
    @POST("comments")
    fun getComments(@Query("lang") lang:String,
                    @Query("provider_id") provider_id:Int):Call<CommentsResponse>

    @POST("cancellations")
    fun getResons(@Query("lang") lang:String):Call<CancelResponse>
    @POST("cancellation-comment")
    fun cancelComment(@Query("lang") lang: String,
                      @Query("provider_id") provider_id:Int,
                      @Query("cancellation_id")cancellation_id:Int,
                      @Query("comment_id") comment_id:Int):Call<ResendCodeModel>
    @POST("financial")
    fun financial(@Query("provider_id") provider_id:Int):Call<ResendCodeModel>
    @POST("bank-accounts")
    fun banks(@Query("lang") lang:String,
              @Query("user_id") user_id:Int):Call<BanksResponse>
    @Multipart
    @POST("bank-accounts")
    fun transfer(@Query("lang") lang:String,
              @Query("user_id") user_id:Int,
                 @Query("name") name:String,
                 @Query("bank_name") bank_name:String,
                 @Query("amount") amount:String,
                 @Part image:MultipartBody.Part):Call<BanksResponse>
    @POST("switch-notification")
    fun switchNotification(@Query("user_id") user_id:Int,
                           @Query("switch") switch:Int):Call<SwitchNotification>

    @POST("log-out")
    fun logOut(@Query("user_id") user_id:Int,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Query("lang") lang:String):Call<ResendCodeModel>

    @POST("company-details")
    fun getCompany(@Query("lang") lang:String,
                   @Query("provider_id") provider_id:Int,
                   @Query("city_id") city_id:Int?,
                   @Query("direction") direction:String?):Call<AvaalbleJobsResponse>

    @POST("company-comments")
    fun companyComments(@Query("lang") lang:String,
                        @Query("provider_id") provider_id:Int):Call<CompanyCommentResponse>
    @POST("job-details-user")
    fun present(@Query("lang") lang:String,
                @Query("user_id") user_id:Int,
                @Query("provider_id") provider_id:Int,
                @Query("job_id") job_id:Int,
                @Query("request_job") request_job:String?):Call<JobDetailsResponse>
    @POST("job-details-user")
    fun Present(@Query("lang") lang:String,
                @Query("user_id") user_id:Int,
                @Query("provider_id") provider_id:Int,
                @Query("job_id") job_id:Int,
                @Query("request_job") request_job:String?):Call<JobDetailsResponse>

    @POST("add-comment")
    fun AddComment(@Query("lang") lang:String,
                   @Query("user_id") user_id:Int,
                   @Query("provider_id") provider_id:Int,
                   @Query("comment") comment:String,
                   @Query("rate") rate:Int):Call<ResendCodeModel>

    @POST("user-orders")
    fun getUserOrders(@Query("lang") lang:String,
                      @Query("user_id") user_id:Int,
                      @Query("status")status:String ):Call<UserOrdersResponse>

    @POST("job-order-details")
    fun getOrderDetails(@Query("lang") lang:String,
                        @Query("user_id") user_id:Int,
                        @Query("order_id") order_id:Int):Call<UserOrderDetailsResponse>

    @POST("languages")
    fun langs(@Query("lang") lang:String):Call<ListResponse>

    @Multipart
    @POST("sign-up")
    fun singUpWithoutImage(@Query("user_type") user_type:String,
                           @Query("name") name:String,
                           @Query("phone") phone:String,
                           @Query("city_id") city_id:Int,
                           @Query("gender") gender:String,
                           @Query("birth_date") birth_date:String,
                           @Query("width") width:String,
                           @Query("height") height:String,
                           @Part cv:MultipartBody.Part,
                           @Query("password") password:String,
                           @Query("device_id") device_id:String,
                           @Query("device_type") device_type:String,
                           @Query("idNumber") idNumber:String,
                           @Query("education") education:String,
                           @Query("language_id") language_id:Int?,
                           @Query("efficiency") efficiency:String?):Call<UserResponse>
    @Multipart
    @POST("sign-up")
    fun singUpWithImage(@Query("user_type") user_type:String,
                           @Query("name") name:String,
                           @Query("phone") phone:String,
                           @Query("city_id") city_id:Int,
                           @Query("gender") gender:String,
                           @Query("birth_date") birth_date:String,
                           @Query("width") width:String,
                           @Query("height") height:String,
                           @Part avatar:MultipartBody.Part,
                           @Part cv:MultipartBody.Part,
                           @Query("password") password:String,
                           @Query("device_id") device_id:String,
                           @Query("device_type") device_type:String,
                           @Query("idNumber") idNumber:String,
                           @Query("education") education:String,
                           @Query("language_id") language_id:Int?,
                           @Query("efficiency") efficiency:String?):Call<UserResponse>

    @POST("edit-profile")
    fun getProfile(@Query("lang") lang:String,
                   @Query("user_id") user_id:Int):Call<UserResponse>

    @Multipart
    @POST("edit-profile")
    fun updateUserImage(@Query("lang") lang:String,
                            @Query("user_id") user_id:Int,
                            @Part avatar:MultipartBody.Part):Call<UserResponse>
    @Multipart
    @POST("edit-profile")
    fun updateUserWithCv(@Query("lang") lang:String,
                        @Query("user_id") user_id:Int,
                         @Query("name") name:String,
                         @Query("phone") phone:String,
                         @Query("city_id") city_id:Int,
                         @Query("gender") gender:String,
                         @Query("birth_date") birth_date:String,
                         @Query("width") width:String,
                         @Query("height") height:String,
                         @Part cv:MultipartBody.Part,
                         @Query("idNumber") idNumber:String,
                         @Query("education") education:String,
                         @Query("language_id") language_id:Int?,
                         @Query("efficiency") efficiency:String?):Call<UserResponse>
    @POST("edit-profile")
    fun updateUserWithoutCv(@Query("lang") lang:String,
                         @Query("user_id") user_id:Int,
                         @Query("name") name:String,
                         @Query("phone") phone:String,
                         @Query("city_id") city_id:Int,
                         @Query("gender") gender:String,
                         @Query("birth_date") birth_date:String,
                         @Query("width") width:String,
                         @Query("height") height:String,
                         @Query("idNumber") idNumber:String,
                         @Query("education") education:String,
                         @Query("language_id") language_id:Int?,
                         @Query("efficiency") efficiency:String?):Call<UserResponse>


    @POST("notifications")
    fun getNotification(@Query("lang") lang: String,
                        @Query("user_id") user_id: Int):Call<NotificationResponse>

    @POST("delete-notification")
    fun delete(@Query("lang") lang: String,
               @Query("user_id") user_id:Int,
               @Query("notification_id") notification_id:Int):Call<ResendCodeModel>

    @POST("search-city")
    fun providerSearch(@Query("lang") lang: String,
                       @Query("city_id") city_id: Int):Call<ProviderSearchModel>

}