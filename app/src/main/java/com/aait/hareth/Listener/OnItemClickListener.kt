package com.aait.hareth.Listener

import android.view.View

interface OnItemClickListener {
    fun onItemClick(view: View, position: Int)
}
